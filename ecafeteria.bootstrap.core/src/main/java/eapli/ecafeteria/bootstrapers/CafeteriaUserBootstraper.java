/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates and open the template
 * in the editor.
 */
package eapli.ecafeteria.bootstrapers;

import java.util.logging.Logger;

import eapli.ecafeteria.application.cafeteria.AcceptRefuseSignupRequestController;
import eapli.ecafeteria.application.cafeteria.SignupController;
import eapli.ecafeteria.domain.account.Account;
import eapli.ecafeteria.domain.account.AccountTransation;
import eapli.ecafeteria.domain.authz.Username;
import eapli.ecafeteria.domain.cafeteria.OrganicUnit;
import eapli.ecafeteria.domain.cafeteria.SignupRequest;
import eapli.ecafeteria.persistence.CafeteriaUserRepository;
import eapli.ecafeteria.persistence.OrganicUnitRepository;
import eapli.ecafeteria.persistence.PersistenceContext;
import eapli.framework.actions.Action;
import eapli.framework.domain.Money;
import eapli.framework.persistence.DataConcurrencyException;
import eapli.framework.persistence.DataIntegrityViolationException;
import java.util.logging.Level;

/**
 *
 * @author Paulo Sousa
 */
public class CafeteriaUserBootstraper implements Action {

    @Override
    public boolean execute() {
        final OrganicUnitRepository unitRepo = PersistenceContext.repositories().organicUnits();
        final OrganicUnit unit = unitRepo.findByAcronym("ISEP");
        final CafeteriaUserRepository repo = PersistenceContext.repositories().cafeteriaUsers(null);

        Username username = new Username("900330");
        signupAndApprove(username.toString(), "Password1", "John", "Smith", "john@smith.com", unit, username.toString(), "123");
        Account acc = repo.findByUsername(username).account();
        AccountTransation tran = new AccountTransation(acc, Money.euros(50), AccountTransation.TransationType.CREDIT);
        try {
            acc.registTransaction(tran);
        } catch (IllegalAccessException ex) {
            Logger.getLogger(CafeteriaUserBootstraper.class.getName()).log(Level.SEVERE, null, ex);
        }

        username = new Username("900331");
        signupAndApprove(username.toString(), "Password1", "Mary", "Smith", "mary@smith.com", unit, username.toString(), "1234");
        acc = repo.findByUsername(username).account();
        tran = new AccountTransation(acc, Money.euros(50), AccountTransation.TransationType.CREDIT);
        try {
            acc.registTransaction(tran);
        } catch (IllegalAccessException ex) {
            Logger.getLogger(CafeteriaUserBootstraper.class.getName()).log(Level.SEVERE, null, ex);
        }

        return false;
    }

    /**
     *
     */
    private SignupRequest signupAndApprove(final String username, final String password, final String firstName,
            final String lastName, final String email, OrganicUnit organicUnit, String mecanographicNumber, String bankAccount) {
        final SignupController signupController = new SignupController();
        final AcceptRefuseSignupRequestController acceptController = new AcceptRefuseSignupRequestController();
        SignupRequest request = null;
        try {
            request = signupController.signup(username, password, firstName, lastName, email, organicUnit,
                    mecanographicNumber, bankAccount);
            acceptController.acceptSignupRequest(request);
        } catch (final DataConcurrencyException | DataIntegrityViolationException e) {
            // assume it just a question of trying to insert duplicate record
            Logger.getLogger(ECafeteriaBootstraper.class.getSimpleName())
                    .info("EAPLI-DI001: Exception during bootstrapping: assuming existing record.");
        }
        return request;
    }
}
