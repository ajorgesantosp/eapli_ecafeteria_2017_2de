/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package eapli.ecafeteria.bootstrapers;

import eapli.ecafeteria.application.meals.PublishMealController;
import eapli.ecafeteria.domain.meals.Dish;
import eapli.ecafeteria.domain.meals.Meal;
import eapli.ecafeteria.domain.meals.MealType;
import eapli.ecafeteria.persistence.DishRepository;
import eapli.ecafeteria.persistence.MealRepository;
import eapli.ecafeteria.persistence.MealTypeRepository;
import eapli.ecafeteria.persistence.PersistenceContext;
import eapli.framework.actions.Action;
import eapli.framework.domain.Designation;
import eapli.framework.persistence.DataConcurrencyException;
import eapli.framework.persistence.DataIntegrityViolationException;
import eapli.util.DateTime;
import java.text.DateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.logging.Logger;

/**
 *
 * @author Bruno Gomes <1151164@isep.ipp.pt>, Nuno Brito <1151173@isep.ipp.pt>
 */
public class MealBootstraper implements Action {

    @Override
    public boolean execute() {

        final DishRepository dishRepo = PersistenceContext.repositories().dishes();
        final MealTypeRepository mealTypeRepo = PersistenceContext.repositories().mealTypes();

        final Dish d1 = dishRepo.findByName(Designation.valueOf("picanha"));
        final Dish d2 = dishRepo.findByName(Designation.valueOf("tofu grelhado"));

        final MealType almoco = mealTypeRepo.findByName("Almoço");
        final MealType jantar = mealTypeRepo.findByName("Jantar");

        Calendar day = DateTime.now();
        day.add(Calendar.DAY_OF_MONTH, 10);
        register(DateTime.format(day,"dd-MM-yyyy"), almoco, d1);
        register(DateTime.format(day,"dd-MM-yyyy"), jantar, d1);
        register(DateTime.format(day,"dd-MM-yyyy"), almoco, d2);
        register(DateTime.format(day,"dd-MM-yyyy"), jantar, d2);

        return false;
    }

    private void register(String day, MealType mealType, Dish dish) {
        final PublishMealController controller = new PublishMealController();
        try {
            final Meal meal = controller.createMeal(day, mealType, dish);
            controller.publishMeal(meal);
        } catch (final DataIntegrityViolationException | DataConcurrencyException e) {
            // ignoring exception. assuming it is just a primary key violation
            // due to the tentative of inserting a duplicated user
            Logger.getLogger(ECafeteriaBootstraper.class.getSimpleName())
                    .info("EAPLI-ME001: bootstrapping existing record.");
        } catch (Exception ex) {
            Logger.getLogger(ECafeteriaBootstraper.class.getSimpleName())
                    .info("EAPLI-ME002: Invalid date.");
        }
    }

    private boolean testFindByDay() {
        final MealRepository oRep = PersistenceContext.repositories().meals();

        Iterable<Meal> meals = oRep.findByDay(DateTime.parseDate("01-05-2017"));
        if (!meals.iterator().hasNext()) {
            return false;
        } else {
            for (Meal m : meals) {
                Logger.getGlobal().info("Meal: id: " + m.id().toString());
            }
            return true;
        }
    }

    private boolean testFindFromDay() {
        final MealRepository oRep = PersistenceContext.repositories().meals();

        Iterable<Meal> meals = oRep.findFromDay(DateTime.parseDate("01-05-2017"));
        if (!meals.iterator().hasNext()) {
            return false;
        } else {
            for (Meal m : meals) {
                Logger.getGlobal().info("Meal: id: " + m.id().toString());
            }
            return true;
        }
    }

    private boolean testFindByDayAndMealType() {
        final MealRepository oRep = PersistenceContext.repositories().meals();

        final MealTypeRepository oRepMT = PersistenceContext.repositories().mealTypes();
        final MealType mealType = oRepMT.findByName("Almoço");

        Iterable<Meal> meals = oRep.findByDayAndMealType(mealType, DateTime.parseDate("01-05-2017"));
        if (!meals.iterator().hasNext()) {
            return false;
        } else {
            for (Meal m : meals) {
                Logger.getGlobal().info("Meal: id: " + m.id().toString());
            }
            return true;
        }
    }
}
