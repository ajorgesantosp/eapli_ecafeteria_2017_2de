/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package eapli.ecafeteria.bootstrapers;

import eapli.ecafeteria.domain.meals.MealType;
import eapli.ecafeteria.persistence.DishRepository;
import eapli.ecafeteria.persistence.MealTypeRepository;
import eapli.framework.actions.Action;
import eapli.framework.domain.Designation;
import eapli.framework.persistence.DataConcurrencyException;
import eapli.framework.persistence.DataIntegrityViolationException;
import java.util.logging.Logger;
import eapli.ecafeteria.persistence.PersistenceContext;

/**
 *
 * @author Bruno Gomes <1151164@isep.ipp.pt>, Nuno Brito <1151173@isep.ipp.pt>
 */
public class MealTypeBootstraper implements Action {

    @Override
    public boolean execute() {
        
        register("Almoço");
        register("Jantar");
        
        return false;
    }
    
    private void register(String mealType) {
        final MealTypeRepository mealTypeRepo = PersistenceContext.repositories().mealTypes();
        try {
            final MealType theMealType = new MealType(mealType);
            mealTypeRepo.save(theMealType);
        } catch (final DataIntegrityViolationException | DataConcurrencyException e) {
            // ignoring exception. assuming it is just a primary key violation
            // due to the tentative of inserting a duplicated user
            Logger.getLogger(ECafeteriaBootstraper.class.getSimpleName())
                    .info("EAPLI-MT001: bootstrapping existing record");
        }
    }
    
}
