/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package eapli.ecafeteria.bootstrapers;

import eapli.ecafeteria.application.MealMenu.CreateEditMealMenuController;
import eapli.ecafeteria.domain.MealMenu.MealMenu;
import eapli.ecafeteria.domain.meals.Dish;
import eapli.ecafeteria.domain.meals.MealType;
import eapli.ecafeteria.persistence.DishRepository;
import eapli.ecafeteria.persistence.MealTypeRepository;
import eapli.ecafeteria.persistence.PersistenceContext;
import eapli.framework.actions.Action;
import eapli.framework.persistence.DataConcurrencyException;
import eapli.framework.persistence.DataIntegrityViolationException;
import eapli.util.DateTime;
import java.util.Calendar;
import java.util.Date;
import java.util.Iterator;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 *
 * @author joaos_000
 */
public class MealMenuBootStrapper implements Action{
    CreateEditMealMenuController controller = new CreateEditMealMenuController();
    
    @Override
    public boolean execute() {
        
        final DishRepository dishRepo = PersistenceContext.repositories().dishes();
        
        Calendar firstDay1 = DateTime.tomorrow(); 
        Calendar lastDay1 = DateTime.tomorrow(); 
        lastDay1.add(Calendar.DAY_OF_MONTH, 6);
        
        
        Calendar mealDay1 = (Calendar)firstDay1.clone();
        mealDay1.add(Calendar.DAY_OF_MONTH, 1);
        
        Calendar mealDay2 = (Calendar)mealDay1.clone();
        mealDay2.add(Calendar.DAY_OF_MONTH, 1);
        
        Calendar mealDay3 = (Calendar)mealDay2.clone();
        mealDay3.add(Calendar.DAY_OF_MONTH, 1);
        
        final MealTypeRepository mealTypeRepo = PersistenceContext.repositories().mealTypes();
        
        MealType mealType1 = mealTypeRepo.findByName(MealType.DINNER);
        MealType mealType2 = mealTypeRepo.findByName(MealType.LUNCH);
        
        Iterator<Dish> dishes = dishRepo.findAll().iterator();
        Dish dish1 = dishes.next();
        Dish dish2 = dishes.next();
        
        MealMenu mealMenu1 = createMealMenu(firstDay1, lastDay1); 
        addMeal(mealDay1, mealType1, dish1, mealMenu1);
        addMeal(mealDay2, mealType1, dish1, mealMenu1);
        addMeal(mealDay3, mealType1, dish2, mealMenu1);
        
        
        return false;
    }

    private MealMenu createMealMenu(Calendar firstDay, Calendar lastDay) {
        
        try {
            return controller.createMealMenu(firstDay, lastDay);
            
        } catch (DataConcurrencyException ex) {
            Logger.getLogger(MealMenuBootStrapper.class.getName()).log(Level.SEVERE, null, ex);
        } catch (DataIntegrityViolationException ex) {
            Logger.getLogger(MealMenuBootStrapper.class.getName()).log(Level.SEVERE, null, ex);
        }
        
        return null;
    }

    private void addMeal(Calendar day,MealType mealType, Dish dish, MealMenu mealMenu) {
        try {
            controller.registerMeal(day, mealType, dish, mealMenu);
        } catch (DataConcurrencyException ex) {
            Logger.getLogger(MealMenuBootStrapper.class.getName()).log(Level.SEVERE, null, ex);
        } catch (DataIntegrityViolationException ex) {
            Logger.getLogger(MealMenuBootStrapper.class.getName()).log(Level.SEVERE, null, ex);
        }
    }

    
}
