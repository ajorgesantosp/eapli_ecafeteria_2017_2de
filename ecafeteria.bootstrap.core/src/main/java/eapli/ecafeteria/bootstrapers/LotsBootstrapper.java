/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package eapli.ecafeteria.bootstrapers;

import eapli.ecafeteria.domain.kitchen.Lot;
import eapli.ecafeteria.domain.kitchen.Material;
import eapli.ecafeteria.persistence.LotRepository;
import eapli.ecafeteria.persistence.MaterialRepository;
import eapli.ecafeteria.persistence.PersistenceContext;
import eapli.framework.actions.Action;
import eapli.framework.persistence.DataConcurrencyException;
import eapli.framework.persistence.DataIntegrityViolationException;
import java.util.logging.Logger;

/**
 *
 * @author calve
 */
public class LotsBootstrapper implements Action {

    @Override
    public boolean execute() {

        final MaterialRepository materialRepo = PersistenceContext.repositories().materials();
        final Material m1 = materialRepo.findByAcronym("eggs");
        final Material m2 = materialRepo.findByAcronym("oil");
        final Material m3 = materialRepo.findByAcronym("so");
        register("LOT002", m1);
        register("LOT003", m1);
        register("LOT001", m1);
        register("LOT004", m2);
        register("LOT005", m2);
        register("LOT006", m2);
        register("LOT007", m3);
        register("LOT008", m3);
        return true;
    }

    private void register(String code, Material material) {
        LotRepository lotRepository = PersistenceContext.repositories().lots();
        try {
            lotRepository.save(new Lot(code, material));
        } catch (final DataIntegrityViolationException | DataConcurrencyException e) {
            // ignoring exception. assuming it is just a primary key violation
            // due to the tentative of inserting a duplicated user
            Logger.getLogger(ECafeteriaBootstraper.class.getSimpleName())
                    .info("EAPLI-DI001: bootstrapping existing record");
        }
    }

}
