/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package eapli.ecafeteria.bootstrapers;

import eapli.ecafeteria.application.pos.OpenCashRegisterController;
import eapli.ecafeteria.domain.cafeteria.CashRegister;
import eapli.ecafeteria.domain.meals.MealType;
import eapli.ecafeteria.persistence.CashRegisterRepository;
import eapli.ecafeteria.persistence.MealTypeRepository;
import eapli.ecafeteria.persistence.PersistenceContext;
import eapli.framework.actions.Action;
import eapli.framework.persistence.DataConcurrencyException;
import eapli.framework.persistence.DataIntegrityViolationException;
import java.util.logging.Logger;

/**
 *
 * @author Jorge Pereira 1141216 \ Luís Magalhães 1141174
 */
public class CashRegisterBootstrapper implements Action {

    @Override
    public boolean execute() {
        CashRegister cashRegister = new CashRegister();
        register(cashRegister);
        return true;
    }

    private void register(CashRegister register) {
        CashRegisterRepository cashRepo = PersistenceContext.repositories().cashRegister(null);
        try {
            cashRepo.save(register);
        } catch (final DataIntegrityViolationException | DataConcurrencyException e) {
            // ignoring exception. assuming it is just a primary key violation
            // due to the tentative of inserting a duplicated cashRegister
            Logger.getLogger(ECafeteriaBootstraper.class.getSimpleName())
                    .info("EAPLI-ME001: bootstrapping existing record.");
        }
    }
}
