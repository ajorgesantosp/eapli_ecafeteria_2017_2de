/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package eapli.ecafeteria.bootstrapers;

import eapli.ecafeteria.application.MealReservation.MealReservationController;
import eapli.ecafeteria.domain.account.AccountTransation;
import eapli.ecafeteria.domain.authz.Username;
import eapli.ecafeteria.domain.cafeteria.CafeteriaUser;
import eapli.ecafeteria.domain.mealReservation.Reservation;
import eapli.ecafeteria.domain.mealReservation.ReservationBuilder;
import eapli.ecafeteria.domain.meals.Meal;
import eapli.ecafeteria.persistence.CafeteriaUserRepository;
import eapli.ecafeteria.persistence.MealRepository;
import eapli.ecafeteria.persistence.MealReservationRepository;
import eapli.ecafeteria.persistence.PersistenceContext;
import eapli.framework.actions.Action;
import eapli.framework.domain.Money;
import eapli.framework.persistence.DataConcurrencyException;
import eapli.framework.persistence.DataIntegrityViolationException;
import eapli.util.DateTime;
import java.util.Calendar;
import java.util.Currency;
import java.util.Date;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.persistence.Persistence;

/**
 *
 * @author Jorge Pereira 1141216 \ Luís Magalhães 1141174
 */
public class MealReservationBoostrapper implements Action{

    @Override
    public boolean execute() {
        CafeteriaUserRepository userRepo = PersistenceContext.repositories().cafeteriaUsers(null);
        MealRepository mealRepo = PersistenceContext.repositories().meals();
        CafeteriaUser user = userRepo.findByUsername(new Username("900330"));
        MealReservationRepository reservationRepo = PersistenceContext.repositories().mealsReservations(null);
        Calendar date = DateTime.now();
        Iterable<Meal> meal =  mealRepo.findFromDay(date);
        try {
            user.account().registTransaction(new AccountTransation(user.account(), new Money(50, Currency.getInstance("EUR")), AccountTransation.TransationType.CREDIT));
        } catch (IllegalAccessException ex) {
            Logger.getLogger(MealReservationBoostrapper.class.getName()).log(Level.SEVERE, null, ex);
        }
        Reservation r = new ReservationBuilder().withMeal(meal.iterator().next()).withCafeteriaUser(user).withDate(DateTime.parseDate(DateTime.format(date, "dd-MM-yyyy"))).build();
        try {
            reservationRepo.save(r);
        } catch (DataConcurrencyException | DataIntegrityViolationException ex) {
            Logger.getLogger(MealReservationBoostrapper.class.getName()).log(Level.SEVERE, null, ex);
        }
        return false;
    }
    
}
