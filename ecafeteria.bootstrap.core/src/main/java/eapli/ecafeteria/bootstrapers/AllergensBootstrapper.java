/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package eapli.ecafeteria.bootstrapers;

import eapli.ecafeteria.domain.meals.Allergen;
import eapli.ecafeteria.persistence.AllergenRepository;
import eapli.ecafeteria.persistence.PersistenceContext;
import eapli.framework.actions.Action;
import eapli.framework.domain.Designation;
import eapli.framework.persistence.DataConcurrencyException;
import eapli.framework.persistence.DataIntegrityViolationException;
import java.util.logging.Logger;

/**
 *
 * @author calve
 */
public class AllergensBootstrapper implements Action {

    @Override
    public boolean execute() {
        register(Designation.valueOf("Cereais que contêm glúten (trigo, centeio, cevada, aveia, espelta, gamut ou outras estirpes hibridizadas) e produtos à base destes cereais"));
        register(Designation.valueOf("Crustáceos e produtos à base de crustáceos"));
        register(Designation.valueOf("Ovos e produtos à base de ovos"));
        register(Designation.valueOf("Peixes e produtos à base de peixe"));
        register(Designation.valueOf("Amendoins e produtos à base de amendoins"));
        register(Designation.valueOf("Soja e produtos à base de soja"));
        register(Designation.valueOf("Leite e produtos à base de leite (incluindo lactose)"));
        register(Designation.valueOf("Frutos de casca rija, nomeadamente, amêndoas, avelãs, nozes, castanhas de caju, pistácios, entre outros"));
        register(Designation.valueOf("Aipo e produtos à base de aipo"));
        register(Designation.valueOf("Mostarda e produtos à base de mostarda"));
        register(Designation.valueOf("Sementes de sésamo e produtos à base de sementes de sésamo"));
        register(Designation.valueOf("Dióxido de enxofre e sulfitos em concentrações superiores a 10mg/Kg ou 10ml/L"));
        register(Designation.valueOf("Tremoço e produtos à base de tremoço"));
        register(Designation.valueOf("Tremoço e produtos à base de tremoço"));
        register(Designation.valueOf("Moluscos e produtos à base de moluscos"));
        return false;
    }

    private void register(Designation name) {
        AllergenRepository allergenRepository = PersistenceContext.repositories().allergens();
        try {
            allergenRepository.save(new Allergen(name));
        } catch (final DataIntegrityViolationException | DataConcurrencyException e) {
            // ignoring exception. assuming it is just a primary key violation
            // due to the tentative of inserting a duplicated user
            Logger.getLogger(ECafeteriaBootstraper.class.getSimpleName())
                    .info("EAPLI-DI001: bootstrapping existing record");
        }
    }

}
