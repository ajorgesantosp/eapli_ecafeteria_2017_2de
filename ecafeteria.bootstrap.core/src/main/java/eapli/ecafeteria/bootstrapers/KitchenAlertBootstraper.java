/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package eapli.ecafeteria.bootstrapers;

import eapli.ecafeteria.application.kitchen.DefineKitchenAlertController;
import eapli.ecafeteria.domain.kitchen.KitchenAlert;
import eapli.framework.actions.Action;
import eapli.framework.domain.Designation;
import eapli.framework.persistence.DataConcurrencyException;
import eapli.framework.persistence.DataIntegrityViolationException;
import java.util.logging.Logger;

/**
 *
 * @author Bruno Gomes <1151164@isep.ipp.pt>, Nuno Brito <1151173@isep.ipp.pt>
 */
public class KitchenAlertBootstraper implements Action {

    @Override
    public boolean execute() {

        final KitchenAlert a1 = new KitchenAlert(Designation.valueOf("Amarelo"), 1);
        final KitchenAlert a2 = new KitchenAlert(Designation.valueOf("Vermelho"), 1);

        register(a1, 75);
        register(a2, 90);
        return false;
    }

    private void register(KitchenAlert ka, Integer limit) {
        final DefineKitchenAlertController controller = new DefineKitchenAlertController();
        try {
            controller.updateLimit(ka, limit);
        } catch (final DataIntegrityViolationException | DataConcurrencyException e) {
            // ignoring exception. assuming it is just a primary key violation
            // due to the tentative of inserting a duplicated user
            Logger.getLogger(ECafeteriaBootstraper.class.getSimpleName())
                    .info("EAPLI-KA001: bootstrapping existing record.");
        }
    }

}
