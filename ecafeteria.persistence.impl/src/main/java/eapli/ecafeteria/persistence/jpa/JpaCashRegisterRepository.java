/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package eapli.ecafeteria.persistence.jpa;

import eapli.ecafeteria.Application;
import eapli.ecafeteria.domain.cafeteria.CashRegister;
import eapli.ecafeteria.persistence.CashRegisterRepository;
import eapli.framework.persistence.repositories.TransactionalContext;
import eapli.framework.persistence.repositories.impl.jpa.JpaAutoTxRepository;

/**
 *
 * @author Jorge Pereira 1141216 \ Luís Magalhães 1141174
 */
public class JpaCashRegisterRepository extends JpaAutoTxRepository<CashRegister, Long> implements CashRegisterRepository{

    public JpaCashRegisterRepository(TransactionalContext autoTx) {
       super(Application.settings().getPersistenceUnitName(), autoTx);
    }

    @Override
    public CashRegister find() {
        //TODO AT THE MOMENT THERE ONLY IS ONE CASH REGISTER
        return iterator().next();
    }
    
}
