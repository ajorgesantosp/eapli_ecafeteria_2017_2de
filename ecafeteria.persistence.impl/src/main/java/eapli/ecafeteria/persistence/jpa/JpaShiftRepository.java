/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package eapli.ecafeteria.persistence.jpa;

import eapli.ecafeteria.domain.cafeteria.Shift;
import eapli.ecafeteria.persistence.ShiftRepository;

/**
 *
 * @author Miguel
 */
public class JpaShiftRepository extends CafeteriaJpaRepositoryBase<Shift, Long>implements ShiftRepository {

    public JpaShiftRepository() {
    }
    
}
