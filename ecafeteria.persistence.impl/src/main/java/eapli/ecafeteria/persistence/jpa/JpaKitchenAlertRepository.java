/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package eapli.ecafeteria.persistence.jpa;

import eapli.ecafeteria.domain.kitchen.KitchenAlert;
import eapli.ecafeteria.persistence.KitchenAlertRepository;
import eapli.framework.domain.Designation;

/**
 *
 * @author Bruno Gomes <1151164@isep.ipp.pt>, Nuno Brito <1151173@isep.ipp.pt>
 */
public class JpaKitchenAlertRepository extends CafeteriaJpaRepositoryBase<KitchenAlert, Designation> implements KitchenAlertRepository {

    @Override
    public KitchenAlert findByName(Designation name) {
        return matchOne("e.name=:name", "name", name);
    }
    
}
