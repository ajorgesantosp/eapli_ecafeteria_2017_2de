/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package eapli.ecafeteria.persistence.jpa;

import eapli.ecafeteria.Application;
import eapli.ecafeteria.domain.cafeteria.CafeteriaUser;
import eapli.ecafeteria.domain.mealReservation.Reservation;
import eapli.ecafeteria.domain.mealReservation.ReservationState;
import eapli.ecafeteria.domain.meals.MealType;
import eapli.ecafeteria.persistence.MealReservationRepository;
import eapli.framework.persistence.repositories.TransactionalContext;
import eapli.framework.persistence.repositories.impl.jpa.JpaAutoTxRepository;
import java.util.Calendar;
import java.util.HashMap;
import java.util.Map;
import java.util.Optional;

/**
 *
 * @author Jorge Pereira 1141216 \ Luís Magalhães 1141174
 */
//public class JpaMealReservationRepository extends CafeteriaJpaRepositoryBase<Reservation, Long> implements MealReservationRepository{
public class JpaMealReservationRepository extends JpaAutoTxRepository<Reservation, Long> implements MealReservationRepository {

    public JpaMealReservationRepository(TransactionalContext autoTx) {
        super(Application.settings().getPersistenceUnitName(), autoTx);
    }

    @Override
    public Iterable<Reservation> findByDay(Calendar day, MealType mt) {
        Map<String, Object> args = new HashMap<>();
        args.put("day", day);
        args.put("mealType", mt);
        return this.repo.match("e.meal.day=:day AND e.meal.mealType =:mealType", args);
    }

    @Override
    public Reservation findReserveCreatedHashDay(Integer hashCode, Calendar day, MealType mt) {
        Map<String, Object> args = new HashMap<>();
        args.put("hash", hashCode);
        args.put("day", day);
        args.put("mealType", mt);
        args.put("state", ReservationState.CREATED);
        return this.repo.matchOne("e.hash=:hash AND e.meal.day=:day AND e.meal.mealType=:mealType AND e.state=:state", args);
    }

    @Override
    public Reservation findFromHashCode(Integer hashCode) {
        return this.repo.matchOne("e.hash=:hash", "hash", hashCode);
    }

    @Override
    public Iterable<Reservation> findReservesNDaysFromUser(Calendar day, CafeteriaUser user) {
        Map<String, Object> args = new HashMap<>();
        args.put("day", day);
        args.put("dayActual", Calendar.getInstance());
        args.put("user", user);
        args.put("reservation_state", ReservationState.CREATED);
        return this.repo.match("e.meal.day<=:day AND e.meal.day>=:dayActual AND e.user=:user AND e.state=:reservation_state", args);
    }

    @Override
    public Iterable<Reservation> reservationByUser(CafeteriaUser cafUser) {

        Map<String, Object> args = new HashMap<>();
        args.put("user", cafUser);
        return this.repo.match("e.user=:user", args);
    }
}
