/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package eapli.ecafeteria.persistence.jpa;

import eapli.ecafeteria.domain.kitchen.CookedMeal;
import eapli.ecafeteria.domain.kitchen.Lot;
import eapli.ecafeteria.domain.meals.Meal;
import eapli.ecafeteria.persistence.CookedMealRepository;
import java.util.HashMap;
import java.util.Map;

/**
 *
 * @author Rafael Delgado <Rafael.delgado at Rafael.org>
 */
public class JpaCookedMealRepository extends CafeteriaJpaRepositoryBase<CookedMeal, Meal> implements CookedMealRepository {

    public JpaCookedMealRepository() {
    }

    @Override
    public Iterable<CookedMeal> findByLot(Lot lot) {
        Map<String, Object> map = new HashMap<>();
        map.put("lot", lot);
        return match(":lot in e.lot", map);
    }

}
