package eapli.ecafeteria.persistence.jpa;

import eapli.ecafeteria.Application;
import eapli.ecafeteria.persistence.AllergenRepository;
import eapli.ecafeteria.persistence.CookedMealRepository;
import eapli.ecafeteria.persistence.BankAccountRepository;
import eapli.ecafeteria.persistence.CashRegisterRepository;
import eapli.ecafeteria.persistence.DishRepository;
import eapli.ecafeteria.persistence.DishTypeRepository;
import eapli.ecafeteria.persistence.KitchenAlertRepository;
import eapli.ecafeteria.persistence.LotRepository;
import eapli.ecafeteria.persistence.MaterialRepository;
import eapli.ecafeteria.persistence.MealMenuRepository;
import eapli.ecafeteria.persistence.MealRepository;
import eapli.ecafeteria.persistence.MealReservationRepository;
import eapli.ecafeteria.persistence.MealTypeRepository;
import eapli.ecafeteria.persistence.OrganicUnitRepository;
import eapli.ecafeteria.persistence.RepositoryFactory;
import eapli.ecafeteria.persistence.SignupRequestRepository;
import eapli.ecafeteria.persistence.UserRepository;
import eapli.framework.persistence.repositories.TransactionalContext;
import eapli.framework.persistence.repositories.impl.jpa.JpaTransactionalContext;
import eapli.ecafeteria.persistence.ShiftRepository;

/**
 *
 * Created by nuno on 21/03/16.
 */
public class JpaRepositoryFactory implements RepositoryFactory {

    @Override
    public UserRepository users(TransactionalContext autoTx) {
        return new JpaUserRepository(autoTx);
    }

    @Override
    public DishTypeRepository dishTypes() {
        return new JpaDishTypeRepository();
    }

    @Override
    public OrganicUnitRepository organicUnits() {
        return new JpaOrganicUnitRepository();
    }

    @Override
    public JpaCafeteriaUserRepository cafeteriaUsers(TransactionalContext autoTx) {
        return new JpaCafeteriaUserRepository(autoTx);
    }

    @Override
    public SignupRequestRepository signupRequests(TransactionalContext autoTx) {
        return new JpaSignupRequestRepository(autoTx);
    }

    @Override
    public DishRepository dishes() {
        return new JpaDishRepository();
    }

    @Override
    public MaterialRepository materials() {
        return new JpaMaterialRepository();
    }

    @Override
    public MealRepository meals() {
        return new JpaMealRepository();
    }

    @Override
    public MealTypeRepository mealTypes() {
        return new JpaMealTypeRepository();
    }

    @Override
    public AllergenRepository allergens() {
        return new JpaAllergenRepository();
    }

    @Override
    public CookedMealRepository cookedMeal() {
        return new JpaCookedMealRepository();
    }

    @Override
    public MealReservationRepository mealsReservations(TransactionalContext autoTx) {
        return new JpaMealReservationRepository(autoTx);
    }

    @Override
    public ShiftRepository shiftRepository() {
        return new JpaShiftRepository();
    }

    @Override
    public TransactionalContext buildTransactionalContext() {
        return new JpaTransactionalContext(Application.settings().getPersistenceUnitName());
    }

    @Override
    public BankAccountRepository accounts(TransactionalContext autoTx) {
        return new JpaBankAccountTypeRepository(autoTx);
    }

    @Override
    public CashRegisterRepository cashRegister(TransactionalContext autoTx) {
        return new JpaCashRegisterRepository(autoTx);
    }

    @Override
    public LotRepository lots() {
        return new JpaLotRepository();
    }

    @Override
    public MealMenuRepository mealMenu() {
        return new JpaMealMenuRepository();
    }

    @Override
    public KitchenAlertRepository kitchenAlerts() {
        return new JpaKitchenAlertRepository();
    }
}
