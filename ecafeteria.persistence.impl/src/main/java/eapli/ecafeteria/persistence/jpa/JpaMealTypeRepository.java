/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package eapli.ecafeteria.persistence.jpa;

import eapli.ecafeteria.domain.meals.MealType;
import eapli.ecafeteria.persistence.MealTypeRepository;
import eapli.framework.domain.Designation;

/**
 *
 * @author bfgomes
 */
public class JpaMealTypeRepository extends CafeteriaJpaRepositoryBase<MealType, String> implements MealTypeRepository {

    @Override
    public MealType findByName(String name) {
        return matchOne("e.name=:name", "name", name);
    }

}
