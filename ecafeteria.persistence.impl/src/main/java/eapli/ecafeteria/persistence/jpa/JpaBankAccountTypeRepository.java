/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package eapli.ecafeteria.persistence.jpa;

import eapli.ecafeteria.Application;
import eapli.ecafeteria.domain.account.Account;
import eapli.ecafeteria.persistence.BankAccountRepository;
import eapli.framework.persistence.repositories.TransactionalContext;
import eapli.framework.persistence.repositories.impl.jpa.JpaAutoTxRepository;

/**
 *
 * @author Alexandre Gomes e André Chaves
 */
public class JpaBankAccountTypeRepository extends JpaAutoTxRepository<Account, String> implements BankAccountRepository {

    public JpaBankAccountTypeRepository(TransactionalContext autoTx) {
        super(Application.settings().getPersistenceUnitName(), autoTx);
    }

    @Override
    public Account findByBankAccount(String BankAccount) {
         return findOne(BankAccount).get();
    }
    
}
