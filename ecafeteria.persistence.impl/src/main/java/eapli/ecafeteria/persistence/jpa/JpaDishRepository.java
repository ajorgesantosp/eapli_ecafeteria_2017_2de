package eapli.ecafeteria.persistence.jpa;

import eapli.ecafeteria.domain.meals.Dish;
import eapli.ecafeteria.domain.meals.DishType;
import eapli.ecafeteria.persistence.DishRepository;
import eapli.framework.domain.Designation;
import java.util.HashMap;
import java.util.Map;

/**
 *
 * @author Jorge Santos ajs@isep.ipp.pt 02/04/2016
 */
class JpaDishRepository extends CafeteriaJpaRepositoryBase<Dish, Designation> implements DishRepository {

    @Override
    public Dish findByName(Designation name) {
        // TODO use parameters instead of string concatenation
         return matchOne("e.name=:name", "name", name);
    }

    @Override
    public Iterable<Dish> findByDishType(DishType dishType) {
        Map<String, Object> args = new HashMap<>();
        args.put("dishType", dishType);
        
        return match("e.dishType=:dishType", args);
    }
}
