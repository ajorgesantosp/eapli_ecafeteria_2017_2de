/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package eapli.ecafeteria.persistence.jpa;

import eapli.ecafeteria.domain.MealMenu.MealMenu;
import eapli.ecafeteria.domain.MealMenu.MealMenuState;
import eapli.ecafeteria.persistence.MealMenuRepository;
import java.util.HashMap;
import java.util.Map;

/**
 *
 * @author João Sabugueiro<1150981@isep.ipp.pt>
 */
public class JpaMealMenuRepository extends CafeteriaJpaRepositoryBase<MealMenu, Long> implements MealMenuRepository {

    @Override
    public Iterable<MealMenu> allInProgress() {
        Map<String, Object> args = new HashMap<>();
        args.put("state", MealMenuState.IN_PROGRESS);
        return match("e.state=:state", args);
    }

}
