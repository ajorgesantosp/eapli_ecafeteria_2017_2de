/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package eapli.ecafeteria.persistence.jpa;

import eapli.ecafeteria.domain.MealMenu.MealMenu;
import eapli.ecafeteria.domain.meals.Meal;
import eapli.ecafeteria.domain.meals.MealType;
import eapli.ecafeteria.persistence.MealRepository;
import eapli.framework.domain.range.TimePeriod;
import eapli.util.DateTime;
import java.util.Calendar;
import java.util.Date;
import java.util.HashMap;
import java.util.Map;

/**
 *
 * @author Bruno Gomes <1151164@isep.ipp.pt>, Nuno Brito <1151173@isep.ipp.pt>
 */
public class JpaMealRepository extends CafeteriaJpaRepositoryBase<Meal, Long> implements MealRepository {

    @Override
    public Iterable<Meal> findByTimePeriod(TimePeriod timePeriod) {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    public Iterable<Meal> findByDay(Calendar day) {
        Map<String, Object> args = new HashMap<>();
        args.put("day", day);
        return match("e.day=:day", args);
    }

    @Override
    public Iterable<Meal> findByDayAndMealType(MealType mealType, Calendar day) {
        Map<String, Object> args = new HashMap<>();
        args.put("day", day);
        args.put("mealType", mealType);
        return match("e.day=:day AND e.mealType=:mealType", args);
    }
    
    
    @Override
    public Iterable<Meal> findFromDay(Calendar day) {
        Map<String, Object> args = new HashMap<>();
        args.put("day", day);
        return match("e.day>=:day", args);
    }

    @Override
    public Iterable<Meal> mealsOfMealMenu(MealMenu mealMenu) {
        Map<String, Object> args = new HashMap<>();
        args.put("mealMenu", mealMenu);
        
        return match("e.mealMenu=:mealMenu", args);
    }
    
}
