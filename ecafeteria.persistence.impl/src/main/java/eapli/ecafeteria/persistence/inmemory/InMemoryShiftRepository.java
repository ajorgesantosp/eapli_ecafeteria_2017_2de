/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package eapli.ecafeteria.persistence.inmemory;

import eapli.ecafeteria.domain.cafeteria.Shift;
import eapli.framework.persistence.repositories.impl.inmemory.InMemoryRepositoryWithLongPK;
import eapli.ecafeteria.persistence.ShiftRepository;

/**
 *
 * @author Miguel
 */
public class InMemoryShiftRepository extends InMemoryRepositoryWithLongPK<Shift> implements ShiftRepository {

    public InMemoryShiftRepository() {
    }
    
}
