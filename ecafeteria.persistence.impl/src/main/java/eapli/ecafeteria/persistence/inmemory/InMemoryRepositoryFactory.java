package eapli.ecafeteria.persistence.inmemory;

//import eapli.ecafeteria.bootstrapers.ECafeteriaBootstraper;
import eapli.ecafeteria.bootstrapers.ECafeteriaBootstraper;
import eapli.ecafeteria.persistence.AllergenRepository;
import eapli.ecafeteria.persistence.BankAccountRepository;
import eapli.ecafeteria.persistence.CafeteriaUserRepository;
import eapli.ecafeteria.persistence.CashRegisterRepository;
import eapli.ecafeteria.persistence.CookedMealRepository;
import eapli.ecafeteria.persistence.DishRepository;
import eapli.ecafeteria.persistence.DishTypeRepository;
import eapli.ecafeteria.persistence.KitchenAlertRepository;
import eapli.ecafeteria.persistence.LotRepository;
import eapli.ecafeteria.persistence.MaterialRepository;
import eapli.ecafeteria.persistence.MealMenuRepository;
import eapli.ecafeteria.persistence.MealRepository;
import eapli.ecafeteria.persistence.MealReservationRepository;
import eapli.ecafeteria.persistence.MealTypeRepository;
import eapli.ecafeteria.persistence.OrganicUnitRepository;
import eapli.ecafeteria.persistence.RepositoryFactory;
import eapli.ecafeteria.persistence.SignupRequestRepository;
import eapli.ecafeteria.persistence.UserRepository;
import eapli.framework.persistence.repositories.TransactionalContext;
import eapli.ecafeteria.persistence.ShiftRepository;

/**
 *
 * Created by nuno on 20/03/16.
 */
public class InMemoryRepositoryFactory implements RepositoryFactory {

    static {
        // only needed because of the in memory persistence
        new ECafeteriaBootstraper().execute();
    }

    @Override
    public UserRepository users(TransactionalContext tx) {
        return new InMemoryUserRepository();
    }

    @Override
    public DishTypeRepository dishTypes() {
        return new InMemoryDishTypeRepository();
    }

    @Override
    public OrganicUnitRepository organicUnits() {
        return new InMemoryOrganicUnitRepository();
    }

    @Override
    public CafeteriaUserRepository cafeteriaUsers(TransactionalContext tx) {

        return new InMemoryCafeteriaUserRepository();
    }

    @Override
    public SignupRequestRepository signupRequests(TransactionalContext tx) {
        return new InMemorySignupRequestRepository();
    }

    @Override
    public DishRepository dishes() {
        return new InMemoryDishRepository();
    }

    @Override
    public MaterialRepository materials() {
        return new InMemoryMaterialRepository();
    }

    @Override
    public TransactionalContext buildTransactionalContext() {
        // in memory does not support transactions...
        return null;
    }

    @Override
    public MealRepository meals() {
        return new InMemoryMealRepository();
    }

    @Override
    public MealTypeRepository mealTypes() {
        return new InMemoryMealTypeRepository();
    }

    @Override
    public AllergenRepository allergens() {
        return new InMemoryAllergenRepository();
    }

    @Override
    public CookedMealRepository cookedMeal() {
        return new inMemoryCookedMealRepository();
    }

    @Override
    public MealReservationRepository mealsReservations(TransactionalContext autoTx) {
        return new InMemoryMealReservationRepository();
    }

    @Override
    public ShiftRepository shiftRepository() {
        return new InMemoryShiftRepository();
    }

    @Override
    public BankAccountRepository accounts(TransactionalContext autoTx) {
        return new InMemoryBankAccountRepository();
    }

    @Override
    public CashRegisterRepository cashRegister(TransactionalContext autoTx) {
        return new InMemoryCashRegisterRepository();
    }

    @Override
    public MealMenuRepository mealMenu() {
        return new inMemoryMealMenu();
    }


    @Override
    public LotRepository lots() {
        return new InMemoryLotRepository();
    }

    @Override
    public KitchenAlertRepository kitchenAlerts() {
        return new InMemoryKitchenAlertRepository();
    }

}
