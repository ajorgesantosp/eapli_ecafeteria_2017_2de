package eapli.ecafeteria.persistence.inmemory;

import eapli.ecafeteria.domain.meals.MealType;
import eapli.ecafeteria.persistence.MealTypeRepository;
import eapli.framework.persistence.repositories.impl.inmemory.InMemoryRepository;

/**
 * 
 * @author Bruno Gomes <1151164@isep.ipp.pt>, Nuno Brito <1151173@isep.ipp.pt>
 */
public class InMemoryMealTypeRepository extends InMemoryRepository<MealType, String> implements MealTypeRepository {

    @Override
    protected String newPK(MealType entity) {
        return entity.id();
    }

    @Override
    public MealType findByName(String name) {
        return matchOne(e -> e.id().equals(name));
    }
    
}
