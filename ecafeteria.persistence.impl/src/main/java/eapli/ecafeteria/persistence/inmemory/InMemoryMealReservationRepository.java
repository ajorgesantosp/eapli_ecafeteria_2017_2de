/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package eapli.ecafeteria.persistence.inmemory;

import eapli.ecafeteria.domain.cafeteria.CafeteriaUser;
import eapli.ecafeteria.domain.mealReservation.Reservation;
import eapli.ecafeteria.domain.meals.MealType;
import eapli.ecafeteria.persistence.MealReservationRepository;
import eapli.framework.persistence.repositories.impl.inmemory.InMemoryRepositoryWithLongPK;
import java.util.Calendar;

/**
 *
 * @author Jorge Pereira 1141216 \ Luís Magalhães 1141174
 */
public class InMemoryMealReservationRepository extends InMemoryRepositoryWithLongPK<Reservation> implements MealReservationRepository {

    @Override
    public Iterable<Reservation> findByDay(Calendar day, MealType mt) {
        //TODO
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    public Reservation findFromHashCode(Integer hashCode) {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    public Iterable<Reservation> reservationByUser(CafeteriaUser cafUser) {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    public Iterable<Reservation> findReservesNDaysFromUser(Calendar day, CafeteriaUser user) {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    public Reservation findReserveCreatedHashDay(Integer hashCode, Calendar day, MealType mt) {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

}
