package eapli.ecafeteria.persistence.inmemory;

import eapli.ecafeteria.domain.MealMenu.MealMenu;
import eapli.ecafeteria.domain.meals.Meal;
import eapli.ecafeteria.domain.meals.MealType;
import eapli.ecafeteria.persistence.MealRepository;
import eapli.framework.domain.range.TimePeriod;
import eapli.framework.persistence.repositories.impl.inmemory.InMemoryRepository;
import java.util.Calendar;

/**
 * 
 * @author Bruno Gomes <1151164@isep.ipp.pt>, Nuno Brito <1151173@isep.ipp.pt>
 */
public class InMemoryMealRepository extends InMemoryRepository<Meal, Long> implements MealRepository {

    @Override
    protected Long newPK(Meal entity) {
        return entity.id();
    }

    @Override
    public Iterable<Meal> findByTimePeriod(TimePeriod timePeriod) {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    public Iterable<Meal> findByDay(Calendar day) {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    public Iterable<Meal> findFromDay(Calendar day) {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    public Iterable<Meal> findByDayAndMealType(MealType mealType, Calendar day) {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    public Iterable<Meal> mealsOfMealMenu(MealMenu mealMenu) {
        return match(e -> e.mealMenu().equals(mealMenu));
    }
}
