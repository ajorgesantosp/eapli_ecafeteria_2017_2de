/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package eapli.ecafeteria.persistence.inmemory;

import eapli.ecafeteria.domain.account.Account;
import eapli.ecafeteria.persistence.BankAccountRepository;
import eapli.framework.persistence.repositories.impl.inmemory.InMemoryRepository;

/**
 *
 * @author Alexandre Gomes e André Chaves
 */
public class InMemoryBankAccountRepository extends InMemoryRepository<Account, String> implements BankAccountRepository {

    public InMemoryBankAccountRepository() {
    }

    @Override
    public Account findByBankAccount(String BankAccount) {
        return findOne(BankAccount).get();
    }

    @Override
    protected String newPK(Account entity) {
        return  entity.getBankAccount();
    }
    
}
