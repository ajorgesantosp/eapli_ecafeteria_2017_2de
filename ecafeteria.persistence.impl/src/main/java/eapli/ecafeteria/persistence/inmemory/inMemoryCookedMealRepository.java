/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package eapli.ecafeteria.persistence.inmemory;

import eapli.ecafeteria.domain.kitchen.CookedMeal;
import eapli.ecafeteria.domain.kitchen.Lot;
import eapli.ecafeteria.domain.meals.Meal;
import eapli.ecafeteria.persistence.CookedMealRepository;
import eapli.framework.persistence.repositories.impl.inmemory.InMemoryRepository;

/**
 *
 * @author Rafael Delgado <Rafael.delgado at Rafael.org> && João
 * Sabugueiro<1150981@isep.ipp.pt>
 */
public class inMemoryCookedMealRepository extends InMemoryRepository<CookedMeal, Meal> implements CookedMealRepository {

    @Override
    protected Meal newPK(CookedMeal entity) {
        return entity.id();
    }

    @Override
    public Iterable<CookedMeal> findByLot(Lot lot) {
        return match(e -> e.containsLot(lot));
    }

}
