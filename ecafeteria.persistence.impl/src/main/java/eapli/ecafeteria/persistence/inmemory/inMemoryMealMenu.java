/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package eapli.ecafeteria.persistence.inmemory;

import eapli.ecafeteria.domain.MealMenu.MealMenu;
import eapli.ecafeteria.persistence.MealMenuRepository;
import eapli.framework.persistence.repositories.impl.inmemory.InMemoryRepository;

/**
 *
 * @author João Sabugueiro<1150981@isep.ipp.pt>
 */
public class inMemoryMealMenu extends InMemoryRepository<MealMenu, Long> implements MealMenuRepository {

    @Override
    protected Long newPK(MealMenu entity) {
        return entity.mealMenuID();
    }

    @Override
    public Iterable<MealMenu> allInProgress() {
        return match(e -> e.isInProgress());
    }

}
