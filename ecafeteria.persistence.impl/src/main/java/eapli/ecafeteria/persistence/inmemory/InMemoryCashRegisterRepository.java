/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package eapli.ecafeteria.persistence.inmemory;

import eapli.ecafeteria.domain.cafeteria.CashRegister;
import eapli.ecafeteria.persistence.CashRegisterRepository;
import eapli.framework.persistence.repositories.impl.inmemory.InMemoryRepository;

/**
 *
 * @author Jorge Pereira 1141216 \ Luís Magalhães 1141174
 */
public class InMemoryCashRegisterRepository extends InMemoryRepository<CashRegister,Long> implements CashRegisterRepository{

    @Override
    protected Long newPK(CashRegister entity) {
        return entity.cashRegisterID();
    }

    @Override
    public CashRegister find() {
        //TODO AT THE MOMENT THERE ONLY IS ONE CASH REGISTER
        return first();
    }
    
}
