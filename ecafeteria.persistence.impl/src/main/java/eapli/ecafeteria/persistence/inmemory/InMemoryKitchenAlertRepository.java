package eapli.ecafeteria.persistence.inmemory;

import eapli.ecafeteria.domain.kitchen.KitchenAlert;
import eapli.ecafeteria.persistence.KitchenAlertRepository;
import eapli.framework.domain.Designation;
import eapli.framework.persistence.repositories.impl.inmemory.InMemoryRepository;

/**
 * 
 * @author Bruno Gomes <1151164@isep.ipp.pt>, Nuno Brito <1151173@isep.ipp.pt>
 */
public class InMemoryKitchenAlertRepository extends InMemoryRepository<KitchenAlert, Designation> implements KitchenAlertRepository {

    @Override
    protected Designation newPK(KitchenAlert entity) {
        return entity.id();
    }
    
    @Override
    public KitchenAlert findByName(Designation name) {
        return matchOne(e -> e.id().equals(name));
    }
    
}
