/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package eapli.ecafeteria.backoffice.consoleapp.presentation.MealMenuManagement;

import eapli.ecafeteria.application.MealMenu.PublishMealMenuController;
import eapli.ecafeteria.domain.MealMenu.MealMenu;
import eapli.framework.application.Controller;
import eapli.framework.presentation.console.AbstractUI;
import eapli.framework.presentation.console.SelectWidget;

/**
 *
 * @author joaos_000
 */
public class PublishMealMenuUI extends AbstractUI{

     private final PublishMealMenuController theController = new PublishMealMenuController();

     protected Controller controller() {
          return this.theController;
     }

     @Override
     protected boolean doShow() {
         Iterable<MealMenu> menusInProgress = theController.allMealMenuInProgress();
         if (!menusInProgress.iterator().hasNext()) {
             System.out.println("No menus in progress");
             return false;
         }
         
         SelectWidget<MealMenu> selector = new SelectWidget<>("Meal Menus", menusInProgress, new MealMenuPrinter());
         selector.show();
         
         if(selector.selectedOption() == 0){
             return false;
         }
         
         MealMenu mealMenu = selector.selectedElement();
         
         this.theController.publishMealMenu(mealMenu);
          
         return false;
     }

     @Override
     public String headline() {
          return "Publish Meal Menu";
     }
    
}
