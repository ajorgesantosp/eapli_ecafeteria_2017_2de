/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package eapli.ecafeteria.backoffice.consoleapp.presentation.kitchen;

import eapli.ecafeteria.application.kitchen.SearchByLotController;
import eapli.ecafeteria.backoffice.consoleapp.presentation.meals.MealPrinter;
import eapli.ecafeteria.domain.kitchen.CookedMeal;
import eapli.ecafeteria.domain.kitchen.Lot;
import eapli.framework.application.Controller;
import eapli.framework.presentation.console.AbstractUI;
import eapli.framework.presentation.console.SelectWidget;

/**
 *
 * @author calve
 */
public class SearchByLotUI extends AbstractUI {

    private final SearchByLotController theController = new SearchByLotController();

    protected Controller controller() {
        return this.theController;
    }

    @Override
    protected boolean doShow() {

        final Iterable<Lot> lots = this.theController.lots();
        final SelectWidget<Lot> selector = new SelectWidget<>("Lots::", lots, new LotPrinter());
        selector.show();
        final Lot theLot = selector.selectedElement();

        final Iterable<CookedMeal> meals = this.theController.meals(theLot);

        System.out.println("Meals:");
        for (CookedMeal m : meals) {
            new MealPrinter().visit(m.meal());
        }

        return true;
    }

    @Override
    public String headline() {
        return "Search By Lot";
    }

}
