/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package eapli.ecafeteria.backoffice.consoleapp.presentation.MealMenuManagement;

import eapli.framework.actions.Action;

/**
 *
 * @author João Sabugueiro<1150981@isep.ipp.pt>
 */
public class CreateEditMealMenuAction implements Action{

    @Override
    public boolean execute() {
        return new CreateEditMealMenuUI().doShow();
    }
     
}
