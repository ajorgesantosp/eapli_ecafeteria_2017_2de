package eapli.ecafeteria.backoffice.consoleapp.presentation.meals;

import eapli.ecafeteria.application.meals.RegisterDishController;
import eapli.ecafeteria.domain.meals.Allergen;
import eapli.ecafeteria.domain.meals.DishType;
import eapli.framework.application.Controller;
import eapli.framework.persistence.DataConcurrencyException;
import eapli.framework.persistence.DataIntegrityViolationException;
import eapli.framework.presentation.console.AbstractUI;
import eapli.framework.presentation.console.SelectWidget;
import eapli.util.io.Console;
import java.util.ArrayList;

/**
 *
 * @author Jorge Santos ajs@isep.ipp.pt
 */
public class RegisterDishUI extends AbstractUI {

    private final RegisterDishController theController = new RegisterDishController();

    protected Controller controller() {
        return this.theController;
    }

    @Override
    protected boolean doShow() {
        final Iterable<DishType> dishTypes = this.theController.dishTypes();

        final SelectWidget<DishType> selector = new SelectWidget<>("Dish types:", dishTypes, new DishTypePrinter());
        selector.show();
        final DishType theDishType = selector.selectedElement();

        final String name = Console.readLine("Name");

        final NutricionalInfoDataWidget nutricionalData = new NutricionalInfoDataWidget();

        nutricionalData.show();

        final double price = Console.readDouble("Price");

        System.out.println("Allergens");

        final Iterable<Allergen> allergens = this.theController.allergens();

        ArrayList<Allergen> dishAllergens = new ArrayList<>();
        final SelectWidget<Allergen> allergenSelector = new SelectWidget<>("allergens", allergens, new AllergenPrinter());
        do {
            allergenSelector.show();
            dishAllergens.add(allergenSelector.selectedElement());
        } while (allergenSelector.selectedElement() != null);

        dishAllergens.remove(dishAllergens.size() - 1);

//        for(Allergen a : dishAllergens){
//            System.out.println(a.name().toString());
//        }
        try {
            this.theController.registerDish(theDishType, name, nutricionalData.calories(), nutricionalData.salt(),
                    price, dishAllergens);
        } catch (final DataIntegrityViolationException | DataConcurrencyException e) {
            System.out.println("You tried to enter a dish which already exists in the database.");
        }

        return true;
    }

    @Override
    public String headline() {
        return "Register Dish";
    }
}
