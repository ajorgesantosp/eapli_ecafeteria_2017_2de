/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package eapli.ecafeteria.backoffice.consoleapp.presentation.MealMenuManagement;

import eapli.framework.actions.Action;

/**
 *
 * @author joaos_000
 */
public class PublishMealMenuAction implements Action{

    @Override
    public boolean execute() {
        return new PublishMealMenuUI().doShow();
    }
    
}
