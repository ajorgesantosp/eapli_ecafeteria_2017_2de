package eapli.ecafeteria.backoffice.consoleapp.presentation.kitchen;

import eapli.ecafeteria.domain.kitchen.KitchenAlert;
import eapli.framework.visitor.Visitor;

/**
 *
 * @author Bruno Gomes <1151164@isep.ipp.pt>, Nuno Brito <1151173@isep.ipp.pt>
 */
public class KitchenAlertPrinter implements Visitor<KitchenAlert>{

     @Override
     public void visit(KitchenAlert visitee) {
          System.out.printf("%-10s%-30d\n", visitee.id(), visitee.limit());
     }
     
}
