/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package eapli.ecafeteria.backoffice.consoleapp.presentation.meals;

import eapli.cafeteria.consoleapp.presentation.meals.MealTypePrinter;
import eapli.ecafeteria.application.meals.PublishMealController;
import eapli.ecafeteria.domain.meals.Dish;
import eapli.ecafeteria.domain.meals.Meal;
import eapli.ecafeteria.domain.meals.MealType;
import eapli.framework.application.Controller;
import eapli.framework.persistence.DataConcurrencyException;
import eapli.framework.persistence.DataIntegrityViolationException;
import eapli.framework.presentation.console.AbstractUI;
import eapli.framework.presentation.console.SelectWidget;
import eapli.util.io.Console;

/**
 *
 * @author Bruno Gomes <1151164@isep.ipp.pt>, Nuno Brito <1151173@isep.ipp.pt>
 */
public class PublishMealUI extends AbstractUI {

     private final PublishMealController theController = new PublishMealController();

     protected Controller controller() {
          return this.theController;
     }

     @Override
     protected boolean doShow() {

          final Iterable<Dish> dishes = this.theController.dishes();
          final Iterable<MealType> mealTypes = this.theController.mealTypes();
          if (!dishes.iterator().hasNext()) {
               System.out.println("There are no registered Dishes");
               return false;
          }
          if (!mealTypes.iterator().hasNext()) {
               System.out.println("There are no registered MealType");
               return false;
          }

          final String day = Console.readLine("Day (dd-mm-yyyy)");

          final SelectWidget<MealType> selectorMealType = new SelectWidget<>("mealTypes", mealTypes, new MealTypePrinter());
          selectorMealType.show();
          final MealType theMealType = selectorMealType.selectedElement();

          final SelectWidget<Dish> selectorDish = new SelectWidget<>("dishes", dishes, new DishPrinter());
          selectorDish.show();
          final Dish theDish = selectorDish.selectedElement();

          try {
               Meal theMeal = this.theController.createMeal(day, theMealType, theDish);
               this.theController.publishMeal(theMeal);
          } catch (final DataIntegrityViolationException | DataConcurrencyException e) {
               System.out.println("You tried to enter a dish which already exists in the database.");
          } catch (Exception ex) {
               System.out.println("You entered a invalid date.");
          }
          return false;
     }

     @Override
     public String headline() {
          return "Publish Meal";
     }

}
