/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package eapli.ecafeteria.backoffice.consoleapp.presentation.kitchen;

import eapli.ecafeteria.application.kitchen.RegisterMealLotsController;
import eapli.ecafeteria.domain.kitchen.CookedMeal;
import eapli.ecafeteria.domain.kitchen.Lot;
import eapli.framework.application.Controller;
import eapli.framework.persistence.DataConcurrencyException;
import eapli.framework.persistence.DataIntegrityViolationException;
import eapli.framework.presentation.console.AbstractUI;
import eapli.framework.presentation.console.SelectWidget;
import java.util.ArrayList;

/**
 *
 * @author calve
 */
public class RegisterMealLotsUI extends AbstractUI {

    private final RegisterMealLotsController theController = new RegisterMealLotsController();

    @Override
    protected boolean doShow() {

        final Iterable<CookedMeal> meals = this.theController.meals();
        final Iterable<Lot> lots = this.theController.lots();
        final SelectWidget<CookedMeal> selector = new SelectWidget<>("Meals:", meals, new CookedMealPrinter());
        ArrayList<Lot> mealLots = new ArrayList<>();

        selector.show();
        final CookedMeal theMeal = selector.selectedElement();

        if (theMeal == null) {
            System.out.println("There are no cooked meals!");
            return false;
        }

        final SelectWidget<Lot> lotSelector = new SelectWidget<>("Lots:", lots, new LotPrinter());
        while (true) {
            lotSelector.show();
            if (lotSelector.selectedElement() == null) {
                break;
            } else {
                mealLots.add(lotSelector.selectedElement());
            }
        }

        try {
            this.theController.registerMealLots(theMeal, mealLots);
        } catch (final DataConcurrencyException | DataIntegrityViolationException ex) {
            System.out.println("Error updating data!");
            return false;
        }
        System.out.println("Sucess updating data!");
        return true;
    }

    protected Controller controller() {
        return this.theController;
    }

    @Override
    public String headline() {
        return "Register Meal Lots";
    }

}
