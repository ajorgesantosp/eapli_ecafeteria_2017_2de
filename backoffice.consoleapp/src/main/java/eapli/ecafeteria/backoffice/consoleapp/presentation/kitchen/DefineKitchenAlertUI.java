/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package eapli.ecafeteria.backoffice.consoleapp.presentation.kitchen;

import eapli.ecafeteria.application.kitchen.DefineKitchenAlertController;
import eapli.ecafeteria.domain.kitchen.KitchenAlert;
import eapli.framework.application.Controller;
import eapli.framework.persistence.DataConcurrencyException;
import eapli.framework.persistence.DataIntegrityViolationException;
import eapli.framework.presentation.console.AbstractUI;
import eapli.framework.presentation.console.SelectWidget;
import eapli.util.io.Console;

/**
 *
 * @author Bruno Gomes <1151164@isep.ipp.pt>, Nuno Brito <1151173@isep.ipp.pt>
 */
public class DefineKitchenAlertUI extends AbstractUI  {

    private final DefineKitchenAlertController theController = new DefineKitchenAlertController();
    
    protected Controller controller() {
        return this.theController;
    }
    
    @Override
    protected boolean doShow() {
        
        final Iterable<KitchenAlert> alerts = this.theController.kitchenAlerts();
        if (!alerts.iterator().hasNext()) {
            System.out.println("There are no registered kitchen alerts.");
            return false;
        }
        
        final SelectWidget<KitchenAlert> selectorKitchenAlert = new SelectWidget<>("Kitchen Alerts",alerts, new KitchenAlertPrinter());
        selectorKitchenAlert.show();
        final KitchenAlert theKitchenAlert = selectorKitchenAlert.selectedElement();
        
        if (theKitchenAlert == null) {
            System.out.println("Kitchen alert needed.");
            return false;
        }
        
        final int limit = Console.readInteger("Introduza o limite:");
        
        try {
            this.theController.updateLimit(theKitchenAlert, limit);
        } catch (final DataIntegrityViolationException | DataConcurrencyException e) {
            System.out.println("Error updating kitchen alert.");
        } catch (final IllegalStateException s) {
            System.out.println(s.getMessage());
        }
        
        return false;
    }

    @Override
    public String headline() {
        return "Define Kitchen Alert";
    }
    
    
    
}
