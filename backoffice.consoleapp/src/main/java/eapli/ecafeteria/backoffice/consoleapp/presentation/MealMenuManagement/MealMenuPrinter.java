/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package eapli.ecafeteria.backoffice.consoleapp.presentation.MealMenuManagement;

import eapli.ecafeteria.domain.MealMenu.MealMenu;
import eapli.framework.visitor.Visitor;
import java.text.SimpleDateFormat;

/**
 *
 * @author João Sabugueiro<1150981@isep.ipp.pt>
 */
public class MealMenuPrinter implements Visitor<MealMenu>{

    @Override
    public void visit(MealMenu visitee) {
        SimpleDateFormat sdf = new SimpleDateFormat("dd-MM-yyyy");
        
        System.out.printf("Week from %s to %s\n", sdf.format(visitee.StartDate().getTime()),sdf.format(visitee.EndDate().getTime()) );
    }
     
}
