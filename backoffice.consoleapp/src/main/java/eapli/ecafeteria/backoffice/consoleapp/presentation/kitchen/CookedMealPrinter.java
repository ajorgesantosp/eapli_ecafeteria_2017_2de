/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package eapli.ecafeteria.backoffice.consoleapp.presentation.kitchen;

import eapli.ecafeteria.domain.kitchen.CookedMeal;
import eapli.framework.visitor.Visitor;
import eapli.util.DateTime;

/**
 *
 * @author Rafael Delgado <Rafael.delgado at Rafael.org>
 */
public class CookedMealPrinter implements Visitor<CookedMeal> {

    @Override
    public void visit(CookedMeal visitee) {
        System.out.printf("%-10s%-30s%-30s%-30s\n", visitee.id(), visitee.meal().dish().name(), visitee.quantity(), DateTime.format(visitee.meal().day(), "dd-MM-yyyy"));
    }

}
