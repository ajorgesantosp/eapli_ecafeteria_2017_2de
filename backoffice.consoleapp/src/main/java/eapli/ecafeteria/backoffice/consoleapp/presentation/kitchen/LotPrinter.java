/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package eapli.ecafeteria.backoffice.consoleapp.presentation.kitchen;

import eapli.ecafeteria.domain.kitchen.Lot;
import eapli.framework.visitor.Visitor;

/**
 *
 * @author calve
 */
public class LotPrinter implements Visitor<Lot> {

    @Override
    public void visit(Lot visitee) {
        System.out.printf("%-15s%-30s\n", visitee.code(), visitee.material().description());
    }

}
