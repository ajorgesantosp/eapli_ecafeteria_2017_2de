/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package eapli.ecafeteria.backoffice.consoleapp.presentation.meals;

import eapli.ecafeteria.domain.meals.MealType;
import eapli.framework.visitor.Visitor;

/**
 *
 * @author Bruno Gomes <1151164@isep.ipp.pt>, Nuno Brito <1151173@isep.ipp.pt>
 */
public class MealTypePrinter implements Visitor<MealType>  {

    @Override
    public void visit(MealType visitee) {
        System.out.printf("%s\n", visitee.id());
    }
    
    @Override
    public void beforeVisiting(MealType visitee) {
        // nothing to do
    }

    @Override
    public void afterVisiting(MealType visitee) {
        // nothing to do
    }
}
