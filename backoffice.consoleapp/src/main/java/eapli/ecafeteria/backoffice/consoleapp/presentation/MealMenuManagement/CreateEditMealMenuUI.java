/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package eapli.ecafeteria.backoffice.consoleapp.presentation.MealMenuManagement;

import eapli.ecafeteria.application.MealMenu.CreateEditMealMenuController;
import eapli.ecafeteria.backoffice.consoleapp.presentation.meals.DishPrinter;
import eapli.ecafeteria.backoffice.consoleapp.presentation.meals.DishTypePrinter;
import eapli.ecafeteria.backoffice.consoleapp.presentation.meals.MealPrinter;
import eapli.ecafeteria.backoffice.consoleapp.presentation.meals.MealTypePrinter;
import eapli.ecafeteria.domain.MealMenu.MealMenu;
import eapli.ecafeteria.domain.meals.Dish;
import eapli.ecafeteria.domain.meals.DishType;
import eapli.ecafeteria.domain.meals.Meal;
import eapli.ecafeteria.domain.meals.MealType;
import eapli.framework.persistence.DataConcurrencyException;
import eapli.framework.persistence.DataIntegrityViolationException;
import eapli.framework.presentation.console.AbstractUI;
import eapli.framework.presentation.console.Menu;
import eapli.framework.presentation.console.MenuItem;
import eapli.framework.presentation.console.MenuRenderer;
import eapli.framework.presentation.console.SelectWidget;
import eapli.framework.presentation.console.VerticalMenuRenderer;
import eapli.framework.visitor.Visitor;
import eapli.util.DateTime;
import eapli.util.io.Console;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.GregorianCalendar;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 *
 * @author João Sabugueiro<1150981@isep.ipp.pt>
 */
public class CreateEditMealMenuUI extends AbstractUI {

     private final CreateEditMealMenuController controller = new CreateEditMealMenuController();

     @Override
     protected boolean doShow() {
          System.out.println("1. Create Meal Menu");
          System.out.println("2. Edit Meal Menu");
          System.out.println("0. Exit");

          int op = Console.readOption(1, 2, 0);

          try {
               switch (op) {
                    case 1:     //create meal menu
                         Calendar startDate = askDate("Start Date:");
                         Calendar endDate = askDate("End Date:");

                         MealMenu mealMenu = controller.createMealMenu(startDate, endDate);

                         //edit new meal item
                         editMealMenu(mealMenu);

                         break;

                    case 2:     //edit meal menu
                         //select meal menu
                         Iterable<MealMenu> menusInProgress = controller.getAllMenusInProgress();
                         if (!menusInProgress.iterator().hasNext()) {
                              System.out.println("No menus in progress");
                              break;
                         }

                         SelectWidget<MealMenu> selector = new SelectWidget<>("\nMenus in Progress", menusInProgress, new MealMenuPrinter());
                         selector.show();
                         MealMenu mealMenuToEdit = selector.selectedElement();

                         editMealMenu(mealMenuToEdit);

                         break;
               }

          } catch (DataConcurrencyException | DataIntegrityViolationException ex) {
               Logger.getLogger(CreateEditMealMenuUI.class.getName()).log(Level.SEVERE, null, ex);
          }

          return false;
     }

     @Override
     public String headline() {
          return "Create or Edit Meal Menus";
     }

     private void editMealMenu(MealMenu mealMenu) throws DataConcurrencyException, DataIntegrityViolationException {

          showMealMenuItens(mealMenu);

          System.out.println("1. Remove Meal");
          System.out.println("2. Add Meal");
          System.out.println("0. Exit");
          int op = Console.readOption(1, 2, 0);

          switch (op) {
               case 1:         //remove meal menu itens
                    boolean show;
                    do {
                         show = showEraseMealMenuItens(mealMenu);
                    } while (!show);

                    break;

               case 2:        // add meal menu itens

                    Calendar date = askMealMenuDate("Day", mealMenu.StartDate(), mealMenu.EndDate());

                    DishType dishType = selectDishType();

                    Dish dish = selectDish(dishType);

                    MealType mealType = selectMealType();

                    if (dish == null || mealType == null || date==null) {
                         break;
                    }

                    Meal meal = controller.registerMeal(date, mealType, dish, mealMenu);
                    break;

          }
     }

     private Calendar askDate(String dateInfo) {
          String[] dateArray;

          do {
               System.out.printf("%s '(dd-mm-yyyy)'\n", dateInfo);
               String st = Console.readLine(":");
               dateArray = st.split("-");
          } while (dateArray.length != 3);

          int day = Integer.parseInt(dateArray[0]);
          int month = Integer.parseInt(dateArray[1]);
          int year = Integer.parseInt(dateArray[2]);

          return DateTime.newCalendar(year, month, day);

     }

     private Menu buildEraseMealMenuItensMenu(MealMenu mealMenu) {
          final Menu itensMenu = new Menu();
          int counter = 1;

          for (final Meal meal : controller.mealsFromMealMenu(mealMenu)) {
               itensMenu.add(new MenuItem(counter, meal.toString(), () -> {
                    try {
                         return controller.removeMealFromMenu(meal);
                    } catch (DataIntegrityViolationException ex) {
                         Logger.getLogger(CreateEditMealMenuUI.class.getName()).log(Level.SEVERE, null, ex);
                    }
                    return false;
               }));
               counter++;
          }
          return itensMenu;
     }

     private void showMealMenuItens(MealMenu mealMenuToEdit) {
          Iterable<Meal> mealMenuItens = controller.mealsFromMealMenu(mealMenuToEdit);
          SimpleDateFormat sdf = new SimpleDateFormat("dd-MM-yyyy");

          int i = 1;
          System.out.println("--------Meals--------");
          for (Meal meal : mealMenuItens) {

               System.out.printf("%d -> %-15s | %-15s | %-15s\n", i, sdf.format(meal.day().getTime()), meal.mealType(), meal.dish().id());
               i++;
          }
          System.out.println("---------------------");
     }

     private Calendar askMealMenuDate(String info, Calendar firstDay, Calendar lastDay) {

          Calendar day;
          do {
               day = askDate(info);
          } while (day.before(firstDay) || day.after(lastDay));
          System.out.println("day:::" + day);
          return day;
     }

     private DishType selectDishType() {
          SelectWidget<DishType> selector = new SelectWidget<>("Dish Types", controller.allDishTypes(), new DishTypePrinter());
          selector.show();
          if (selector.selectedOption()==0) {
               return null;
          }
          return selector.selectedElement();
     }

     private Dish selectDish(DishType dishType) {
          SelectWidget<Dish> selector = new SelectWidget<>("Dishes", controller.getDishWithDishType(dishType), new DishPrinter());
          selector.show();
          if (selector.selectedOption()==0) {
               return null;
          }
          return selector.selectedElement();
     }

     private MealType selectMealType() {
          SelectWidget<MealType> selector = new SelectWidget<>("Meal Types", controller.allMealTypes(), new MealTypePrinter());
          selector.show();
          if (selector.selectedOption()==0) {
               return null;
          }
          return selector.selectedElement();
     }

     private boolean showEraseMealMenuItens(MealMenu mealMenu) {
          final Menu itensMenu = buildEraseMealMenuItensMenu(mealMenu);
          final MenuRenderer renderer = new VerticalMenuRenderer(itensMenu);
          return renderer.show();
     }

     
     
}
