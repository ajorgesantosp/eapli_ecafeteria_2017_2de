/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package eapli.ecafeteria.backoffice.consoleapp.presentation.kitchen;

import eapli.cafeteria.consoleapp.presentation.meals.MealPrinter;
import eapli.ecafeteria.application.kitchen.RegisterCookedMealController;
import eapli.ecafeteria.backoffice.consoleapp.presentation.meals.MealTypePrinter;
import eapli.ecafeteria.domain.meals.Meal;
import eapli.ecafeteria.domain.meals.MealType;
import eapli.framework.application.Controller;
import eapli.framework.persistence.DataConcurrencyException;
import eapli.framework.persistence.DataIntegrityViolationException;
import eapli.framework.presentation.console.AbstractUI;
import eapli.framework.presentation.console.SelectWidget;
import eapli.util.DateTime;
import eapli.util.io.Console;
import java.util.Calendar;

/**
 *
 * @author Rafael Delgado <Rafael.delgado at Rafael.org>
 */
public class RegisterCookedMealUI extends AbstractUI{

     private final RegisterCookedMealController theController = new RegisterCookedMealController();

     protected Controller controller() {
          return this.theController;
     }

     @Override
     protected boolean doShow() {
          final Iterable<MealType> mealTypes = this.theController.getAllMealType();
          final SelectWidget<MealType> mealTypeSelector = new SelectWidget<>("MealTypes",mealTypes,new MealTypePrinter());
          mealTypeSelector.show();
          final MealType theMealType = mealTypeSelector.selectedElement();

         Calendar cDay = getCurrentDay();
         
         final Iterable<Meal> meals = this.theController.getMeals(theMealType, cDay);
         final SelectWidget<Meal> mealSelector = new SelectWidget<>("Meals",meals, new MealPrinter());
         mealSelector.show();
         final Meal theMeal = mealSelector.selectedElement();
         
         if (theMeal != null) {
             final int quantity = Console.readInteger("Quantity");

             try {
                 this.theController.registerCookedMeal(theMeal, quantity);
             } catch (DataConcurrencyException | DataIntegrityViolationException ex) {
                 System.out.println("Saving cooked meal failed. ");
             }

         }
          return false;
     }

     @Override
     public String headline() {
          return "Register Cooked Meal";
     }

    Calendar getCurrentDay() {

        Calendar now = DateTime.now();

        int day = now.get(Calendar.DAY_OF_MONTH);
        int month = now.get(Calendar.MONTH);
        int year = now.get(Calendar.YEAR);

        Calendar currentDay = Calendar.getInstance();
        currentDay.set(day, month, year);

        return currentDay;
    }
}
