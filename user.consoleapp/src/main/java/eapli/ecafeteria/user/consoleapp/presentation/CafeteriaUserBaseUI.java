/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates and open the template
 * in the editor.
 */
package eapli.ecafeteria.user.consoleapp.presentation;

import eapli.ecafeteria.Application;
import eapli.ecafeteria.application.CafeteriaUserBaseController;
import eapli.framework.presentation.console.AbstractUI;

/**
 *
 * @author mcn
 */
public abstract class CafeteriaUserBaseUI extends AbstractUI {

    protected  CafeteriaUserBaseController controller() {
        return new CafeteriaUserBaseController();        
    }

    public String showBalance() {
        return "CURRENT BALANCE OF YOUR USERCARD: " + controller().balance().toString();
    }

    @Override
    public String headline() {
        return "eCAFETERIA [@" + Application.session().session().authenticatedUser().id() + "]   " ;
    }

    @Override
    public boolean show() {
        System.out.println("\n"+showBalance());
        return super.show(); //To change body of generated methods, choose Tools | Templates.
    }

    
    @Override
    protected void drawFormTitle(String title) {
        // drawFormBorder();
        final String titleBorder = BORDER.substring(0, 2) + " " + title;
        System.out.println(titleBorder);
        drawFormBorder();
    }
}
