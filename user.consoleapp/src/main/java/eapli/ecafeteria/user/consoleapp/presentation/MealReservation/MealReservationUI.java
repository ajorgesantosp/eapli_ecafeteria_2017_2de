package eapli.ecafeteria.user.consoleapp.presentation.MealReservation;

import eapli.cafeteria.consoleapp.presentation.MealReservation.ReservationPrinter;
import eapli.cafeteria.consoleapp.presentation.meals.MealForReservationPrinter;
import eapli.ecafeteria.application.MealReservation.MealReservationController;
import eapli.ecafeteria.domain.account.exceptions.NoMoneyException;
import eapli.ecafeteria.domain.mealReservation.Reservation;
import eapli.ecafeteria.domain.meals.Meal;
import eapli.ecafeteria.user.consoleapp.presentation.CafeteriaUserBaseUI;
import eapli.framework.persistence.DataConcurrencyException;
import eapli.framework.persistence.DataIntegrityViolationException;
import eapli.framework.presentation.console.SelectWidget;
import eapli.util.io.Console;

/**
 *
 * @author Jorge Pereira 1141216 \\ Luís Magalhães 1141174
 */
public class MealReservationUI extends CafeteriaUserBaseUI{

    private final MealReservationController ctrl = new MealReservationController();

    @Override
    protected boolean doShow() {
        final Iterable<Meal> meals = ctrl.listMeals();
        final SelectWidget<Meal> mealSelector = new SelectWidget("meals",meals, new MealForReservationPrinter());
        mealSelector.show();
        Meal choosen = mealSelector.selectedElement();
        Reservation r;
        try {
             r = ctrl.createReservation(choosen);
        } catch (IllegalStateException | IllegalArgumentException | NoMoneyException e) {
            System.out.println(e.getMessage());
            return false;
        }
        System.out.printf("Confirm the reservation:\n");
        new ReservationPrinter().visit(r);
        System.out.printf("\n1 - Confirm, 0 - exit\n");
        if (Console.readOption(1, 1, 0) == 1) {
            try {
                ctrl.doReservation();
            } catch (DataConcurrencyException | DataIntegrityViolationException ex) {
                System.out.println("That Reservation was already made.");
                return false;
            } catch (IllegalAccessException ex) {
                System.out.println(ex.getMessage());
                return false;
            }
        }
        return false;
    }

    @Override
    public String headline() {
        return "Meal Reservation";
    }

}
