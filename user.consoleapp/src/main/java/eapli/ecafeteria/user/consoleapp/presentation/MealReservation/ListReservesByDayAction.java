/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package eapli.ecafeteria.user.consoleapp.presentation.MealReservation;

import eapli.framework.actions.Action;

/**
 *
 * @author Alexandre Gomes e André Chaves
 */
public class ListReservesByDayAction implements Action{

    @Override
    public boolean execute() {
        return new ListReserveByDayUI().show();
    }
    
}
