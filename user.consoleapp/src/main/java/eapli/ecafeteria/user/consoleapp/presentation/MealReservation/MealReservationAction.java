package eapli.ecafeteria.user.consoleapp.presentation.MealReservation;

import eapli.framework.actions.Action;

/**
 *
 * @author Jorge Pereira 1141216 \\ Luís Magalhães 1141174
 */
public class MealReservationAction implements Action{

    @Override
    public boolean execute() {
        return new MealReservationUI().show();
    }
    
}
