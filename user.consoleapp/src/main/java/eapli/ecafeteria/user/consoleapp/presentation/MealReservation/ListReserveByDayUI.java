/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package eapli.ecafeteria.user.consoleapp.presentation.MealReservation;

import eapli.cafeteria.consoleapp.presentation.MealReservation.ReservationPrinter;
import eapli.ecafeteria.application.MealReservation.ListReserveByDayController;
import eapli.ecafeteria.domain.mealReservation.Reservation;
import eapli.ecafeteria.user.consoleapp.presentation.CafeteriaUserBaseUI;
import eapli.framework.presentation.console.SelectWidget;
import eapli.util.io.Console;

/**
 *
 * @author Alexandre Gomes e André Chaves
 * 
 */
public class ListReserveByDayUI extends CafeteriaUserBaseUI{
    
    /**
     * Use Case Controller
     */
    private final ListReserveByDayController theController = new ListReserveByDayController();
    
    @Override
    protected boolean doShow() {
        int numberOfDays = Console.readInteger("Insert number of days you wish to see your next reserves: \n");
        
        Iterable<Reservation> listReservations = theController.reservationByDate(numberOfDays);
        final SelectWidget<Reservation> reservationSelecter = new SelectWidget("Reservations", listReservations, new ReservationPrinter());
        reservationSelecter.show();
        
        return false;
    }
    
}
