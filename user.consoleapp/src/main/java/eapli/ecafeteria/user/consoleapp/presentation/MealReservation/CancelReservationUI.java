/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package eapli.ecafeteria.user.consoleapp.presentation.MealReservation;

import eapli.cafeteria.consoleapp.presentation.MealReservation.ReservationPrinter;
import eapli.ecafeteria.application.MealReservation.CancelReservationController;
import eapli.ecafeteria.application.MealReservation.ListReserveByDayController;
import eapli.ecafeteria.domain.mealReservation.Reservation;
import eapli.ecafeteria.user.consoleapp.presentation.CafeteriaUserBaseUI;
import eapli.framework.persistence.DataConcurrencyException;
import eapli.framework.persistence.DataIntegrityViolationException;
import eapli.framework.presentation.console.SelectWidget;
import eapli.util.io.Console;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 *
 * @author André Chaves e Alexandre Gomes
 */
public class CancelReservationUI extends CafeteriaUserBaseUI {

    private final CancelReservationController controller = new CancelReservationController();
    private final ListReserveByDayController theController = new ListReserveByDayController();

    @Override
    protected boolean doShow() {
        int numberOfDays = Console.readInteger("Input the number of days: \n");
        Iterable<Reservation> listReservations = theController.reservationByDate(numberOfDays);
        final SelectWidget<Reservation> reservationSelecter = new SelectWidget("Reservations", listReservations, new ReservationPrinter());
        reservationSelecter.show();
        Reservation optionSelected = reservationSelecter.selectedElement();
        try {
            controller.cancelReservation(optionSelected);
        } catch (IllegalAccessException ex) {
            Logger.getLogger(CancelReservationUI.class.getName()).log(Level.SEVERE, null, ex);
        } catch (DataConcurrencyException ex) {
            Logger.getLogger(CancelReservationUI.class.getName()).log(Level.SEVERE, null, ex);
        } catch (DataIntegrityViolationException ex) {
            Logger.getLogger(CancelReservationUI.class.getName()).log(Level.SEVERE, null, ex);
        }
        return false;
    }

    @Override
    public String headline() {
        return "Cancel Reservation";
    }

}
