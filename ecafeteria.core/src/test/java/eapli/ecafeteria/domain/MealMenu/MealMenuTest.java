/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package eapli.ecafeteria.domain.MealMenu;

import eapli.ecafeteria.domain.meals.Dish;
import eapli.ecafeteria.domain.meals.DishType;
import eapli.ecafeteria.domain.meals.MealType;
import eapli.framework.domain.Designation;
import eapli.framework.domain.Money;
import eapli.framework.dto.GenericDTO;
import eapli.util.DateTime;
import java.util.Calendar;
import java.util.Currency;
import org.junit.After;
import org.junit.AfterClass;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;
import static org.junit.Assert.*;

/**
 *
 * @author Rafael Delgado <Rafael.delgado at Rafael.org>
 */
public class MealMenuTest {

     Calendar date1;
     Calendar date2;
     Calendar date3;
     Calendar date4;
     Calendar date5;

     public MealMenuTest() {

     }

     @BeforeClass
     public static void setUpClass() {
     }

     @AfterClass
     public static void tearDownClass() {
     }

     @Before
     public void setUp() {

          date1 = DateTime.parseDate("03-06-2017");
          date2 = DateTime.parseDate("02-06-2016");
          date3 = DateTime.parseDate("11-06-2017");
          date4 = DateTime.parseDate("09-06-2016");
          date5 = DateTime.parseDate("04-06-2017");

     }

     @After
     public void tearDown() {
     }

     /**
      * Test if start date is after todays date
      */
     @Test(expected = IllegalStateException.class)
     public void ensureStartDateIsValid() {
          System.out.println("Ensure date is valid ");
          MealMenu instance = new MealMenu(date2, date3);

     }

     /**
      * Test if start date is not empty
      */
     @Test(expected = IllegalStateException.class)
     public void ensureStartDateIsNotEmpty() {
          System.out.println("Ensure date is not empty");
          MealMenu instance = new MealMenu(null, date3);

     }

     /**
      * Test if start date is not empty
      */
     @Test(expected = IllegalStateException.class)
     public void ensureStartDateIsBeforeEndDate() {
          System.out.println("Ensure start date is before end date");
          MealMenu instance = new MealMenu(date5, date1);

     }

     /**
      * tests if end date is after start date.
      */
     @Test(expected = IllegalStateException.class)
     public void ensureEndDateIsValid() {
          System.out.println("Ensure end date is valid");
          MealMenu instance = new MealMenu(date1, date4);

     }

     /**
      * Test of setStartDate method, of class MealMenu.
      */
     @Test(expected = IllegalStateException.class)
     public void ensureEndDateIsNotEmpty() {
          System.out.println("ensure end date is not valid");
          MealMenu instance = new MealMenu(date1, null);

     }

     /**
      * Test of setStartDate method, of class MealMenu.
      */
     @Test(expected = IllegalStateException.class)
     public void ensureEndDateAfterStartDate() {
          System.out.println("setStartDate");
          MealMenu instance = new MealMenu(date3, date1);

     }

     /**
      * Test of setStartDate method, of class MealMenu.
      */
     @Test(expected = IllegalStateException.class)
     public void ensureEndDateAndStartDateAreDifferent() {
          System.out.println("setStartDate");
          MealMenu instance = new MealMenu(date1, date1);

     }

     /**
      * Test of setEndDate method, of class MealMenu.
      */
     @Test()
     public void ensureInitialStateIsInProgress() {
          System.out.println("check if intial date is in progress");
          MealMenu instance = new MealMenu(date1, date3);
          assertEquals(instance.getState(), MealMenuState.IN_PROGRESS);

     }

     /**
      * Test return of start date
      */
     @Test()
     public void ensureStartDateValue() {
          System.out.println("Check if start date value when initialized is correct");
          MealMenu instance = new MealMenu(date1, date3);
          assertEquals(instance.StartDate(), date1);

     }

     /**
      * Test of return of end date
      */
     @Test()
     public void ensureEndDateValue() {
          System.out.println("Check if end date value when initialized is correct");
          MealMenu instance = new MealMenu(date1, date3);
          assertEquals(instance.EndDate(), date3);

     }

     /**
      * Test of Published method, of class MealMenu.
      */
     @Test(expected = IllegalStateException.class)
     public void ensureItchangestoPublishedifItsInProgress() {
          System.out.println("ensures that when changing states it doesnt change a published meal menu to published again or the dates anre not valid");
          MealMenu instance = new MealMenu(date1, date3);
          instance.changeStateToPublished();
          instance.changeStateToPublished();

     }

     /**
      * Test of sameAs method, of class MealMenu.
      */
     @Test
     public void testSameAs() {
          System.out.println("sameAs");
          Object other = new MealMenu(date1, date3);
          MealMenu instance = new MealMenu(date5, date3);

          boolean expResult = false;
          boolean result = instance.sameAs(other);

          assertEquals(expResult, result);

     }

     /**
      * Checks if meal menu has no description if it as the desired description
      */
     @Test
     public void whenMealMenuHasNoDescription() {
          System.out.println("Checks when meal menu has no description");
          MealMenu instance = new MealMenu(date5, date3);

          String expResult = "Sem descricao";
          String result = instance.description();

          assertEquals(expResult, result);

     }

     /**
      * Checks if meal menu has description if it as the desired description
      */
     @Test
     public void whenMealMenuHasDescription() {
          System.out.println("Checks when meal menu has description");
          String expResult = "Ceia de Natal";
          MealMenu instance = new MealMenu(date5, date3, expResult);

          String result = instance.description();

          assertEquals(expResult, result);

     }

}
