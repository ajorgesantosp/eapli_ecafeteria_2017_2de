/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package eapli.ecafeteria.domain.account;

import eapli.ecafeteria.domain.authz.Password;
import eapli.ecafeteria.domain.authz.RoleType;
import eapli.ecafeteria.domain.authz.SystemUser;
import eapli.ecafeteria.domain.authz.SystemUserBuilder;
import eapli.ecafeteria.domain.authz.Username;
import eapli.ecafeteria.domain.cafeteria.CafeteriaUser;
import eapli.ecafeteria.domain.cafeteria.MecanographicNumber;
import eapli.ecafeteria.domain.cafeteria.OrganicUnit;
import eapli.framework.domain.EmailAddress;
import eapli.framework.domain.Money;
import eapli.util.DateTime;
import java.util.Currency;
import java.util.HashSet;
import java.util.Set;
import org.junit.After;
import org.junit.AfterClass;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;
import static org.junit.Assert.*;

/**
 *
 * @author André Chaves e Alexandre Gomes
 */
public class AccountTransationTest {
    
    String bankAccount = "0000 1111 2222 3333 4";
    Account acc = new Account(bankAccount);
    
    private CafeteriaUser cafteriaUser;
    
    public AccountTransationTest() {
    }
    
    @BeforeClass
    public static void setUpClass() {
    }
    
    @AfterClass
    public static void tearDownClass() {
    }
    
    @Before
    public void setUp() {
        
        //SETUP USER
        Username username = new Username("1141174");
        Password pw = new Password("Password1");
        String firstName = "Luis";
        String lastName = "Magalhaes";
        EmailAddress email = EmailAddress.valueOf("mm@gmail.com");
        Set<RoleType> roles = new HashSet<>();
        roles.add(RoleType.CAFETERIA_USER);

        SystemUser su = new SystemUserBuilder().withUsername(username).withPassword(pw).withFirstName(firstName).withLastName(lastName)
                .withEmail(email).withCreatedOn(DateTime.now()).withRoles(roles).build();

        cafteriaUser = new CafeteriaUser(su, new OrganicUnit("ISEP", "ISEP", "ISEP"), new MecanographicNumber("1141174"));
        
        
    }
    
    @After
    public void tearDown() {
    }

    /**
     * Test of doTransation method, of class AccountTransation.
     * @throws java.lang.IllegalAccessException
     */
    @Test
    public void testDoTransation() throws IllegalAccessException {
        System.out.println("doTransation");
        
        Money money = new Money(20, Currency.getInstance("EUR"));
        
        AccountTransation accTr = new AccountTransation(acc, money, AccountTransation.TransationType.CREDIT);
        acc.registTransaction(accTr);
        Money money2 = new Money(20, Currency.getInstance("EUR"));
        
        assertEquals(money2, acc.balance());
        
        
        Money money3 = new Money(10, Currency.getInstance("EUR"));
        AccountTransation accTr2 = new AccountTransation(acc, money3, AccountTransation.TransationType.DEBIT);
        acc.registTransaction(accTr2);
        Money money4 = new Money(10, Currency.getInstance("EUR"));
        
    
        assertEquals(money4, acc.balance());
        
        cafteriaUser.createAccount(bankAccount);
        cafteriaUser.account().registTransaction(accTr);
        
        System.out.println(cafteriaUser.account().balance().amount());
    }
    

    
}
