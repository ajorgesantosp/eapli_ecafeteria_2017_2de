package eapli.ecafeteria.domain.mealReservation;

import eapli.ecafeteria.domain.MealMenu.MealMenu;
import eapli.ecafeteria.domain.account.Account;
import eapli.ecafeteria.domain.account.AccountTransation;
import eapli.ecafeteria.domain.authz.Password;
import eapli.ecafeteria.domain.authz.RoleType;
import eapli.ecafeteria.domain.authz.SystemUser;
import eapli.ecafeteria.domain.authz.SystemUserBuilder;
import eapli.ecafeteria.domain.authz.Username;
import eapli.ecafeteria.domain.cafeteria.CafeteriaUser;
import eapli.ecafeteria.domain.cafeteria.MecanographicNumber;
import eapli.ecafeteria.domain.cafeteria.OrganicUnit;
import eapli.ecafeteria.domain.meals.Dish;
import eapli.ecafeteria.domain.meals.DishType;
import eapli.ecafeteria.domain.meals.Meal;
import eapli.ecafeteria.domain.meals.MealType;
import eapli.ecafeteria.domain.meals.NutricionalInfo;
import eapli.framework.domain.Designation;
import eapli.framework.domain.EmailAddress;
import eapli.framework.domain.Money;
import eapli.util.DateTime;
import java.util.Calendar;
import java.util.Currency;
import java.util.HashSet;
import java.util.Set;
import java.util.logging.Level;
import java.util.logging.Logger;
import org.junit.Before;
import org.junit.Test;
import static org.junit.Assert.*;

/**
 *
 * @author Jorge Pereira 1141216 \ Luís Magalhães 1141174
 */
public class ReservationTest {

     private Reservation r;
     private CafeteriaUser cafteriaUser;
     private Meal mealDinner;
     private Meal mealLunch;
     private static Calendar validDateDinner;
     private static Calendar invalidDateDinner;
     private static Calendar validDateLunch;
     private static Calendar invalidDateLunch;

     public ReservationTest() {
     }

     @Before
     public void setUp() {
          validDateDinner = Calendar.getInstance();
          validDateDinner.set(2018, 04, 2, 12, 0);

          invalidDateDinner = Calendar.getInstance();
          invalidDateDinner.set(2018, 04, 2, 16, 0);

          validDateLunch = Calendar.getInstance();
          validDateLunch.set(2018, 04, 2, 9, 0);

          invalidDateLunch = Calendar.getInstance();
          invalidDateLunch.set(2018, 04, 2, 10, 0);

          mealDinner = new Meal(DateTime.parseDate("02-05-2018"), new MealType(MealType.DINNER), new Dish(new DishType("fish", "fish"), Designation.valueOf("Salmão"), new NutricionalInfo(144, 20), Money.euros(15.00)), new MealMenu(DateTime.parseDate("01-05-2018"), DateTime.parseDate("08-05-2018")));

          mealLunch = new Meal(DateTime.parseDate("02-05-2018"), new MealType(MealType.LUNCH), new Dish(new DishType("fish", "fish"), Designation.valueOf("Salmão"), new NutricionalInfo(144, 20), Money.euros(15.00)), new MealMenu(DateTime.parseDate("01-05-2018"), DateTime.parseDate("08-05-2018")));

          //SETUP USER
          Username username = new Username("1141174");
          Password pw = new Password("Password1");
          String firstName = "Luis";
          String lastName = "Magalhaes";
          EmailAddress email = EmailAddress.valueOf("mm@gmail.com");
          Set<RoleType> roles = new HashSet<>();
          roles.add(RoleType.CAFETERIA_USER);

          SystemUser su = new SystemUserBuilder().withUsername(username).withPassword(pw).withFirstName(firstName).withLastName(lastName)
                  .withEmail(email).withCreatedOn(DateTime.now()).withRoles(roles).build();

          cafteriaUser = new CafeteriaUser(su, new OrganicUnit("ISEP", "ISEP", "ISEP"), new MecanographicNumber("1141174"), "asd");
          try {
               cafteriaUser.account().registTransaction(new AccountTransation(new Account("asd"), new Money(50, Currency.getInstance("EUR")), AccountTransation.TransationType.CREDIT));
          } catch (IllegalAccessException ex) {
               Logger.getLogger(ReservationTest.class.getName()).log(Level.SEVERE, null, ex);
          }

          r = new Reservation();
     }

     @Test
     public void ensureReservationIsValid() {
          System.out.println("ensureReservationIsValid");

          r.setMeal(mealDinner);
          r.setUser(cafteriaUser);
          r.setDate(validDateDinner);

          r.setCreated();

          assertTrue(r.createdState());
     }

     @Test(expected = IllegalStateException.class)
     public void ensureReservationIsNotValidWithoutUser() {
          System.out.println("ensureReservationIsNotValidWithoutUser");
          Reservation rnv = new Reservation();

          rnv.setMeal(mealDinner);
          rnv.setDate(validDateDinner);

          r.setCreated();
     }

     @Test(expected = IllegalStateException.class)
     public void ensureReservationIsNotValidWithoutMeal() {
          System.out.println("ensureReservationIsNotValidWithoutMeal");
          Reservation rnv = new Reservation();

          rnv.setUser(cafteriaUser);
          rnv.setDate(validDateDinner);
     }

     @Test(expected = IllegalStateException.class)
     public void ensureReservationIsNotValidWithoutDate() {
          System.out.println("ensureReservationIsNotValidWithoutDate");
          Reservation rnv = new Reservation();

          rnv.setMeal(mealDinner);
          rnv.setUser(cafteriaUser);

          r.setCreated();
     }

     @Test
     public void ensureReservationIntegrity() {
          System.out.println("ensureReservationIntegrity");

          r.setMeal(mealDinner);
          r.setUser(cafteriaUser);
          r.setDate(validDateDinner);

          r.setCreated();
     }

     @Test(expected = IllegalStateException.class)
     public void ensureReservationHasNoIntegrityDinner() {
          System.out.println("ensureReservationIntegrity");

          r.setMeal(mealDinner);
          r.setDate(validDateDinner);

          r.setCreated();
     }

     @Test(expected = IllegalStateException.class)
     public void ensureReservationHasNoIntegrityLunch() {
          System.out.println("ensureReservationIntegrity");

          r.setMeal(mealLunch);
          r.setDate(validDateLunch);

          r.setCreated();
     }

     @Test(expected = IllegalArgumentException.class)
     public void ensureReservationIsMadeOnTimeDinner() {
          System.out.println("ensureReservationIsMadeOnTimeDinner");

          r.setMeal(mealDinner);
          r.setDate(invalidDateDinner);
     }

     @Test(expected = IllegalArgumentException.class)
     public void ensureReservationIsMadeOnTimeLunch() {
          System.out.println("ensureReservationIsMadeOnTimeLunch");

          r.setMeal(mealLunch);
          r.setDate(invalidDateLunch);
     }

     @Test(expected = IllegalStateException.class)
     public void ensureReservationHasMealBeforeSetTheDate() {
          System.out.println("ensureReservationHasMealBeforeSetTheDate");

          r.setDate(invalidDateDinner);
     }

     @Test(expected = IllegalStateException.class)
     public void ensureReservationHasMealBeforeSetTheUser() {
          System.out.println("ensureReservationHasMealBeforeSetTheUser");

          r.setUser(cafteriaUser);
     }

     //TODO Allergens, balance and change the state test
     @Test
     public void cancelReservation() {
          r.setMeal(mealLunch);
          r.setUser(cafteriaUser);
          r.setDate(validDateLunch);

          r.setCreated();

          Money money = r.cancelReservation();

          assertEquals(7.5, money.amount(), 0.0);
     }
}
