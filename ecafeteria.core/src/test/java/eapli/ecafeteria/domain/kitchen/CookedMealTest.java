/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package eapli.ecafeteria.domain.kitchen;

import eapli.ecafeteria.domain.MealMenu.MealMenu;
import eapli.ecafeteria.domain.meals.Dish;
import eapli.ecafeteria.domain.meals.DishType;
import eapli.ecafeteria.domain.meals.Meal;
import eapli.ecafeteria.domain.meals.MealType;
import eapli.ecafeteria.domain.meals.NutricionalInfo;
import eapli.framework.domain.Designation;
import eapli.framework.domain.Money;
import eapli.util.DateTime;
import java.util.Calendar;
import org.junit.After;
import org.junit.AfterClass;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;
import static org.junit.Assert.*;

/**
 *
 * @author Rafael Delgado <Rafael.delgado at Rafael.org>
 */
public class CookedMealTest {

     Calendar date = DateTime.newCalendar(2, 5, 2017);
     static Designation des1 = Designation.valueOf("Designation1");
     static Designation des2 = Designation.valueOf("Designation2");
     Meal meal1;
     Meal meal2;

     public CookedMealTest() {
     }

     @BeforeClass
     public static void setUpClass() {
     }

     @AfterClass
     public static void tearDownClass() {
     }

     @Before
     public void setUp() {
          Calendar startDay = DateTime.tomorrow();
          Calendar lastDay = DateTime.tomorrow();
          lastDay.add(Calendar.DAY_OF_MONTH, 5);
          MealMenu mealMenu = new MealMenu(startDay, lastDay);

          MealType mealType1 = new MealType("Almoco");
          DishType dishType1 = new DishType("Fish", "Bacalhau,Atum,Robalo");
          Money price1 = Money.euros(5);
          NutricionalInfo nutInfo1 = new NutricionalInfo(300, 2);
          Dish dish1 = new Dish(dishType1, des1, nutInfo1, price1);

          Calendar mealDate = DateTime.tomorrow();
          mealDate.add(Calendar.DAY_OF_MONTH, 1);

          meal1 = new Meal(mealDate, mealType1, dish1, mealMenu);

          MealType mealType2 = new MealType("Jantar");
          DishType dishType2 = new DishType("vegie", "Saladas");
          Money price2 = Money.euros(4.30);
          NutricionalInfo nutInfo2 = new NutricionalInfo(180, 1);
          Dish dish2 = new Dish(dishType2, des2, nutInfo2, price2);
          meal2 = new Meal(mealDate, mealType2, dish2, mealMenu);
     }

     @After
     public void tearDown() {
     }

    /**
     * Tests initializer
     */
     @Test(expected = IllegalStateException.class)
     public void ensureCookedMealQuantityIsNotZero() {
          System.out.println("Ensure quantity is not zero");
          CookedMeal instance = new CookedMeal(meal2, 0);
     }

     /**
      * Tests initializer
      */
     @Test(expected = IllegalStateException.class)
     public void ensureCookedMealQuantityIsValid() {
          System.out.println("Ensure quantity is valid");
          CookedMeal instance = new CookedMeal(meal2, -13);
     }

     /**
      * Tests initializer
      */
     @Test(expected = IllegalStateException.class)
     public void ensureCookedMealMealIsNotNNull() {
          System.out.println("Ensure cooked MealI meal is not empty");
          CookedMeal instance = new CookedMeal(null, -13);
     }
     
     

     /**
      * Test of id method, of class CookedMeal.
      */
     @Test
     public void testId() {
          System.out.println("id");
          CookedMeal instance = new CookedMeal(meal2, 30);
          Meal expResult = meal2;
          Meal result = instance.id();
          assertEquals("Expected Null on the return because the id is null beacause is a variable created by the data base", expResult, result);
     }

     /**
      * Test of meal method, of class CookedMeal.
      */
     @Test
     public void testMeal() {
          System.out.println("meal");
          CookedMeal instance = new CookedMeal(meal1, 20);
          Meal expResult = meal1;
          Meal result = instance.meal();
          assertEquals("Test the return of meal of cooked meal is the same as instanced", expResult, result);
     }

     /**
      * Test of quantity method, of class CookedMeal.
      */
     @Test
     public void testQuantity() {
          System.out.println("quantity");
          CookedMeal instance = new CookedMeal(meal2, 40);
          int expResult = 40;
          int result = instance.quantity();
          assertEquals("Test if the return of quantity of cooked meal is the same as the instance", expResult, result);
     }

     /**
      * Test of changeMealInfoTo method, of class CookedMeal.
      */
     @Test
     public void testChangeMealInfoTo() {
          System.out.println("changeMealInfoTo");
          Meal newMeal = meal2;
          CookedMeal instance = new CookedMeal(meal1, 20);
          assertFalse("Tests cookedMeal meal is the same as the new meal - Must retrun false ", instance.meal().sameAs(newMeal));
          instance.changeMealInfoTo(newMeal);
          assertTrue("Test cookedMeal meal is the same as the new meal(after using changeMealInfo - Must return true ", instance.meal().sameAs(newMeal));
     }

     /**
      * Test of changeQuantityTo method, of class CookedMeal.
      */
     @Test
     public void testChangeQuantityTo() {
          System.out.println("changeQuantityTo");
          int newQuantity = 20;
          CookedMeal instance = new CookedMeal(meal2, 40);
          assertNotEquals("Test quantity of cookedMeal is equal the new quantity - Must Return false", instance.quantity(), newQuantity);
          instance.changeQuantityTo(newQuantity);
          assertEquals("Test quantity of cookedMeal is equal the new quantity (after using changeQuantityInfo) - MustReturn true", instance.quantity(), newQuantity);
     }

}
