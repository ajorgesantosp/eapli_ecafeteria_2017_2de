/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package eapli.ecafeteria.domain.kitchen;

import javax.print.DocFlavor;
import org.junit.After;
import org.junit.AfterClass;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;
import static org.junit.Assert.*;
import org.junit.Ignore;

/**
 *
 * @author Fabio
 */
public class LotTest {

     private final String code = "123";
     private final String name = "testMaterialName";
     private final String description = "testDescriptionName";
     private final Material material = new Material(name, description);
     private final Lot testLot = new Lot(code, material);

     public LotTest() {
     }

     @BeforeClass
     public static void setUpClass() {
     }

     @AfterClass
     public static void tearDownClass() {
     }

     @Before
     public void setUp() {

     }

     @After
     public void tearDown() {
     }

     /**
      * Test of equals method, of class Lot.
      */
     @Test
     public void testEquals() {
          System.out.println("equals");
          Lot o = new Lot(code, material);
          boolean expResult = true;
          boolean result = testLot.equals(o);
          assertEquals(expResult, result);
     }

     /**
      * Test of hashCode method, of class Lot.
      */
     @Test
     public void testHashCode() {
          System.out.println("hashCode");
          int expResult = code.hashCode();
          int result = testLot.hashCode();
          assertEquals(expResult, result);
     }

     /**
      * Test of toString method, of class Lot.
      */
     @Ignore
     @Test
     public void testToString() {
          System.out.println("toString");
          Lot instance = new Lot();
          String expResult = "";
          String result = instance.toString();
          assertEquals(expResult, result);

     }

     /**
      * Test of code method, of class Lot.
      */
     @Test
     public void testCode() {
          System.out.println("code");
          String expResult = this.code;
          String result = testLot.code();
          assertEquals(expResult, result);
     }

     /**
      * Test of material method, of class Lot.
      */
     @Test
     public void testMaterial() {
          System.out.println("material");
          Material expResult = material;
          Material result = testLot.material();
          assertEquals(expResult, result);
     }

}
