/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package eapli.ecafeteria.domain.meals;

import org.junit.After;
import org.junit.AfterClass;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;
import static org.junit.Assert.*;

/**
 *
 * @author bfgomes
 */
public class MealTypeTest {
    
    public MealTypeTest() {
    }
    
    @BeforeClass
    public static void setUpClass() {
    }
    
    @AfterClass
    public static void tearDownClass() {
    }
    
    @Before
    public void setUp() {
    }
    
    @After
    public void tearDown() {
    }

    /**
     * Test of ensureNameIsNotEmpty method, of class MealType.
     */
    @Test(expected = IllegalArgumentException.class)
    public void ensureNameIsNotEmpty() {
        System.out.println("ensureNameIsNotEmpty");
        new MealType("");
    }
    
}
