/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package eapli.ecafeteria.domain.meals;

import eapli.framework.domain.Designation;
import java.util.ArrayList;
import org.junit.Test;
import static org.junit.Assert.*;

/**
 *
 * @author calve
 */
public class DishAllergensTest {

    private final Allergen a = new Allergen(Designation.valueOf("Cereais"));

    public DishAllergensTest() {
    }

    @Test(expected = IllegalArgumentException.class)
    public void testAllergenListMustNotBeNull() {
        System.out.println("must have a list");
        DishAllergens instance = new DishAllergens(null);
    }

    @Test
    public void testInexistenceOfDuplicates() {
        System.out.println("must not have duplicates");
        DishAllergens list1 = new DishAllergens();
        list1.add(a);
        DishAllergens list2 = new DishAllergens();
        list2.add(a);
        list2.add(a);
        boolean result = list1.sameAs(list2);
        assertTrue(result);
    }

}
