/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package eapli.ecafeteria.domain.meals;

import eapli.ecafeteria.domain.MealMenu.MealMenu;
import eapli.framework.domain.Designation;
import eapli.framework.domain.Money;
import eapli.util.DateTime;
import java.util.Calendar;
import org.junit.After;
import org.junit.AfterClass;
import static org.junit.Assert.*;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Ignore;
import org.junit.Test;

/**
 *
 *  @author Bruno Gomes <1151164@isep.ipp.pt>, Nuno Brito <1151173@isep.ipp.pt>
 */
public class MealTest {
    
    private MealType mealType;
    private Dish dish;
    private Calendar calendar1;
    private Calendar calendar2;
    private static String DATE2= "01-05-2017";
    private static String DATE1= "01-05-2018";
    private MealMenu mealmenu;
    
    public MealTest() {
    }
    
    @BeforeClass
    public static void setUpClass() {
    }
    
    @AfterClass
    public static void tearDownClass() {
    }
    
    @Before
    public void setUp() {
        mealType = new MealType();
        dish = new Dish();
        calendar1 = DateTime.parseDate(DATE1);
        calendar2 = DateTime.parseDate(DATE2);
        mealmenu= new MealMenu(DateTime.parseDate("30-04-2018"),DateTime.parseDate("07-05-2018"));
    }
    
    @After
    public void tearDown() {
    }

    
    /**
     * Ensure the date is not empty.
     */

    @Test(expected = IllegalStateException.class)
    public void ensureDateIsValid() {
        System.out.println("Ensure the date is not empty.");
        Meal instance = new Meal(null,mealType,dish,mealmenu);
    }

     /**
     * Ensure the meal type is not empty.
     */
    
    @Test(expected = IllegalStateException.class)
    public void ensureMealTypeNotEmpty() {
        System.out.println("Ensure the meal type is not empty.");
        Meal instance = new Meal(calendar1,null,dish,mealmenu);
    }
    
    /**
     * Ensure the dish is not empty.
     */

    @Test(expected = IllegalStateException.class)
    public void ensureDishNotEmpty() {
        System.out.println("Ensure the dish is not empty.");
        Meal instance = new Meal(calendar1,mealType,null,mealmenu);
    }
    
    /**
     * Ensure the meal menu is not empty.
     */

    @Test(expected = IllegalStateException.class)
    public void ensureMeaMenuNotEmpry() {
        System.out.println("Ensure the mealmenu is not empty.");
        Meal instance = new Meal(calendar1,mealType,dish,null);
    }
    
    /**
     * Ensure the dish exists or is valid.
     */

    @Test
    public void ensureDishExistsOrIsValid() {
        System.out.println("Ensure the dish exists or is valid.");
        
        //MEAL MENU ARGS
        Calendar startDay = DateTime.tomorrow();
        Calendar endDay = DateTime.tomorrow();
        endDay.add(Calendar.DAY_OF_MONTH, 5);
        
        // MEAL ARGS
        calendar2 = DateTime.parseDate(DATE2);
        mealType = new MealType("Almoço");
        
        // DISH ARGS
        DishType dishType = new DishType("vegie", "vegie");
        Designation designation = Designation.valueOf("tofu grelhado");
        Money money = Money.euros(2.56);
        
        dish = new Dish(dishType, designation, money);
        
        MealMenu mealMenu = new MealMenu(startDay, endDay);
        
        Calendar mealDay = DateTime.tomorrow();
        mealDay.add(Calendar.DAY_OF_MONTH, 1);
        
        Meal instance = new Meal(mealDay, mealType, dish, mealMenu);
        assertTrue(instance.dish().sameAs(dish));
    }

}
