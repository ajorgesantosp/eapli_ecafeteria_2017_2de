
package eapli.ecafeteria.domain.cafeteria;

import eapli.ecafeteria.domain.meals.MealType;
import eapli.util.DateTime;
import java.util.Calendar;
import org.junit.Before;
import org.junit.Test;
import static org.junit.Assert.*;

/**
 *
 * @author Jorge Pereira 1141216 \ Luís Magalhães 1141174
 */
public class ShiftTest {
    
    private Shift shift;
    private MealType mealTypeDinner;
    private MealType mealTypeLunch;
    private Calendar date;
    
    public ShiftTest() {
    }
    
    @Before
    public void setUp() {
        date = DateTime.now();
        mealTypeDinner = new MealType(MealType.DINNER);
        mealTypeLunch = new MealType(MealType.LUNCH);
        shift = new Shift(mealTypeDinner, date);
    }
    
    
}
