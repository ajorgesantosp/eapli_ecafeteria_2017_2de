/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package eapli.ecafeteria.domain.cafeteria;

import eapli.ecafeteria.domain.MealMenu.MealMenu;
import eapli.ecafeteria.domain.account.Account;
import eapli.ecafeteria.domain.account.AccountTransation;
import eapli.ecafeteria.domain.authz.Password;
import eapli.ecafeteria.domain.authz.RoleType;
import eapli.ecafeteria.domain.authz.SystemUser;
import eapli.ecafeteria.domain.authz.SystemUserBuilder;
import eapli.ecafeteria.domain.authz.Username;
import eapli.ecafeteria.domain.mealReservation.Reservation;
import eapli.ecafeteria.domain.mealReservation.ReservationBuilder;
import eapli.ecafeteria.domain.mealReservation.ReservationTest;
import eapli.ecafeteria.domain.meals.Dish;
import eapli.ecafeteria.domain.meals.DishType;
import eapli.ecafeteria.domain.meals.Meal;
import eapli.ecafeteria.domain.meals.MealType;
import eapli.ecafeteria.domain.meals.NutricionalInfo;
import eapli.framework.domain.Designation;
import eapli.framework.domain.EmailAddress;
import eapli.framework.domain.Money;
import eapli.util.DateTime;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Currency;
import java.util.HashSet;
import java.util.Set;
import java.util.logging.Level;
import java.util.logging.Logger;
import org.junit.After;
import org.junit.AfterClass;
import static org.junit.Assert.*;
import static org.junit.Assert.assertTrue;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Ignore;
import org.junit.Test;

/**
 *
 * @author Jorge Pereira 1141216 \ Luís Magalhães 1141174
 */
public class CashRegisterTest {

     private CashRegister cashRegisterOpen;
     private CashRegister cashRegisterClosed;
     private MealType dinnerMealType;
     private MealType lunchMealType;
     private Calendar date;
     private MealMenu mealMenu;

     public CashRegisterTest() {
     }

     @BeforeClass
     public static void setUpClass() {
     }

     @AfterClass
     public static void tearDownClass() {
     }

     @Before
     public void setUp() {
          date = DateTime.now();
          dinnerMealType = new MealType(MealType.DINNER);
          lunchMealType = new MealType(MealType.LUNCH);

          cashRegisterOpen = new CashRegister();
          cashRegisterOpen.toggleState();

          cashRegisterClosed = new CashRegister();

          mealMenu = new MealMenu(DateTime.parseDate("01-05-2018"), DateTime.parseDate("08-05-2018"));
     }

     @After
     public void tearDown() {
     }

     @Test(expected = IllegalStateException.class)
     public void testCashRegisterCannotBeOpenWhenOpen() {
          System.out.println("CashRegisterCannotBeOpenWhenOpen");
          cashRegisterOpen.openCashRegister(dinnerMealType, date);
     }

     @Test
     public void testCashRegisterOpensWhenClose() {
          System.out.println("CashRegisterOpensWhenClose");
          cashRegisterClosed.openCashRegister(lunchMealType, date);
          assertTrue(cashRegisterClosed.isOpen());
     }

     @Test
     public void testEnsureWhenCashRegisterIsCreatedItIsClose() {
          System.out.println("testEnsureWhenCashRegisterIsCreatedItIsClose");
          assertFalse(cashRegisterClosed.isOpen());
     }

     @Test
     public void testEnsureIfTheCashRegisterOpens() {
          System.out.println("testEnsureIfTheCashRegisterOpens");
          cashRegisterClosed.openCashRegister(lunchMealType, date);
          assertTrue(cashRegisterClosed.isOpen());
     }


     @Test
     public void testEnsureThatCashReturnTheCorrectNumberOfCreatedDeliveredNotDeliveredAndCancelledReservations() {
          System.out.println("testEnsureThatCashReturnTheCorrectNumberOfCreatedDeliveredNotDeliveredAndCancelledReservations");
          ReservationBuilder r;
          CafeteriaUser cafteriaUser;
          Meal mealLunch;
          Calendar validDateLunch;

          validDateLunch = Calendar.getInstance();
          validDateLunch.set(2018, 04, 2, 9, 0);

          mealLunch = new Meal(DateTime.parseDate("02-05-2018"), new MealType(MealType.LUNCH), new Dish(new DishType("fish", "fish"), Designation.valueOf("Salmão"), new NutricionalInfo(144, 20), Money.euros(15.00)), mealMenu);

          //SETUP USER
          Username username = new Username("1141174");
          Password pw = new Password("Password1");
          String firstName = "Luis";
          String lastName = "Magalhaes";
          EmailAddress email = EmailAddress.valueOf("mm@gmail.com");
          Set<RoleType> roles = new HashSet<>();
          roles.add(RoleType.CAFETERIA_USER);

          SystemUser su = new SystemUserBuilder().withUsername(username).withPassword(pw).withFirstName(firstName).withLastName(lastName)
                  .withEmail(email).withCreatedOn(DateTime.now()).withRoles(roles).build();

          cafteriaUser = new CafeteriaUser(su, new OrganicUnit("ISEP", "ISEP", "ISEP"), new MecanographicNumber("1141174"), "asd");
          try {
               cafteriaUser.account().registTransaction(new AccountTransation(new Account("asd"), new Money(50, Currency.getInstance("EUR")), AccountTransation.TransationType.CREDIT));
          } catch (IllegalAccessException ex) {
               Logger.getLogger(ReservationTest.class.getName()).log(Level.SEVERE, null, ex);
          }

          r = new ReservationBuilder();
          r.withMeal(mealLunch);
          r.withDate(validDateLunch);

          r.withCafeteriaUser(cafteriaUser);

          Reservation res = r.build();
          ArrayList<Reservation> list = new ArrayList();
          list.add(res);
          int result = cashRegisterOpen.getCancelledReservations(list);
          int expected = 0;

          assertEquals(expected, result);

          list = new ArrayList();
          list.add(res);

          result = cashRegisterOpen.getCreatedReservations(list);
          expected = 1;

          assertEquals(expected, result);

          list = new ArrayList();
          res.setDelivered();
          list.add(res);

          result = cashRegisterOpen.getDeliveredReservations(list);
          expected = 1;

          assertEquals(expected, result);

          list = new ArrayList();
//        res.setCreated();
          list.add(res);

     }
}
