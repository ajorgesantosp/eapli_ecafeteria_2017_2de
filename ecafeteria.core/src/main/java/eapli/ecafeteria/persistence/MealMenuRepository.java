/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package eapli.ecafeteria.persistence;

import eapli.ecafeteria.domain.MealMenu.MealMenu;
import eapli.ecafeteria.domain.kitchen.CookedMeal;
import eapli.framework.persistence.repositories.DataRepository;

/**
 *
 * @author João Sabugueiro<1150981@isep.ipp.pt>
 */
public interface MealMenuRepository extends DataRepository<MealMenu,Long>{
    Iterable<MealMenu> allInProgress();
     
}
