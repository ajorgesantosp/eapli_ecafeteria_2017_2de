/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package eapli.ecafeteria.persistence;

import eapli.ecafeteria.domain.cafeteria.CafeteriaUser;
import eapli.ecafeteria.domain.mealReservation.Reservation;
import eapli.ecafeteria.domain.meals.MealType;
import eapli.framework.persistence.repositories.DataRepository;
import java.util.Calendar;

/**
 *
 * @author Jorge Pereira 1141216 \ Luís Magalhães 1141174
 */
public interface MealReservationRepository extends DataRepository<Reservation, Long> {

    public Iterable<Reservation> findByDay(Calendar day, MealType mt);

    public Iterable<Reservation> findReservesNDaysFromUser(Calendar day, CafeteriaUser user);

    public Iterable<Reservation> reservationByUser(CafeteriaUser cafUser);

    public Reservation findFromHashCode(Integer hashCode);

    public Reservation findReserveCreatedHashDay(Integer hashCode, Calendar day, MealType mt);
}
