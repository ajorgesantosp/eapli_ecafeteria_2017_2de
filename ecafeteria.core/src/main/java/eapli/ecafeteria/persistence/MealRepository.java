package eapli.ecafeteria.persistence;

import eapli.ecafeteria.domain.MealMenu.MealMenu;
import eapli.ecafeteria.domain.meals.Meal;
import eapli.ecafeteria.domain.meals.MealType;
import eapli.framework.domain.range.TimePeriod;
import eapli.framework.persistence.repositories.DataRepository;
import java.util.Calendar;

/**
 *
 * @author Bruno Gomes <1151164@isep.ipp.pt>, Nuno Brito <1151173@isep.ipp.pt>
 */
public interface MealRepository extends DataRepository<Meal, Long> {

    Iterable<Meal> findByTimePeriod(TimePeriod timePeriod);
    Iterable<Meal> findByDay(Calendar day);
    Iterable<Meal> findByDayAndMealType(MealType mealType, Calendar day);
    Iterable<Meal> findFromDay(Calendar day);

    public Iterable<Meal> mealsOfMealMenu(MealMenu mealMenu);

    
}
