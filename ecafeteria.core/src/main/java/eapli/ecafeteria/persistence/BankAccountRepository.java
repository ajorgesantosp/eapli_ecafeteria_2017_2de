/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package eapli.ecafeteria.persistence;

import eapli.ecafeteria.domain.account.Account;
import eapli.framework.persistence.repositories.DataRepository;

/**
 *
 * @author Alexandre Gomes e André Chaves
 */
public interface BankAccountRepository  extends  DataRepository<Account, String>{
    
    public Account findByBankAccount (String BankAccount);
}
