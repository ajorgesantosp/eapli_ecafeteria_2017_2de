/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package eapli.ecafeteria.persistence;

import eapli.ecafeteria.domain.kitchen.CookedMeal;
import eapli.ecafeteria.domain.kitchen.Lot;
import eapli.ecafeteria.domain.meals.Meal;
import eapli.framework.persistence.repositories.DataRepository;

/**
 *
 * @author Rafael Delgado <Rafael.delgado at Rafael.org>
 */
public interface CookedMealRepository extends DataRepository<CookedMeal, Meal> {

    Iterable<CookedMeal> findByLot(Lot lot);

}
