/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package eapli.ecafeteria.persistence;

import eapli.ecafeteria.domain.cafeteria.Shift;
import eapli.framework.persistence.repositories.DataRepository;

/**
 *
 * @author Miguel
 */
public interface ShiftRepository extends DataRepository<Shift, Long>{
    
}
