/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package eapli.ecafeteria.persistence;

import eapli.ecafeteria.domain.cafeteria.CashRegister;
import eapli.framework.persistence.repositories.DataRepository;

/**
 *
 * @author Jorge Pereira 1141216 \ Luís Magalhães 1141174
 */
public interface CashRegisterRepository extends DataRepository<CashRegister, Long>{
   
    CashRegister find();
}
