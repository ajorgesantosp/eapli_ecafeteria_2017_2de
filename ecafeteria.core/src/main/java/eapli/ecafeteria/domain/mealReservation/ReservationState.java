package eapli.ecafeteria.domain.mealReservation;

/**
 * State of the reservation
 *
 * @author Jorge Pereira 1141216 \ Luís Magalhães 1141174
 */
public enum ReservationState {
    CREATED,
    CANCELED,
    DELIVERED,
    NOT_DELIVERED;
}
