package eapli.ecafeteria.domain.mealReservation;

import com.google.zxing.EncodeHintType;
import com.google.zxing.WriterException;
import com.google.zxing.qrcode.decoder.ErrorCorrectionLevel;
import eapli.ecafeteria.domain.account.exceptions.NoMoneyException;
import eapli.ecafeteria.domain.cafeteria.CafeteriaUser;
import eapli.ecafeteria.domain.meals.Meal;
import eapli.ecafeteria.domain.meals.MealType;
import eapli.framework.domain.AggregateRoot;
import eapli.framework.domain.Money;
import eapli.framework.dto.DTOable;
import eapli.framework.dto.GenericDTO;
import eapli.util.DateTime;
import eapli.util.QRCodeHandler;
import java.io.File;
import java.io.IOException;
import java.io.Serializable;
import java.time.temporal.ChronoUnit;
import java.util.Calendar;
import java.util.EnumMap;
import java.util.Map;
import java.util.Objects;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.ManyToOne;
import javax.persistence.Temporal;

/**
 * Domain class that represents a reservation of a meal. This class is created
 * throught builder pattern. Only the builder has permission to set the
 * reservation
 *
 * @author Jorge Pereira 1141216 \ Luís Magalhães 1141174
 */
@Entity
public class Reservation implements AggregateRoot<Integer>, Serializable, DTOable {

    private static String QR_CODE_CHARSET = "UTF-8";
    private static int QR_CODE_WIDTH = 200;
    private static int QR_CODE_HEIGHT = 200;

    /**
     * The hash of this reservation
     */
    @Column(name = "reservation_hash")
    @Id
    private Integer hash;

    /**
     * Meal that was booked
     */
    @ManyToOne(cascade = CascadeType.MERGE)
    private Meal meal;

    /**
     * Cafeteria user that made the reservation
     */
    @ManyToOne(cascade = CascadeType.MERGE)
    private CafeteriaUser user;

    /**
     * The date in which the reservation was made
     */
    @Column(name = "reservation_date")
    @Temporal(javax.persistence.TemporalType.DATE)
    private Calendar date;

    /**
     * The state of the reservation
     */
    @Column(name = "reservation_state")
    @javax.persistence.Enumerated
    private ReservationState state;

    /**
     * Reservation constructor for builder pattern
     *
     * @see ReservationBuilder
     */
    protected Reservation() {
        //Reservation is made with the builder pattern
        //if a reservation was made throught this constructor the object will
        //not be in a valid state
        //This is use for ORM
    }

    /**
     * Sets the cafeteria user that made the reservation
     *
     * @param user
     * @throws IllegalArgumentException
     * @throws IllegalStateException
     * @throws NoMoneyException
     */
    public void setUser(CafeteriaUser user) throws IllegalArgumentException, IllegalStateException, NoMoneyException {
        if (user == null) {
            throw new IllegalArgumentException("The user is invalid");
        }
        if (meal == null) {
            throw new IllegalStateException("You must first pick a meal");
        }
        if (!user.account().hasEnoughCredits(meal.dish().currentPrice())) {
            throw new NoMoneyException("The user has not enough money");
        }
        this.user = user;
    }

    /**
     * Sets the date in which the reservation was made
     *
     * @param date
     */
    public void setDate(Calendar date) {
        if (date == null) {
            throw new IllegalArgumentException("The date is invalid");
        }
        if (meal == null) {
            throw new IllegalStateException("You must first pick a meal");
        }
        //FIXME shouldn't exist an enumeration with the mealTypes?
        if (date.getTime().toInstant().until(meal.day().toInstant(), ChronoUnit.DAYS) <= 0) {
            if (meal.mealType().is(MealType.LUNCH)) {
                if (date.get(Calendar.HOUR_OF_DAY) >= 10) {
                    throw new IllegalArgumentException("The reservations for this meal are closed");
                }
            }
            if (meal.mealType().is(MealType.DINNER)) {
                if (date.get(Calendar.HOUR_OF_DAY) >= 16) {
                    throw new IllegalArgumentException("The reservations for this meal are closed");
                }
            }
        }
        this.date = date;
    }

    /**
     * Date of the reservation
     *
     * @return
     */
    public Calendar date() {
        return (Calendar) this.date.clone();
    }

    /**
     * Set the meal
     *
     * @param m
     */
    public void setMeal(Meal m) {
        if (m == null) {
            throw new IllegalArgumentException("The meal is invalid");
        }
        this.meal = m;
    }

    /**
     * Returns the reservation state
     *
     * @return the reservation state
     */
    protected ReservationState getState() {
        return state;
    }

    /**
     * Changes the state of the reservation to created
     *
     * @throws IllegalStateException
     */
    public void setCreated() throws IllegalStateException {
        if (this.state != null) {
            throw new IllegalStateException("The Reservation was already created");
        }
        if (user == null || date == null || meal == null) {
            throw new IllegalStateException("The Reservation is not valid.");
        }
        this.state = ReservationState.CREATED;
    }

    /**
     * Changes the state of the reservation to delivered
     *
     * @throws IllegalStateException
     */
    public void setDelivered() throws IllegalStateException {
        if (this.state == null || !this.createdState()) {
            throw new IllegalStateException("The Reservation is not in a valid state");
        }
        this.state = ReservationState.DELIVERED;
    }

    /**
     * Change the state of the reservation to canceled
     *
     * @throws IllegalStateException
     */
    public void setNotDelivered() throws IllegalStateException {
        if (this.state == null || !this.createdState()) {
            throw new IllegalStateException("The Reservation is not in a valid state");
        }
        this.state = ReservationState.NOT_DELIVERED;
    }

    /**
     * Change the state of the reservation to canceled
     *
     * @throws IllegalStateException
     */
    public void setCanceled() throws IllegalStateException {
        if (this.state == null || !this.createdState()) {
            throw new IllegalStateException("The Reservation is not in a valid state");
        }
        this.state = ReservationState.CANCELED;
    }

    /**
     * Check if the state of this reservation is created
     *
     * @return
     */
    public boolean createdState() {
        return this.state.equals(ReservationState.CREATED);
    }

    /**
     * Check if the state of this reservation is canceled
     *
     * @return
     */
    public boolean canceledState() {
        return this.state.equals(ReservationState.CANCELED);
    }

    /**
     * Check if the state of this reservation is not delivered
     *
     * @return
     */
    public boolean notDeliveredState() {
        return this.state.equals(ReservationState.NOT_DELIVERED);
    }

    /**
     * Check if the state of this reservation is delivered
     *
     * @return
     */
    public boolean deliveredState() {
        return this.state.equals(ReservationState.DELIVERED);
    }

    /**
     * Returns the cafeteria user that made the reservation
     *
     * @return Cafeteria User
     */
    public CafeteriaUser cafeteriaUser() {
        return this.user;
    }

    /**
     * Returns the meal that was reserved
     *
     * @return meal
     */
    public Meal meal() {
        return this.meal;
    }

    /**
     * The hascode
     *
     * @throws IllegalStateException if the reservation is not valid
     * @return
     */
    @Override
    public int hashCode() {
        if (getState() == null) {
            throw new IllegalStateException();
        }
        int hash = 5;
        hash = 7 * hash + Objects.hashCode(this.meal);
        hash = 7 * hash + Objects.hashCode(this.user);
        hash = 7 * hash + Objects.hashCode(this.date);
        this.hash = hash;

        return hash;
    }

    @Override
    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (obj == null) {
            return false;
        }
        if (getClass() != obj.getClass()) {
            return false;
        }
        final Reservation other = (Reservation) obj;
        return Objects.equals(this.hash, other.hash);
    }

    @Override
    public String toString() {
        return meal + "\n" + user;
    }

    @Override
    public boolean sameAs(Object other) {
        Reservation otherReservation = (Reservation) other;
        if (!this.date.equals(otherReservation.date)) {
            return false;
        }
        if (!this.hash.equals(otherReservation.hash)) {
            return false;
        }
        if (!this.meal.equals(otherReservation.meal)) {
            return false;
        }
        if (!this.user.equals(otherReservation.user)) {
            return false;
        }
        return true;
    }

    @Override
    public boolean is(Integer id) {
        return id == hashCode();
    }

    @Override
    public Integer id() {
        return hashCode();
    }

    @Override
    public GenericDTO toDTO() {
        GenericDTO dto = new GenericDTO("Reservation");
        dto.put("name", this.meal.toDTO());
        dto.put("user", this.user);
        dto.put("date", DateTime.format(date));
        return dto;
    }

    /**
     * Method that generatesQrCode Image on PC with the reservation hash
     */
    public void GenerateQrCode() {
        try {
            Map<EncodeHintType, ErrorCorrectionLevel> hintMap;
            hintMap = new EnumMap<>(EncodeHintType.class);
            hintMap.put(EncodeHintType.ERROR_CORRECTION, ErrorCorrectionLevel.L);
            String basePath = new File("").getAbsolutePath();
            File file = new File(basePath + "\\qrcode\\images\\" + this.hash + ".png");
            file.getParentFile().mkdirs();

            QRCodeHandler.createQRCodeToFile(String.valueOf(this.hash), file.getAbsolutePath(), QR_CODE_CHARSET, hintMap, QR_CODE_HEIGHT, QR_CODE_WIDTH);
        } catch (WriterException ex) {
            Logger.getLogger(Reservation.class.getName()).log(Level.SEVERE, null, ex);
        } catch (IOException ex) {
            Logger.getLogger(Reservation.class.getName()).log(Level.SEVERE, null, ex);
        }

    }

    /**
     * Method that cancels a reservation and refunds, or not, the user.
     *
     * If the lunch is cancelled before 10h, the user is fully refunded. If the
     * lunch is cancelled after 10h, the user is half refunded. If the dinner is
     * cancelled before 16h, the user is fully refunded. If the dinner is
     * cancelled after 16h, the user is half refunded.
     *
     * @return the refunded value
     */
    public Money cancelReservation() {

        setCanceled();

        if (date.getTime().toInstant().until(meal.day().toInstant(), ChronoUnit.DAYS) <= 0) {

            if (meal.mealType().is(MealType.LUNCH)) {
                if ((Calendar.HOUR_OF_DAY) >= 10) {
                    return meal.dish().currentPrice().multiply(0.5);
                }
            }

            if (meal.mealType().is(MealType.DINNER)) {
                if ((Calendar.HOUR_OF_DAY) >= 16) {
                    return meal.dish().currentPrice().multiply(0.5);
                }
            }
        }

        return meal.dish().currentPrice();

    }

}
