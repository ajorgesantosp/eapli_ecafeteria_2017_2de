package eapli.ecafeteria.domain.mealReservation;

import eapli.ecafeteria.domain.cafeteria.CafeteriaUser;
import eapli.ecafeteria.domain.meals.Meal;
import java.util.Calendar;
import org.eclipse.persistence.exceptions.IntegrityException;

/**
 * Reservation builder pattern
 *
 * @author Jorge Pereira 1141216 \ Luís Magalhães 1141174
 *
 */
public class ReservationBuilder {

    private final Reservation reservation;

    /**
     * Builder constructor
     */
    public ReservationBuilder() {
        reservation = new Reservation();
    }

    /**
     * Sets the meal
     *
     * @param meal
     * @return
     */
    public ReservationBuilder withMeal(Meal meal) {
        reservation.setMeal(meal);
        return this;
    }

    /**
     * Sets the cafeteria user that made the reservation
     *
     * @param user
     * @return
     */
    public ReservationBuilder withCafeteriaUser(CafeteriaUser user) {
        reservation.setUser(user);
        return this;
    }

    /**
     * Sets the date in which the reservation was made
     *
     * @param date
     * @return
     */
    public ReservationBuilder withDate(Calendar date) {
        reservation.setDate(date);
        return this;
    }

    /**
     * Builds a reservation checking the integrity of the object and defines the
     * state of the object to created in case of success
     *
     * @return the reservation
     * @throws IntegrityException
     */
    public Reservation build() throws IllegalStateException {
        reservation.setCreated();
        reservation.hashCode();
        reservation.GenerateQrCode();
        return reservation;
    }

}
