/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package eapli.ecafeteria.domain.kitchen;

import eapli.framework.domain.ValueObject;
import java.io.Serializable;
import java.util.ArrayList;
import javax.persistence.ElementCollection;
import javax.persistence.Embeddable;
import javax.persistence.Embedded;
import javax.persistence.FetchType;

/**
 *
 * @author calve
 */
@Embeddable
public class MealLots implements ValueObject, Serializable {

    private static final long serialVersionUID = 1L;

    @ElementCollection(fetch = FetchType.LAZY)
    @Embedded
    private ArrayList<Lot> lots;

    public MealLots(ArrayList<Lot> lots) {
        if (lots == null) {
            throw new IllegalArgumentException();
        }
        this.lots = new ArrayList<>();
        for (Lot l : lots) {
            add(l);
        }
    }

    public MealLots() {
        lots = new ArrayList<>();
    }

    public boolean add(Lot l) {
        if (lots.contains(l)) {
            return false;
        } else {
            lots.add(l);
            return true;
        }
    }

    public ArrayList<Lot> lotsList() {
        return this.lots;
    }

    public boolean contains(Lot l) {
        return lots.contains(l);
    }

    public boolean sameAs(MealLots other) {
        if (other.lotsList().size() != this.lots.size()) {
            return false;
        }

        for (int i = 0; i < this.lots.size(); i++) {
            String otherLot = other.lots.get(i).toString();
            String thisLot = this.lots.get(i).toString();

            if (!otherLot.equals(thisLot)) {
                return false;
            }
        }
        return true;
    }

}
