/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package eapli.ecafeteria.domain.kitchen;

import java.io.Serializable;

/**
 *
 * @author Rafael Delgado <Rafael.delgado at Rafael.org>
 *
 * /*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates and open the template
 * in the editor.
 */
import eapli.ecafeteria.domain.meals.Meal;
import eapli.framework.domain.AggregateRoot;
import java.util.Objects;
import javax.persistence.CascadeType;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.ManyToOne;
import javax.persistence.Version;

/**
 *
 * @author Rafael Delgado <Rafael.delgado at Rafael.org>
 */
@Entity
public class CookedMeal implements AggregateRoot<Meal>, Serializable {

     @Id
     @ManyToOne(cascade = CascadeType.MERGE)
     private Meal meal;

     int quantity;

     private MealLots lots;

     public CookedMeal() {
          //ORM
     }

     /**
      * Contructor of cooked meal
      *
      * @param meal meal to register quantity
      * @param quantity amount cooked
      */
     public CookedMeal(Meal meal, int quantity) {
          if (meal == null || quantity <= 0) {
               throw new IllegalStateException("New cooked meal info invalid");
          }
          this.meal = meal;
          this.quantity = quantity;
     }

     /**
      * Equals method
      *
      * @param o object to be compared
      * @return if equal return true, if not
      */
     @Override
     public boolean equals(Object o) {
          if (this == o) {
               return true;
          }
          if (!(o instanceof CookedMeal)) {
               return false;
          }
          final CookedMeal other = (CookedMeal) o;
          return id().equals(other.id());
     }

     /**
      * Same as Method
      * @param other other Cooked meal 
      * @return if they have same meal info then they returns true;
      */
     @Override
     public boolean sameAs(Object other) {
          if (!(other instanceof CookedMeal)) {
               return false;
          }
          final CookedMeal that = (CookedMeal) other;

          if (this == that) {
               return true;
          }

          return meal.sameAs(that.meal) && quantity == that.quantity;
     }

     /**
      * Return meal of cooked meal
      * @return meal
      */
     public Meal meal() {
          return this.meal;
     }

     /**
      * Returns quantity to cooked meal
      * @return quantity
      */
     public int quantity() {
          return this.quantity;
     }
     

     /**
      * Chenges info of  meal
      * @param newMeal the new info to be chenged
      */
     public void changeMealInfoTo(Meal newMeal) {
          if (newMeal == null) {
               throw new IllegalArgumentException("Invalid new cooked meal info");
          }

          this.meal = newMeal;
     }

     /**
      * Changes info of quantity of cooked meal
      * @param newQuantity new cooked meal quantity
      */
     public void changeQuantityTo(int newQuantity) {
          if (newQuantity <= 0) {
               throw new IllegalArgumentException("New quantity is invalid");
          }

          this.quantity = newQuantity;
     }

     @Override
     public int hashCode() {
          int hash = 3;
          hash = 79 * hash + Objects.hashCode(this.meal);
          return hash;
     }

     /**
      * Adds lot list to cooked meal
      * @param l lot info
      */
     public void addLot(Lot l) {
          if (!containsLot(l)) {
               this.lots.add(l);
          }
     }

     /**
      * Changes info of lot
      * @param lots 
      */
     public void changeLots(MealLots lots) {
          if (lots != null) {
               this.lots = lots;
          }
     }

     public boolean containsLot(Lot l) {
          return this.lots.contains(l);
     }

     /**
      * Compares cooked meal id (meal id)
      * @param id
      * @return 
      */
     @Override
     public boolean is(Meal id) {
          return this.id().equals(id);
     }

     /**
      * return cooked meal id
      * @return id of cooked meal
      */
     @Override
     public Meal id() {
          return this.meal();
     }

}
