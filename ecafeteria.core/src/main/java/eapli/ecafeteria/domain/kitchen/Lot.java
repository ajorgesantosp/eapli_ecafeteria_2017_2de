/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package eapli.ecafeteria.domain.kitchen;

import java.io.Serializable;
import javax.persistence.CascadeType;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.ManyToOne;
import javax.persistence.Version;

/**
 *
 * @author calve
 */
@Entity
public class Lot implements Serializable {

    private static final long serialVersionUID = 1L;

    @Version
    private Long version;

    @Id
    private String code;
    @ManyToOne(cascade = CascadeType.MERGE)
    private Material material;

    public Lot(String code, Material material) {
        if (code == null || material == null) {
            throw new IllegalStateException();
        }
        this.code = code;
        this.material = material;
    }

    protected Lot() {
        // for ORM
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (!(o instanceof Lot)) {
            return false;
        }
        final Lot that = (Lot) o;

        return this.code.equals(that.code);
    }

    @Override
    public int hashCode() {
        return this.code.hashCode();
    }

    @Override
    public String toString() {
        return "Code: " + this.code + " , Material: " + this.material.toString();
    }

    public String code() {
        return this.code;
    }

    public Material material() {
        return this.material;
    }

}
