package eapli.ecafeteria.domain.kitchen;

import eapli.framework.domain.AggregateRoot;
import eapli.framework.domain.Designation;
import java.io.Serializable;
import java.util.Objects;
import javax.persistence.EmbeddedId;
import javax.persistence.Entity;
import javax.persistence.Version;

/**
 *
 * @author Bruno Gomes <1151164@isep.ipp.pt>, Nuno Brito <1151173@isep.ipp.pt>
 */
@Entity
public class KitchenAlert implements AggregateRoot<Designation>, Serializable {

    private static final long serialVersionUID = 1L;
       
    @Version
    private Long version;
    
    @EmbeddedId
    private Designation name;
    private Integer limitP;

    public KitchenAlert() {
        // for ORM only
    }

    public KitchenAlert(Designation name, Integer limitP) {
        if (name == null | limitP == null | (limitP < 0 && limitP > 100)) {
            throw new IllegalStateException();
        }
        this.name = name;
        this.limitP = limitP;
    }

    public void setLimit(Integer limitP) {
        this.limitP = limitP;
    }
    
    public Integer limit() {
        return this.limitP;
    }
    
    @Override
    public boolean sameAs(Object other) {
        final KitchenAlert alert = (KitchenAlert) other;
        return this.equals(alert) && Objects.equals(limit(), alert.limit());
    }

    @Override
    public boolean is(Designation id) {
        return id.equals(id());
    }

    @Override
    public Designation id() {
        return this.name;
    }

    @Override
    public int hashCode() {
        return Objects.hashCode(this.name);
    }

    @Override
    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (!(obj instanceof KitchenAlert)) {
            return false;
        }

        final KitchenAlert other = (KitchenAlert) obj;
        return id().equals(other.id());
    }
    
    

}
