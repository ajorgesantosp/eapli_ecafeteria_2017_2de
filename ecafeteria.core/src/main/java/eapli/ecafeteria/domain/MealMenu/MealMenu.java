/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package eapli.ecafeteria.domain.MealMenu;

import eapli.framework.domain.AggregateRoot;
import eapli.framework.dto.DTOable;
import eapli.framework.dto.GenericDTO;
import eapli.util.DateTime;
import java.io.Serializable;
import java.util.Calendar;
import java.util.Objects;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.Temporal;

/**
 *
 * @author Rafael Delgado <Rafael.delgado at Rafael.org>
 */
@Entity
public class MealMenu implements AggregateRoot<Integer>, Serializable, DTOable {

     /*
     Hash code for meal menu
      */
     @Column(name = "mealmenu_id")
     @Id
     @GeneratedValue
     private Long id;

     @Column(name = "mealmenu_start_date")
     @Temporal(javax.persistence.TemporalType.DATE)
     private Calendar startDate;

     @Column(name = "mealmenu_end_date")
     @Temporal(javax.persistence.TemporalType.DATE)
     private Calendar endDate;

     @Column(name = "mealmenu_state")
     @javax.persistence.Enumerated
     private MealMenuState state;

     @Column(name = "description")
     private String description;

     static final String DESCRIPTION_BY_OMISSION = "Sem descricao";

     protected MealMenu() {
          //This is use for ORM
     }

     public MealMenu(Calendar startDate, Calendar endDate, String description) {
          MealMenu mealMenu = new MealMenu(startDate, endDate);
          if (startDate == null || endDate == null) {
               throw new IllegalStateException("Dates are invalid!");
          }

          if (!startDate.after(Calendar.getInstance()) || !endDate.after(Calendar.getInstance())) {
               throw new IllegalStateException("Dates are in the past");
          }

          if (!endDate.after(startDate)) {
               throw new IllegalStateException("Dates are invalid!");
          }

          this.startDate = startDate;
          this.endDate = endDate;
          this.state = MealMenuState.IN_PROGRESS;
          
          if (description!=null||!description.isEmpty()) {
          this.description = des;
               
          }else{
          this.description = DESCRIPTION_BY_OMISSION;
          }
         

     }

     public MealMenu(Calendar startDate, Calendar endDate) {
          if (startDate == null || endDate == null) {
               throw new IllegalStateException("Dates are invalid!");
          }

          if (!startDate.after(Calendar.getInstance()) || !endDate.after(Calendar.getInstance())) {
               throw new IllegalStateException("Dates are in the past");
          }

          if (!endDate.after(startDate)) {
               throw new IllegalStateException("Dates are invalid!");
          }

          this.startDate = startDate;
          this.endDate = endDate;
          this.state = MealMenuState.IN_PROGRESS;
          this.description = DESCRIPTION_BY_OMISSION;

     }

     public Calendar StartDate() {
          return (Calendar) this.startDate.clone();
     }

     public Calendar EndDate() {
          return (Calendar) this.endDate.clone();
     }

     /**
      *
      * @return
      */
     protected MealMenuState getState() {
          return state;
     }

     public void changeStateToPublished() throws IllegalStateException {
          if (this.state == null || !this.isInProgress()) {
               throw new IllegalStateException("The MealMeanu was already created");
          }
          if (startDate == null || endDate == null) {
               throw new IllegalStateException("The MealMenu is not valid");

          }

          this.state = MealMenuState.PUBLISHED;

     }

     public boolean isInProgress() {
          return this.state.equals(MealMenuState.IN_PROGRESS);
     }

     public boolean isPublished() {
          return this.state.equals(MealMenuState.PUBLISHED);

     }

     @Override
     public int hashCode() {
          if (getState() == null) {
               throw new IllegalStateException();
          }
          int hash = 5;
          hash = 89 * hash + Objects.hashCode(this.startDate);
          hash = 89 * hash + Objects.hashCode(this.endDate);
          hash = 89 * hash + Objects.hashCode(this.state);

          return hash;
     }

     @Override
     public boolean equals(Object obj) {
          if (this == obj) {
               return true;
          }
          if (obj == null) {
               return false;
          }
          if (getClass() != obj.getClass()) {
               return false;
          }
          final MealMenu other = (MealMenu) obj;
          return this.id.equals(other.id);
     }

     @Override
     public boolean is(Integer id) {
          return id == hashCode();
     }

     @Override
     public Integer id() {
          return hashCode();
     }

     public String description() {
          return this.description;
     }

     @Override
     public GenericDTO toDTO() {
          GenericDTO dto = new GenericDTO("MealMenu");
          dto.put("state", this.state);
          dto.put("startDate", DateTime.format(startDate));
          dto.put("endDate", DateTime.format(endDate));
          return dto;
     }

     @Override
     public boolean sameAs(Object other) {
          MealMenu otherMealMenu = (MealMenu) other;

          if (!this.startDate.equals(otherMealMenu.StartDate())) {
               return false;
          }
          if (!this.endDate.equals(otherMealMenu.EndDate())) {
               return false;
          }

          if (!this.state.equals(otherMealMenu.getState())) {
               return false;
          }
          return true;
     }

     public Long mealMenuID() {
          return this.id;
     }

     private void setDescrtpion(String description) {
          this.description = description;
     }

}
