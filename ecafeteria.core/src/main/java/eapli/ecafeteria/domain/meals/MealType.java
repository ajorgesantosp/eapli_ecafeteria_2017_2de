/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package eapli.ecafeteria.domain.meals;

import eapli.framework.domain.AggregateRoot;
import eapli.framework.domain.Designation;
import java.io.Serializable;
import java.util.Objects;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.Version;

/**
 *
 * @author Bruno Gomes <1151164@isep.ipp.pt>, Nuno Brito <1151173@isep.ipp.pt>
 */
@Entity
public class MealType implements AggregateRoot<String>, Serializable {

    private static final long serialVersionUID = 1L;
    
    @Version
    private Long version;
    
    @Id
    @GeneratedValue
    private Long id;
    
    @Column(unique = true)
    private String name;

    public static final String DINNER = "Jantar";
    public static final String LUNCH="Almoço";
    public MealType() {
        // for ORM only
    }
    
    public MealType(String name) {
        if(name == null || name.isEmpty())
            throw new IllegalArgumentException("Invalid Name");
        this.name = name;
        
    }
    
    @Override
    public boolean sameAs(Object other) {
        final MealType mealType = (MealType) other;
	return this.equals(mealType);
    }

    @Override
    public boolean is(String id) {
        return id.equals(id());
    }

    @Override
    public String id() {
        return this.name;
    }

    @Override
    public int hashCode() {
        int hash = 5;
        hash = 11 * hash + Objects.hashCode(this.name);
        return hash;
    }

    @Override
    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (obj == null) {
            return false;
        }
        if (getClass() != obj.getClass()) {
            return false;
        }
        final MealType other = (MealType) obj;
        return Objects.equals(this.name, other.name);
    }

    @Override
    public String toString() {
        return "MealType{" + "name=" + name + '}';
    }
    
}
