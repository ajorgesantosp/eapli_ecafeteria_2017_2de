/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package eapli.ecafeteria.domain.meals;

import eapli.framework.domain.Designation;
import java.io.Serializable;
import javax.persistence.EmbeddedId;
import javax.persistence.Entity;
import javax.persistence.Version;

/**
 *
 * @author calve
 */
@Entity
public class Allergen implements Serializable {

    private static final long serialVersionUID = 1L;

    @Version
    private Long version;

    @EmbeddedId
    private Designation name;

    public Allergen(Designation name) {
        if (name == null) {
            throw new IllegalArgumentException();
        }
        this.name = name;
    }

    protected Allergen() {
        // for ORM
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (!(o instanceof Allergen)) {
            return false;
        }

        final Allergen that = (Allergen) o;

        return this.name.equals(that.name);
    }

    @Override
    public int hashCode() {
        return this.name.hashCode();
    }

    @Override
    public String toString() {
        return this.name.toString();
    }

    public Designation name() {
        return name;
    }
    
    public boolean sameAs(Allergen other){
        
        if (!other.name().toString().equals(this.name.toString())) {
            return false;
        } else  return true;
    }

}
