/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package eapli.ecafeteria.domain.meals;

import eapli.framework.domain.ValueObject;
import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;
import javax.persistence.ElementCollection;
import javax.persistence.Embeddable;
import javax.persistence.Embedded;
import javax.persistence.FetchType;

/**
 *
 * @author calve
 */
@Embeddable
public class DishAllergens implements ValueObject, Serializable {

    private static final long serialVersionUID = 1L;

    @ElementCollection(fetch = FetchType.LAZY)
    @Embedded
    private List<Allergen> allergens;

    public DishAllergens(ArrayList<Allergen> allergens) {
        if (allergens == null) {
            throw new IllegalArgumentException();
        }
        this.allergens = new ArrayList<>();
        for (Allergen a : allergens) {
            add(a);
        }
    }

    protected DishAllergens() {
        allergens = new ArrayList<>();
    }

    public boolean add(Allergen a) {
        if (allergens.contains(a)) {
            return false;
        } else {
            allergens.add(a);
            return true;
        }
    }

    public List<Allergen> allergernsList() {
        return this.allergens;
    }

    public boolean sameAs(DishAllergens other) {
        if (other.allergernsList().size() != this.allergens.size()) {
            return false;
        }

        for (int i = 0; i < this.allergens.size(); i++) {
            String otherAllergern = other.allergens.get(i).toString();
            String thisAllergern = this.allergens.get(i).toString();

            if (!otherAllergern.equals(thisAllergern)) {
                return false;
            }
        }
        return true;
    }

}
