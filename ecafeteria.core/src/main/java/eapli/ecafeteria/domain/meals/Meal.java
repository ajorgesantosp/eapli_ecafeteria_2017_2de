package eapli.ecafeteria.domain.meals;

import eapli.ecafeteria.domain.MealMenu.MealMenu;
import eapli.framework.domain.AggregateRoot;
import eapli.framework.dto.DTOable;
import eapli.framework.dto.GenericDTO;
import java.io.Serializable;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Objects;
import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.ManyToOne;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import javax.persistence.Version;

/**
 *
 * @author Bruno Gomes <1151164@isep.ipp.pt>, Nuno Brito <1151173@isep.ipp.pt>
 */
@Entity
public class Meal implements AggregateRoot<Long>, Serializable, DTOable {

    private static final long serialVersionUID = 1L;
    @Id
    @GeneratedValue
    private Long id;
    @Version
    private Long version;

    @Temporal(value = TemporalType.DATE)
    @Column(name = "meal_day")
    private Calendar day;
    @ManyToOne(cascade = CascadeType.MERGE)
    private MealType mealType;

    @ManyToOne(cascade = CascadeType.MERGE)
    private MealMenu mealMenu;

    @ManyToOne(cascade = CascadeType.MERGE)
    private Dish dish;

    public Meal() {
        // for ORM only
    }

    public Meal(Calendar day, MealType mealType, Dish dish, MealMenu mealmenu) {
        if (day == null || mealType == null || dish == null || mealmenu == null) {
            throw new IllegalStateException("Invalid date for meal");
        }
        if (day.before(mealmenu.StartDate()) || day.after(mealmenu.EndDate())) {
            throw new IllegalStateException("The meal Date is not valid");
        }
        this.day = day;
        this.mealType = mealType;
        this.dish = dish;
        this.mealMenu = mealmenu;
    }

    public Meal(Calendar day, MealType mealType, Dish dish) {
        if (day == null || mealType == null || dish == null) {
            throw new IllegalStateException("Invalid date for meal");
        }

        this.day = day;
        this.mealType = mealType;
        this.dish = dish;
    }

    @Override
    public int hashCode() {
        int hash = 3;
        hash = 17 * hash + Objects.hashCode(this.id);
        hash = 17 * hash + Objects.hashCode(this.day);
        hash = 17 * hash + Objects.hashCode(this.mealType);
        hash = 17 * hash + Objects.hashCode(this.dish);
        return hash;
    }

    @Override
    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (obj == null) {
            return false;
        }
        if (getClass() != obj.getClass()) {
            return false;
        }
        final Meal other = (Meal) obj;
        return Objects.equals(this.id, other.id);
    }

    @Override
    public String toString() {
        return "Meal{" + "id=" + id + ", day=" + day + ", mealType=" + mealType + ", dish=" + dish + '}';
    }

    @Override
    public boolean sameAs(Object other) {
        final Meal meal = (Meal) other;
        return this.equals(meal) && day().equals(meal.day()) && mealType() == meal.mealType() && dish() == meal.dish();
    }

    @Override
    public boolean is(Long id) {
        return id.equals(id());
    }

    @Override
    public Long id() {
        return this.id;
    }

    public Calendar day() {
        return this.day;
    }

    public MealType mealType() {
        return this.mealType;
    }

    public Dish dish() {
        return this.dish;
    }

    public MealMenu mealMenu() {
        return this.mealMenu;
    }

    @Override
    public GenericDTO toDTO() {
        GenericDTO dto = new GenericDTO("Meal");
        dto.put("id", this.dish.id());
        dto.put("name", this.dish.name());
        dto.put("price", this.dish.currentPrice());
        dto.put("dish type", this.dish.dishType());
        dto.put("calories", this.dish.nutricionalInfo().calories());
        dto.put("salt", this.dish.nutricionalInfo().salt());
        dto.put("day", this.day.toString());
        dto.put("meal menu", this.mealMenu.toString());

        dto.put("meal type", this.mealType.id());
        return dto;
    }
}
