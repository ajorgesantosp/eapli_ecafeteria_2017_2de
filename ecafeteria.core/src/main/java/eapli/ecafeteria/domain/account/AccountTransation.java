/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package eapli.ecafeteria.domain.account;

import eapli.ecafeteria.domain.account.exceptions.NoMoneyException;
import eapli.framework.domain.Money;
import java.io.Serializable;
import javax.persistence.CascadeType;
import javax.persistence.Embedded;
import javax.persistence.Entity;
import javax.persistence.Enumerated;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.ManyToOne;
import javax.persistence.Version;

/**
 *
 * @author Alexandre Gomes e André Chaves
 */
@Entity
public class AccountTransation implements Serializable {

    private static final long serialVersionUID = 1L;

    @Version
    private Long version;

    @Id
    @GeneratedValue
    private Long id;
    
    /**
     * Account of the user
     */
    @ManyToOne(cascade = CascadeType.MERGE)     
    private Account acc;
    
    /**
     * Money
     */
    @Embedded
    private Money value;
    
    /**
     * Type of transation
     */
    @Enumerated
    private TransationType transation;
    
    /**
     * Balance after transations
     */
    @Embedded
    private Money balanceAfterTransation;

    /**
     * AccountTransation Constructor
     */
    protected AccountTransation() {
        //Used for ORM
    }

    /**
     * Constructor of AccountTransation
     * @param acc account
     * @param value money
     * @param transation type of transation
     */
    public AccountTransation(Account acc, Money value, TransationType transation) {
        this.acc = acc;
        this.value = value;
        this.transation = transation;
    }

    /**
     * Method that does the transations
     * In case of CREDIT -> Add Funds to the account
     * In case of DEBIT -> Subtract Funds to the account
     */
    public void doTransation() {

        switch (transation) {
            case CREDIT:
                balanceAfterTransation = acc.balance().add(this.value);
                break;
            case DEBIT:
                if (!acc.hasEnoughCredits(this.value)) {
                    throw new NoMoneyException();
                }
                balanceAfterTransation = acc.balance().subtract(this.value);
                break;
        }
    }

    /**
     * Obtains the balance after transations
     * 
     * @return balance
     */
    public Money getValue() {
        return balanceAfterTransation;
    }

    /**
     * Types of transation
     */
    public enum TransationType {
        CREDIT,
        DEBIT;
    }
    
    /**
     * States of a transation
     */
    public enum TransationState {
        CANCEL,
        DONE;
    }

}
