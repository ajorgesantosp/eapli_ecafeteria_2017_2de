/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package eapli.ecafeteria.domain.account.exceptions;

/**
 *
 * @author Alexandre Gomes e André Chaves
 */
public class NoMoneyException extends IndexOutOfBoundsException{
    
    public NoMoneyException(){
        
    }
    
    public NoMoneyException(String message){
        super(message);
    }
}
