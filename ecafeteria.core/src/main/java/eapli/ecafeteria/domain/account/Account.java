/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package eapli.ecafeteria.domain.account;

import eapli.framework.domain.AggregateRoot;
import eapli.framework.domain.Money;
import java.io.Serializable;
import java.util.LinkedList;
import javax.persistence.CascadeType;
import javax.persistence.ElementCollection;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.Id;
import javax.persistence.OneToMany;
import javax.persistence.Version;

/**
 *
 * @author André Chaves e Alexandre Gomes
 */
@Entity
public class Account implements Serializable, AggregateRoot<String> {

    private static final long serialVersionUID = 1L;

    @Version
    private Long version;

    /**
     * Bank Account information
     */
    @Id
    private String bankAccount;

    /**
     * List of transations
     */
    @ElementCollection(fetch = FetchType.LAZY)
    @OneToMany(cascade = CascadeType.MERGE)
    private LinkedList<AccountTransation> accTransation;

    /**
     * Account constructor 
     *
     */
    protected Account() {
        //Used for ORM 
    }

    /**
     * Constructor of Account
     * 
     * @param bankAccount bank account information 
     */
    public Account(String bankAccount) {

        this.bankAccount = bankAccount;
        this.accTransation = new LinkedList<>();
    }

    /**
     * Returns Bank Account information
     * @return bank account
     */
    public String getBankAccount() {
        return bankAccount;
    }

    /**
     * Obtains the balance of the user
     * 
     * @return balance
     */
    public Money balance() {

        if (!accTransation.isEmpty()) {
            Money balance = accTransation.getLast().getValue();

            return balance;
        }

        return Money.euros(0);
    }

    /**
     * Validates if the user has enough money
     * 
     * @param value amount of money
     * @return balance greater or equal 
     */
    public boolean hasEnoughCredits(Money value) {
        return balance().greaterThanOrEqual(value);
    }

    @Override
    public boolean sameAs(Object other) {
        Account otherAcc = (Account) other;
        return this.equals(otherAcc);
    }

    @Override
    public boolean is(String id) {
        return this.bankAccount.equals(id);
    }

    @Override
    public String id() {
        return this.bankAccount;
    }

    

    /**
     * Registates a transation
     * 
     * @param transation transation
     * @throws IllegalAccessException exception
     */
    public void registTransaction(AccountTransation transation) throws IllegalAccessException {

        if (transation != null) {
            transation.doTransation();
            accTransation.addLast(transation);
        } else {
            throw new IllegalAccessException("Transation Failed!");
        }

    }

}
