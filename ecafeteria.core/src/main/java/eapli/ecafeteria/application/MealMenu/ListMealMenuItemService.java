/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package eapli.ecafeteria.application.MealMenu;

import eapli.ecafeteria.Application;
import eapli.ecafeteria.domain.MealMenu.MealMenu;
import eapli.ecafeteria.domain.authz.ActionRight;
import eapli.ecafeteria.domain.meals.Meal;
import eapli.ecafeteria.persistence.MealRepository;
import eapli.ecafeteria.persistence.PersistenceContext;

/**
 *
 * @author Rafael Delgado <Rafael.delgado at Rafael.org>
 */
public class ListMealMenuItemService {

    private MealRepository repo = PersistenceContext.repositories().meals();

    Iterable<Meal> mealMenuItemsOfMealMenu(MealMenu mealMenu) {
        Application.ensurePermissionOfLoggedInUser(ActionRight.MANAGE_MENUS);
        
        return this.repo.mealsOfMealMenu(mealMenu);
    }
}
