/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package eapli.ecafeteria.application.MealMenu;

import eapli.ecafeteria.Application;
import eapli.ecafeteria.domain.MealMenu.MealMenu;
import eapli.ecafeteria.domain.authz.ActionRight;
import eapli.ecafeteria.persistence.MealMenuRepository;
import eapli.ecafeteria.persistence.PersistenceContext;

/**
 *
 * @author Rafael Delgado <Rafael.delgado at Rafael.org>
 */
public class ListMealMenuService {

     private MealMenuRepository repo = PersistenceContext.repositories().mealMenu();

     public Iterable<MealMenu> getAllMealMenuInProgress() {
          Application.ensurePermissionOfLoggedInUser(ActionRight.MANAGE_MENUS);
          
          return this.repo.allInProgress();
     }

}
