/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package eapli.ecafeteria.application.MealMenu;

import eapli.ecafeteria.application.meals.ListMealService;
import eapli.ecafeteria.domain.MealMenu.MealMenu;
import eapli.ecafeteria.domain.meals.Meal;
import eapli.ecafeteria.persistence.MealMenuRepository;
import eapli.ecafeteria.persistence.PersistenceContext;
import eapli.framework.application.Controller;
import eapli.framework.persistence.DataConcurrencyException;
import eapli.framework.persistence.DataIntegrityViolationException;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 *
 * @author Rafael Delgado <Rafael.delgado at Rafael.org>
 */
public class PublishMealMenuController implements Controller {

     private MealMenuRepository repoMealMenu = PersistenceContext.repositories().mealMenu();

     public Iterable<MealMenu> allMealMenuInProgress() {
          final ListMealMenuService mealMenuService = new ListMealMenuService();
          return mealMenuService.getAllMealMenuInProgress();
     }

     public Iterable<Meal> MealsFromMenu(MealMenu mealMenu) {
          final ListMealService mealService = new ListMealService();
          return mealService.mealsFromMealMenu(mealMenu);
     }

     public void publishMealMenu(MealMenu mealMenu) {
         try {
             mealMenu.changeStateToPublished();
             repoMealMenu.save(mealMenu);
         } catch (DataConcurrencyException | DataIntegrityViolationException ex) {
             Logger.getLogger(PublishMealMenuController.class.getName()).log(Level.SEVERE, null, ex);
         }
     }
}
