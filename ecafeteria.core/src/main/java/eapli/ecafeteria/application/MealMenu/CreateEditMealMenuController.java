/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package eapli.ecafeteria.application.MealMenu;

import eapli.ecafeteria.Application;
import eapli.ecafeteria.application.meals.ListDishService;
import eapli.ecafeteria.domain.MealMenu.MealMenu;
import eapli.ecafeteria.domain.authz.ActionRight;
import eapli.ecafeteria.domain.meals.Dish;
import eapli.ecafeteria.domain.meals.DishType;
import eapli.ecafeteria.domain.meals.Meal;
import eapli.ecafeteria.persistence.MealMenuRepository;
import eapli.ecafeteria.persistence.PersistenceContext;
import eapli.framework.application.Controller;
import eapli.framework.persistence.DataConcurrencyException;
import eapli.framework.persistence.DataIntegrityViolationException;
import java.util.Calendar;
import eapli.ecafeteria.application.meals.ListDishTypeService;
import eapli.ecafeteria.application.meals.ListMealService;
import eapli.ecafeteria.application.meals.ListMealTypeService;
import eapli.ecafeteria.domain.meals.MealType;
import eapli.ecafeteria.persistence.MealRepository;

/**
 *
 * @author Rafael Delgado <Rafael.delgado at Rafael.org>
 */
public class CreateEditMealMenuController implements Controller {

     private MealMenuRepository repoMealMenu = PersistenceContext.repositories().mealMenu();
     private MealRepository repoMeal = PersistenceContext.repositories().meals();

     public Iterable<MealMenu> getAllMenusInProgress() {
          final ListMealMenuService mealMenuService = new ListMealMenuService();
          return mealMenuService.getAllMealMenuInProgress();
     }

     public MealMenu createMealMenu(Calendar startDate, Calendar endDate) throws DataConcurrencyException, DataIntegrityViolationException {
          Application.ensurePermissionOfLoggedInUser(ActionRight.MANAGE_MENUS);
          MealMenu mealMenu = new MealMenu(startDate, endDate);

          return this.repoMealMenu.save(mealMenu);

     }

     public Iterable<Dish> getDishWithDishType(DishType dishType) {
          final ListDishService dishService = new ListDishService();
          return dishService.allDishesWithDishType(dishType);
     }

     public Iterable<DishType> allDishTypes() {
          final ListDishTypeService dishTypeService = new ListDishTypeService();
          return dishTypeService.activeDishTypes();

     }

     public Iterable<Meal> mealsFromMealMenu(MealMenu mealMenu) {
          final ListMealService mealService = new ListMealService();
          return mealService.mealsFromMealMenu(mealMenu);
     }

     public boolean removeMealFromMenu(Meal meal) throws DataIntegrityViolationException {
          repoMeal.delete(meal);
          return true;
     }

     public Meal registerMeal(Calendar date, MealType mealType, Dish dish, MealMenu mealMenu) throws DataConcurrencyException, DataIntegrityViolationException {
          Meal meal = new Meal(date, mealType, dish, mealMenu);
          return this.repoMeal.save(meal);
     }


     public Iterable<MealType> allMealTypes() {
          final ListMealTypeService mealTypeService = new ListMealTypeService();
          return mealTypeService.all();

     }

}
