/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package eapli.ecafeteria.application.kitchen;

import eapli.ecafeteria.Application;
import eapli.ecafeteria.application.meals.ListMealService;
import eapli.ecafeteria.application.meals.ListMealTypeService;
import eapli.ecafeteria.domain.authz.ActionRight;
import eapli.ecafeteria.domain.kitchen.CookedMeal;
import eapli.ecafeteria.domain.meals.Meal;
import eapli.ecafeteria.domain.meals.MealType;
import eapli.ecafeteria.persistence.CookedMealRepository;
import eapli.ecafeteria.persistence.PersistenceContext;
import eapli.framework.application.Controller;
import eapli.framework.persistence.DataConcurrencyException;
import eapli.framework.persistence.DataIntegrityViolationException;
import java.util.Calendar;
import java.util.Date;

/**
 *
 * @author Rafael Delgado <Rafael.delgado at Rafael.org> && João Sabugueiro<1150981@isep.ipp.pt>
 */
public class RegisterCookedMealController implements Controller {

     private CookedMealRepository repo = PersistenceContext.repositories().cookedMeal();

     public Iterable<MealType> getAllMealType() {
          final ListMealTypeService mealTypeService = new ListMealTypeService();
          return mealTypeService.all();

     }

     public Iterable<Meal> getMeals(MealType theMealType, Calendar date) {
          final ListMealService mealService = new ListMealService();
          return mealService.getMealsByDateAndType(theMealType, date);
     }

     public CookedMeal registerCookedMeal(Meal theMeal, int quantity) throws DataConcurrencyException, DataIntegrityViolationException {

          Application.ensurePermissionOfLoggedInUser(ActionRight.MANAGE_KITCHEN);
          CookedMeal cookedMeal = new CookedMeal(theMeal, quantity);

          return this.repo.save(cookedMeal);

     }

}
