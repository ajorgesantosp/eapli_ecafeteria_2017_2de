/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package eapli.ecafeteria.application.kitchen;

import eapli.ecafeteria.domain.kitchen.CookedMeal;
import eapli.ecafeteria.domain.kitchen.Lot;
import eapli.ecafeteria.persistence.CookedMealRepository;
import eapli.ecafeteria.persistence.PersistenceContext;

/**
 *
 * @author Rafael Delgado <Rafael.delgado at Rafael.org>
 */
public class ListCookedMealsService {

    private CookedMealRepository repo = PersistenceContext.repositories().cookedMeal();

    public Iterable<CookedMeal> allCookedMeals() {
        return this.repo.findAll();
    }

    public Iterable<CookedMeal> findByLot(Lot l) {
        return this.repo.findByLot(l);
    }

}
