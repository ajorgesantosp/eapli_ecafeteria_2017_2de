/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package eapli.ecafeteria.application.kitchen;

import eapli.ecafeteria.Application;
import eapli.ecafeteria.domain.authz.ActionRight;
import eapli.ecafeteria.domain.kitchen.CookedMeal;
import eapli.ecafeteria.domain.kitchen.Lot;
import eapli.ecafeteria.domain.kitchen.MealLots;
import eapli.ecafeteria.persistence.CookedMealRepository;
import eapli.ecafeteria.persistence.LotRepository;
import eapli.ecafeteria.persistence.PersistenceContext;
import eapli.framework.application.Controller;
import eapli.framework.persistence.DataConcurrencyException;
import eapli.framework.persistence.DataIntegrityViolationException;
import java.util.ArrayList;

/**
 *
 * @author calve
 */
public class RegisterMealLotsController implements Controller {
    
    private ListLotService lotSvc = new ListLotService();
    private ListCookedMealsService mealSvc = new ListCookedMealsService();
    
    private LotRepository lotRepository = PersistenceContext.repositories().lots();
    private CookedMealRepository mealRepository = PersistenceContext.repositories().cookedMeal();
    
    public Iterable<CookedMeal> meals() {
        return this.mealSvc.allCookedMeals();
    }
    
    public Iterable<Lot> lots() {
        return this.lotSvc.allLots();
    }
    
    public CookedMeal registerMealLots(CookedMeal theMeal, ArrayList<Lot> mealLots) throws DataConcurrencyException, DataIntegrityViolationException {
        Application.ensurePermissionOfLoggedInUser(ActionRight.MANAGE_KITCHEN);
        theMeal.changeLots(new MealLots(mealLots));
        CookedMeal ret = this.mealRepository.save(theMeal);
        return ret;
    }
    
}
