/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package eapli.ecafeteria.application.kitchen;

import eapli.ecafeteria.application.meals.ListMealService;
import eapli.ecafeteria.domain.kitchen.CookedMeal;
import eapli.ecafeteria.domain.kitchen.Lot;
import eapli.ecafeteria.domain.meals.Meal;
import eapli.ecafeteria.persistence.CookedMealRepository;
import eapli.ecafeteria.persistence.LotRepository;
import eapli.ecafeteria.persistence.MealRepository;
import eapli.ecafeteria.persistence.PersistenceContext;
import eapli.framework.application.Controller;
import java.util.ArrayList;

/**
 *
 * @author calve
 */
public class SearchByLotController implements Controller {

    private ListLotService lotSvc = new ListLotService();
    private ListCookedMealsService mealSvc = new ListCookedMealsService();

    private LotRepository lotRepository = PersistenceContext.repositories().lots();
    private CookedMealRepository mealRepository = PersistenceContext.repositories().cookedMeal();

    public Iterable<CookedMeal> meals(Lot lot) {
        
        Iterable<CookedMeal> allMeals = this.mealSvc.allCookedMeals();
        ArrayList<CookedMeal> validMeals = new ArrayList<>();
        for (CookedMeal m : allMeals) {
            if (m.containsLot(lot)) {
                validMeals.add(m);
            }
        }
        return validMeals;
         
       // return this.mealSvc.findByLot(lot);
    }

    public Iterable<Lot> lots() {
        return this.lotSvc.allLots();
    }

}
