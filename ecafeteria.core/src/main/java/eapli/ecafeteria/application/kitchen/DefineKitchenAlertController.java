package eapli.ecafeteria.application.kitchen;

import eapli.ecafeteria.application.meals.ListKitchenAlertService;
import eapli.ecafeteria.domain.kitchen.KitchenAlert;
import eapli.ecafeteria.persistence.KitchenAlertRepository;
import eapli.ecafeteria.persistence.PersistenceContext;
import eapli.framework.application.Controller;
import eapli.framework.domain.Designation;
import eapli.framework.persistence.DataConcurrencyException;
import eapli.framework.persistence.DataIntegrityViolationException;

/**
 *
 * @author Bruno Gomes <1151164@isep.ipp.pt>, Nuno Brito <1151173@isep.ipp.pt>
 */
public class DefineKitchenAlertController implements Controller {

    private ListKitchenAlertService serviceListDish = new ListKitchenAlertService();
    private KitchenAlertRepository oRep = PersistenceContext.repositories().kitchenAlerts();
    
    public Iterable<KitchenAlert> kitchenAlerts() {
        return this.serviceListDish.all();
    }
    
    public KitchenAlert updateLimit(KitchenAlert ka, Integer limit) throws DataConcurrencyException, DataIntegrityViolationException, IllegalStateException {
        ka.setLimit(limit);
        
        KitchenAlert other;
        switch (ka.id().toString()) {
            case "Vermelho":
                other = serviceListDish.findByName(Designation.valueOf("Amarelo"));
                if (ka.limit()<=other.limit())
                    throw new IllegalStateException("Red alert cannot be less than yellow.");
                break;
            case "Amarelo":
                other = serviceListDish.findByName(Designation.valueOf("Vermelho"));
                if (ka.limit()>=other.limit())
                    throw new IllegalStateException("Yellow alert cannot be greater than red.");
                break;
        }
        KitchenAlert ret = this.oRep.save(ka);
        return ret;
    }
}
