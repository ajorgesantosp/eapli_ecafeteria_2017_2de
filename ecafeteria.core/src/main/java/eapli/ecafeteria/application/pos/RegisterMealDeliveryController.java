package eapli.ecafeteria.application.pos;

import com.google.zxing.EncodeHintType;
import com.google.zxing.NotFoundException;
import com.google.zxing.qrcode.decoder.ErrorCorrectionLevel;
import eapli.ecafeteria.domain.mealReservation.Reservation;
import eapli.ecafeteria.persistence.MealReservationRepository;
import eapli.ecafeteria.persistence.PersistenceContext;
import eapli.framework.application.Controller;
import eapli.framework.persistence.DataConcurrencyException;
import eapli.framework.persistence.DataIntegrityViolationException;
import eapli.util.DateTime;
import java.io.IOException;
import java.util.Calendar;
import java.util.EnumMap;
import java.util.Map;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 *
 * @author Fábio Marques Carneiro e André Sousa
 */
public class RegisterMealDeliveryController implements Controller {

    private final MealReservationRepository reservationRepo = PersistenceContext.repositories().mealsReservations(null);
    private final CloseShiftService service = new CloseShiftService();
    private final String CHARSET = "UTF-8";
    private final String INVALID_RESERVATION = "Invalid Reservation or Reservation State";

    private String qrCodeHash = "";

    public String validateQrCode(String FilePath) {
        if ("".equals(FilePath)) {
            throw new IllegalArgumentException();
        }

        Map<EncodeHintType, ErrorCorrectionLevel> hintMap = new EnumMap<>(EncodeHintType.class);
        hintMap.put(EncodeHintType.ERROR_CORRECTION, ErrorCorrectionLevel.L);
        try {
            qrCodeHash = eapli.util.QRCodeHandler.readQRCodeFromFile(FilePath, CHARSET, hintMap);

        } catch (IOException | NotFoundException | NullPointerException ex) {
            Logger.getLogger(RegisterMealDeliveryController.class.getName()).log(Level.SEVERE, null, ex);
            return "";
        }

        try {
            Integer reservePk = Integer.valueOf(qrCodeHash);
            Reservation reservation = reservationRepo.findReserveCreatedHashDay(reservePk, DateTime.now(), service.getMealType());
            if (reservation != null) {
                return reservation.toString();
            }
        } catch (Exception ex) {
            Logger.getLogger(RegisterMealDeliveryController.class.getName()).log(Level.SEVERE, null, ex);
        }

        return "";
    }

    public String confirmReserveDelivery() {
        try {
            Integer reservePk = Integer.valueOf(qrCodeHash);
            Reservation reservation = reservationRepo.findReserveCreatedHashDay(reservePk, DateTime.now(), service.getMealType());
            if (reservation != null) {
                reservation.setDelivered();
                try {
                    reservationRepo.save(reservation);
                } catch (DataConcurrencyException ex) {
                    Logger.getLogger(RegisterMealDeliveryController.class.getName()).log(Level.SEVERE, null, ex);
                } catch (DataIntegrityViolationException ex) {
                    Logger.getLogger(RegisterMealDeliveryController.class.getName()).log(Level.SEVERE, null, ex);
                }
                return reservation.toString();
            }
        } catch (NumberFormatException | IllegalStateException ex) {
            Logger.getLogger(RegisterMealDeliveryController.class.getName()).log(Level.SEVERE, null, ex);
        }

        return INVALID_RESERVATION;
    }

}
