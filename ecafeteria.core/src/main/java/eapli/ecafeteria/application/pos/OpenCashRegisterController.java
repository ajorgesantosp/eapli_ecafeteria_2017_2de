/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package eapli.ecafeteria.application.pos;

import eapli.ecafeteria.Application;
import eapli.ecafeteria.domain.authz.ActionRight;
import eapli.ecafeteria.domain.cafeteria.CashRegister;
import eapli.ecafeteria.domain.cafeteria.Shift;
import eapli.ecafeteria.domain.meals.MealType;
import eapli.ecafeteria.persistence.CashRegisterRepository;
import eapli.ecafeteria.persistence.MealTypeRepository;
import eapli.ecafeteria.persistence.PersistenceContext;
import eapli.framework.application.Controller;
import eapli.framework.persistence.DataConcurrencyException;
import eapli.framework.persistence.DataIntegrityViolationException;
import eapli.util.DateTime;
import java.util.Calendar;

/**
 *
 * @author Jorge Pereira 1141216 \ Luís Magalhães 1141174
 */
public class OpenCashRegisterController implements Controller {

    private final CashRegister cashRegister = PersistenceContext.repositories().cashRegister(null).find();
    private final MealTypeRepository repo = PersistenceContext.repositories().mealTypes();
    private final CashRegisterRepository cashRepo = PersistenceContext.repositories().cashRegister(null);

    /**
     * Get the default meal type by the current hour
     *
     * @return
     */
    public String defaultMealType() {
        return Shift.defaultMealType();
    }

    /**
     * Get the current date(default)
     *
     * @return
     */
    public Calendar defaultDate() {
        return Shift.defaultDate();
    }

    /**
     * List the meal types avaiable
     *
     * @return
     */
    public Iterable<MealType> listMealTypes() {
        return repo.findAll();
    }

    public boolean startUseCase() {
        Application.ensurePermissionOfLoggedInUser(ActionRight.SALE);
        return !cashRegister.isOpen();
    }

    /**
     * Open the cash register
     *
     * @throws eapli.framework.persistence.DataConcurrencyException
     * @throws eapli.framework.persistence.DataIntegrityViolationException
     *
     * @param mt
     * @param date
     */
    public void openCashRegister(MealType mt, Calendar date) throws DataConcurrencyException, DataIntegrityViolationException {
        Application.ensurePermissionOfLoggedInUser(ActionRight.SALE);
        cashRegister.openCashRegister(mt, date);
        saveCashRegister();
    }

    /**
     * Save the cash register
     *
     * @throws DataConcurrencyException
     * @throws DataIntegrityViolationException
     */
    private void saveCashRegister() throws DataConcurrencyException, DataIntegrityViolationException {
        cashRepo.save(cashRegister);
    }
}
