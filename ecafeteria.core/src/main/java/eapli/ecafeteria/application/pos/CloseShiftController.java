/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package eapli.ecafeteria.application.pos;

import eapli.ecafeteria.domain.mealReservation.Reservation;
import eapli.framework.application.Controller;
import eapli.framework.persistence.DataConcurrencyException;
import eapli.framework.persistence.DataIntegrityViolationException;

/**
 *
 * @author Miguel
 */
public class CloseShiftController implements Controller {

    private final CloseShiftService service = new CloseShiftService();
    private int numCreated = 0;
    private int numCancelled = 0;
    private int numDelivered = 0;
    private final Iterable<Reservation> list = getMealsSummary();
    
    public boolean getCashState() {
        return service.getCashState();
    }

    private Iterable<Reservation> getMealsSummary() {
        return service.getMealsSummary();
    }

    public void close() throws DataConcurrencyException, DataIntegrityViolationException {

        // Number of not delivered will be the number of created
        // Number of created will be a sum of delivered, created, and cancelled
        service.registNotDelivered(list, numCreated, (numCreated + numDelivered + numCancelled) , numDelivered, numCancelled);
    }

    public int getCreatedReservations() {
        numCreated = service.getCreatedReservations(list);
        return numCreated;
    }

    public int getCancelledReservations() {
        numCancelled = service.getCancelledReservations(list);
        return numCancelled;

    }

    public int getDeliveredReservations() {
        numDelivered = service.getDeliveredReservations(list);
        return numDelivered;

    }
}
