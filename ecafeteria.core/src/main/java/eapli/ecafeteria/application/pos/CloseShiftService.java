/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package eapli.ecafeteria.application.pos;

import eapli.ecafeteria.domain.cafeteria.CashRegister;
import eapli.ecafeteria.domain.cafeteria.Shift;
import eapli.ecafeteria.domain.mealReservation.Reservation;
import eapli.ecafeteria.domain.meals.MealType;
import eapli.ecafeteria.persistence.CashRegisterRepository;
import eapli.ecafeteria.persistence.MealReservationRepository;
import eapli.ecafeteria.persistence.PersistenceContext;
import eapli.framework.persistence.DataConcurrencyException;
import eapli.framework.persistence.DataIntegrityViolationException;
import eapli.framework.persistence.repositories.TransactionalContext;

/**
 *
 * @author Miguel
 */
public class CloseShiftService {
    
    private final TransactionalContext autoTx = PersistenceContext.repositories().buildTransactionalContext();
    private MealReservationRepository mealRep = PersistenceContext.repositories().mealsReservations(autoTx);
    private CashRegisterRepository cashRegisterRepo = PersistenceContext.repositories().cashRegister(autoTx);
    private CashRegister cashRegister = cashRegisterRepo.find();

    public Iterable<Reservation> getMealsSummary() {
        return mealRep.findByDay(cashRegister.currentShift().date(), getMealType());
    }

    public boolean getCashState() {
        return cashRegister.isOpen();
    }

    public MealType getMealType() {
        return cashRegister.currentShift().mealType();
    }
    
    public int getCreatedReservations(Iterable<Reservation> res) {
       return cashRegister.getCreatedReservations(res);
    }

    public int getCancelledReservations(Iterable<Reservation> res) {
        return cashRegister.getCancelledReservations(res);
    }

    public int getDeliveredReservations(Iterable<Reservation> res) {
        return cashRegister.getDeliveredReservations(res);
    }

    public int getNotDeliveredReservations(Iterable<Reservation> res) {
        return cashRegister.getNotDeliveredReservations(res);
    }
    
    public void registNotDelivered(Iterable<Reservation> res, int nNotDelivered, int nCreated, int nDelivered, int nCancelled) throws DataConcurrencyException, DataIntegrityViolationException{
        autoTx.beginTransaction();
        cashRegister.closeCashRegister(nNotDelivered, nDelivered, nCancelled, nCreated);
        cashRegisterRepo.save(cashRegister).currentShift();
        for (Reservation reservation : cashRegister.registNotDelivered(res)) {
            mealRep.save(reservation);
        }
        autoTx.commit();
    }
}
