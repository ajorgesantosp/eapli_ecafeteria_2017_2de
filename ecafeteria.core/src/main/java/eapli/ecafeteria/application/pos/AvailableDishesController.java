/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package eapli.ecafeteria.application.pos;

import eapli.ecafeteria.application.meals.ListMealService;
import eapli.ecafeteria.domain.mealReservation.Reservation;
import eapli.ecafeteria.persistence.MealReservationRepository;
import eapli.ecafeteria.persistence.PersistenceContext;
import java.util.ArrayList;
import java.util.Collections;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

/**
 *
 * @author Fabio Carneiro 1091328
 */
public class AvailableDishesController {

    private final ListMealService mealService = new ListMealService();
    private final MealReservationRepository reservationRepo = PersistenceContext.repositories().mealsReservations(null);
    private final CloseShiftService service = new CloseShiftService();

    /**
     * Returns HashMap with key representing name of dish and value of current
     * available
     *
     * @return availableDishes HashMap ( Key = designation , Value = current
     * available
     */
    public HashMap<String, Integer> availableDishes() {
        HashMap<String, Integer> availableDishes;
        availableDishes = new HashMap<>();
        List<String> dishNameList = new ArrayList<>();

        Iterable<Reservation> res = service.getMealsSummary();
        for (Reservation r : res) {
            if (r.createdState()) {
                dishNameList.add(r.meal().dish().name().toString());
            }
        }
        Set<String> uniqueDishes = new HashSet<>(dishNameList);
        for (String dish : uniqueDishes) {
            availableDishes.put(dish, Collections.frequency(dishNameList, dish));
        }
        return availableDishes;
    }
}
