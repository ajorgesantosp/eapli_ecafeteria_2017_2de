package eapli.ecafeteria.application.MealReservation;

import eapli.ecafeteria.domain.mealReservation.ReservationBuilder;
import eapli.ecafeteria.Application;
import eapli.ecafeteria.domain.account.Account;
import eapli.ecafeteria.domain.account.AccountTransation;
import eapli.ecafeteria.domain.authz.SystemUser;
import eapli.ecafeteria.domain.mealReservation.Reservation;
import eapli.ecafeteria.domain.meals.Meal;
import eapli.ecafeteria.persistence.CafeteriaUserRepository;
import eapli.ecafeteria.persistence.MealReservationRepository;
import eapli.ecafeteria.persistence.PersistenceContext;
import eapli.framework.persistence.DataConcurrencyException;
import eapli.framework.persistence.DataIntegrityViolationException;
import eapli.framework.persistence.repositories.TransactionalContext;
import eapli.util.DateTime;
import eapli.util.QRCodeHandler;

/**
 *
 * @author Jorge Pereira 1141216 \ Luís Magalhães 1141174
 */
public class ReservationService {

    private final ReservationBuilder builder = new ReservationBuilder();
    private final TransactionalContext TxCtx
            = PersistenceContext.repositories().buildTransactionalContext();
    private final SystemUser sUser = Application.session().session().authenticatedUser();
    private final CafeteriaUserRepository repo = PersistenceContext.repositories().cafeteriaUsers(TxCtx);
    private final MealReservationRepository reservationRepo = PersistenceContext.repositories().mealsReservations(TxCtx);
    private Reservation reservation;

    /**
     * Create a reservation with that meal, will return a dto object to
     * represent the reservation
     *
     * @param m
     * @return
     */
    public Reservation createReservation(Meal m) {
        builder.withMeal(m).
                withCafeteriaUser(repo.findByUsername(sUser.id())).
                withDate(DateTime.now());
        return reservation = builder.build();
    }

    /**
     * Do the reservation saving and finnalize the service
     *
     * @throws DataConcurrencyException
     * @throws DataIntegrityViolationException
     * @throws java.lang.IllegalAccessException
     */
    public void doReservation() throws DataConcurrencyException, DataIntegrityViolationException, IllegalAccessException {
        if(reservation == null){
            throw new IllegalStateException("The reservation is not ready");
        }
        //FIXME This do not need a transactional context
        TxCtx.beginTransaction();
        Account cafeteriaUserAcc = reservation.cafeteriaUser().account();
        if (reservation.createdState()) {
            cafeteriaUserAcc.registTransaction(new AccountTransation(cafeteriaUserAcc, reservation.meal().dish().currentPrice(), AccountTransation.TransationType.DEBIT));
        }
        //TODO increment the number of reservations made
        reservationRepo.save(reservation);
//        repo.save(reservation.cafeteriaUser());
        TxCtx.commit();
    }

}
