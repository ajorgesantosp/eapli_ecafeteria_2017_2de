package eapli.ecafeteria.application.MealReservation;

import eapli.ecafeteria.application.meals.ListMealService;
import eapli.ecafeteria.domain.mealReservation.Reservation;
import eapli.ecafeteria.domain.meals.Meal;
import eapli.framework.application.Controller;
import eapli.framework.persistence.DataConcurrencyException;
import eapli.framework.persistence.DataIntegrityViolationException;
import eapli.util.DateTime;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 *
 * @author Jorge Pereira 1141216 \ Luís Magalhães 1141174
 */
public class MealReservationController implements Controller {

    private final ListMealService svcMeals = new ListMealService();
    private final ReservationService svcReservation = new ReservationService();
    private final Iterable<Meal> meals = svcMeals.getFromDay(DateTime.now());

    /**
     * Returns a list of genericDto of the available meals to make a reservation
     *
     * @return list of available meals
     */
    public Iterable<Meal> listMeals() {
        return meals;
    }

    /**
     * Create the reservation
     *
     * @param meal
     */
    public Reservation createReservation(Meal meal) {
        return svcReservation.createReservation(meal);
    }

    /**
     * Do the reservation
     *
     * @throws DataConcurrencyException
     * @throws DataIntegrityViolationException
     */
    public void doReservation() throws DataConcurrencyException, DataIntegrityViolationException, IllegalAccessException {
            svcReservation.doReservation();
    }
}
