/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package eapli.ecafeteria.application.MealReservation;

import eapli.ecafeteria.domain.account.AccountTransation;
import eapli.ecafeteria.domain.cafeteria.CafeteriaUser;
import eapli.ecafeteria.domain.mealReservation.Reservation;
import eapli.ecafeteria.persistence.CafeteriaUserRepository;
import eapli.ecafeteria.persistence.MealReservationRepository;
import eapli.ecafeteria.persistence.PersistenceContext;
import eapli.framework.application.Controller;
import eapli.framework.domain.Money;
import eapli.framework.persistence.DataConcurrencyException;
import eapli.framework.persistence.DataIntegrityViolationException;

/**
 *
 * @author André Chaves e Alexandre Gomes
 */
public class CancelReservationController implements Controller {

    private final CafeteriaUserRepository repo = PersistenceContext.repositories().cafeteriaUsers(null);
    private final MealReservationRepository mealRepo = PersistenceContext.repositories().mealsReservations(null);
    
    /**
     * Method that cancels a reservation choosed by the user
     * 
     * @param reservation reservation to be cancelled
     * @throws IllegalAccessException exception
     * @throws DataConcurrencyException exception
     * @throws DataIntegrityViolationException exception
     */
    public void cancelReservation(Reservation reservation) throws IllegalAccessException, DataConcurrencyException, DataIntegrityViolationException {

        Money money = reservation.cancelReservation();
        CafeteriaUser cafUser = reservation.cafeteriaUser();

        cafUser.account().registTransaction(new AccountTransation(cafUser.account(), money, AccountTransation.TransationType.CREDIT));
        mealRepo.save(reservation);
    }
}
