/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package eapli.ecafeteria.application.MealReservation;

import eapli.ecafeteria.Application;
import eapli.ecafeteria.domain.authz.SystemUser;
import eapli.ecafeteria.domain.cafeteria.CafeteriaUser;
import eapli.ecafeteria.domain.mealReservation.Reservation;
import eapli.ecafeteria.persistence.MealReservationRepository;
import eapli.ecafeteria.persistence.PersistenceContext;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.List;

/**
 *
 * @author Alexandre Gomes | André Chaves
 */
public class ListReservesService {

    private MealReservationRepository repo = PersistenceContext.repositories().mealsReservations(null);
    private SystemUser sysUser = Application.session().session().authenticatedUser();
    private CafeteriaUser cafUser = PersistenceContext.repositories().cafeteriaUsers(null).findByUsername(sysUser.username());

    public Iterable<Reservation> reservationByDate(int numberOfDays) {
        Calendar date = Calendar.getInstance();
        date.add(Calendar.DATE, numberOfDays);
        return repo.findReservesNDaysFromUser(date, cafUser);
    }
}
