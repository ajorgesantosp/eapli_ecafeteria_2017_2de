/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package eapli.ecafeteria.application.MealReservation;

import eapli.ecafeteria.domain.mealReservation.Reservation;
import eapli.framework.application.Controller;

/**
 *
 * @author André Chaves e Alexandre Gomes
 */
public class ListReserveByDayController implements Controller {

    private final ListReservesService lstReserveService = new ListReservesService();
    
    /**
     * Returns a list of reservations by a number of days
     * 
     * @param numberOfDays number of days
     * @return list of reservations
     */
    public Iterable<Reservation> reservationByDate(int numberOfDays){
       return lstReserveService.reservationByDate(numberOfDays);
    }
    
            

}
