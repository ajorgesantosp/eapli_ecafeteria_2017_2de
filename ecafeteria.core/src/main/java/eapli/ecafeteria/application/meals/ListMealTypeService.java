/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package eapli.ecafeteria.application.meals;

import eapli.ecafeteria.Application;
import eapli.ecafeteria.domain.authz.ActionRight;
import eapli.ecafeteria.domain.meals.MealType;
import eapli.ecafeteria.persistence.MealTypeRepository;
import eapli.ecafeteria.persistence.PersistenceContext;

/**
 *
 * @author Bruno Gomes <1151164@isep.ipp.pt>, Nuno Brito <1151173@isep.ipp.pt>
 */
public class ListMealTypeService {
    
    private MealTypeRepository mealTypeRepository = PersistenceContext.repositories().mealTypes();

    public Iterable<MealType> all() {
        Application.ensurePermissionOfLoggedInUser(ActionRight.MANAGE_MENUS, ActionRight.MANAGE_KITCHEN);
        return this.mealTypeRepository.findAll();
    }
    
}
