/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package eapli.ecafeteria.application.meals;

import eapli.ecafeteria.Application;
import eapli.ecafeteria.domain.authz.ActionRight;
import eapli.ecafeteria.domain.kitchen.KitchenAlert;
import eapli.ecafeteria.persistence.KitchenAlertRepository;
import eapli.ecafeteria.persistence.PersistenceContext;
import eapli.framework.domain.Designation;

/**
 *
 * @author i151164
 */
public class ListKitchenAlertService {
    
    private final KitchenAlertRepository alertRepository = PersistenceContext.repositories().kitchenAlerts();
    
    public Iterable<KitchenAlert> all() {
        Application.ensurePermissionOfLoggedInUser(ActionRight.ADMINISTER);
        return this.alertRepository.findAll();
    }
    
    public KitchenAlert findByName(Designation name) {
        return this.alertRepository.findByName(name);
    }
    
}
