/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package eapli.ecafeteria.application.meals;

import eapli.ecafeteria.Application;
import eapli.ecafeteria.domain.authz.ActionRight;
import eapli.ecafeteria.domain.meals.Dish;
import eapli.ecafeteria.domain.meals.Meal;
import eapli.ecafeteria.domain.meals.MealType;
import eapli.ecafeteria.persistence.MealRepository;
import eapli.ecafeteria.persistence.PersistenceContext;
import eapli.framework.application.Controller;
import eapli.framework.persistence.DataConcurrencyException;
import eapli.framework.persistence.DataIntegrityViolationException;
import eapli.util.DateTime;
import java.util.Calendar;

/**
 *
 * @author Bruno Gomes <1151164@isep.ipp.pt>, Nuno Brito <1151173@isep.ipp.pt>
 */
public class PublishMealController implements Controller {
    
    private ListDishService serviceListDish = new ListDishService();
    private ListMealTypeService serviceListMealType = new ListMealTypeService();
    
    private MealRepository oRep = PersistenceContext.repositories().meals();
    
    public Meal createMeal(String day, MealType mealType, Dish dish) throws Exception {
        Application.ensurePermissionOfLoggedInUser(ActionRight.MANAGE_MENUS);
        
        final Calendar cDay = DateTime.parseDate(day);
        if (cDay==null)
            throw new Exception("Invalid date.");
        // VALIDAR DATA
        return new Meal(cDay, mealType, dish);
    }
    
    public Meal publishMeal(Meal oMeal) throws DataConcurrencyException, DataIntegrityViolationException {
        return this.oRep.save(oMeal);
    }
    
    public Iterable<Dish> dishes() {
        return this.serviceListDish.allDishes();
    }
    
    public Iterable<MealType> mealTypes() {
        return this.serviceListMealType.all();
    }
    
}
