/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package eapli.ecafeteria.application.meals;

import eapli.ecafeteria.Application;
import eapli.ecafeteria.domain.MealMenu.MealMenu;
import eapli.ecafeteria.domain.authz.ActionRight;
import eapli.ecafeteria.domain.meals.Meal;
import eapli.ecafeteria.domain.meals.MealType;
import eapli.ecafeteria.persistence.MealRepository;
import eapli.ecafeteria.persistence.PersistenceContext;
import java.util.Calendar;
import java.util.Date;

/**
 *
 * @author Rafael Delgado <Rafael.delgado at Rafael.org>
 */
public class ListMealService {

    private final MealRepository mealRepository = PersistenceContext.repositories().meals();

    public Iterable<Meal> all() {
        Application.ensurePermissionOfLoggedInUser(ActionRight.MANAGE_MENUS);
        return this.mealRepository.findAll();
    }

    public Iterable<Meal> getMealsByDateAndType(MealType mealType, Calendar date) {
         Application.ensurePermissionOfLoggedInUser(ActionRight.MANAGE_KITCHEN);
        return this.mealRepository.findByDayAndMealType(mealType, date);
    }

    public Iterable<Meal> getFromDay(Calendar date) {
        Application.ensurePermissionOfLoggedInUser(ActionRight.SELECT_MEAL);
        //TODO change to find meals to reserve
        return this.mealRepository.findFromDay(date);
    }

    public Iterable<Meal> mealsFromMealMenu(MealMenu mealMenu) {
        Application.ensurePermissionOfLoggedInUser(ActionRight.MANAGE_MENUS);
        
        return this.mealRepository.mealsOfMealMenu(mealMenu);
    }

}
