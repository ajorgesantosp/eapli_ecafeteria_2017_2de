/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package eapli.ecafeteria.application.account;

import eapli.ecafeteria.domain.account.Account;
import eapli.ecafeteria.domain.account.AccountTransation;
import eapli.ecafeteria.persistence.BankAccountRepository;
import eapli.ecafeteria.persistence.PersistenceContext;
import eapli.framework.domain.Money;
import eapli.framework.persistence.DataConcurrencyException;
import eapli.framework.persistence.DataIntegrityViolationException;

/**
 *
 * @author Alexandre Gomes e André Chaves
 */
class AddFundsService {

    
    private final BankAccountRepository BArepo = PersistenceContext.repositories().accounts(null);
    private Account acc;

    public Account getAccount(String bankAccount) {
        if(bankAccount == null && bankAccount.isEmpty()){
            throw  new IllegalArgumentException("The bankAccount is invalid.");
        }
        return acc = BArepo.findByBankAccount(bankAccount);
    }

    public void addFunds(Money fundsValue) throws IllegalAccessException, DataConcurrencyException, DataIntegrityViolationException {
        if(acc ==null){
            throw new IllegalStateException("You should get the account first.");
        }
        if(fundsValue == null){
            throw  new IllegalArgumentException("The money is invalid.");
        }
        AccountTransation transaction = new AccountTransation(acc, fundsValue, AccountTransation.TransationType.CREDIT);
        //TODO deverá ser feito noutro metodo para confirmar a transaction?
        transaction.doTransation();
        acc.registTransaction(transaction);
        BArepo.save(acc);
    }
    
}
