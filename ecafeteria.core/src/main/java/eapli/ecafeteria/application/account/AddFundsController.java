/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package eapli.ecafeteria.application.account;

import eapli.ecafeteria.persistence.PersistenceContext;
import eapli.framework.application.Controller;
import eapli.framework.domain.Money;
import eapli.framework.persistence.DataConcurrencyException;
import eapli.framework.persistence.DataIntegrityViolationException;
import eapli.framework.persistence.repositories.TransactionalContext;

/**
 *
 * @author André Chaves e Alexandre Gomes
 */
public class AddFundsController implements Controller {

    private final AddFundsService addFundservice = new AddFundsService();
    private final TransactionalContext autoTx = PersistenceContext.repositories().buildTransactionalContext();
            
    /** Add funds to the account of the User
     *
     * @param fundsValue the ammount to be added
     * @param bankAccount bank account information
     * @return true if the operaton is successfuly done
     * @throws DataIntegrityViolationException exception
     * @throws IllegalAccessException exception
     */
    public boolean addFunds(double fundsValue, String bankAccount) throws DataIntegrityViolationException, IllegalAccessException, DataConcurrencyException {
        
        if(fundsValue >= 3){
            autoTx.beginTransaction();
            addFundservice.getAccount(bankAccount);
            addFundservice.addFunds(Money.euros(fundsValue));
            autoTx.commit();
            return true;
            
        } else{
            return false;
        }
    }

}
