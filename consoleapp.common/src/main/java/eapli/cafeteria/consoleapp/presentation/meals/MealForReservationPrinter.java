package eapli.cafeteria.consoleapp.presentation.meals;

import eapli.ecafeteria.domain.meals.Meal;
import eapli.framework.visitor.Visitor;
import eapli.util.DateTime;

/**
 *
 * @author Jorge Pereira 1141216 \\ Luís Magalhães 1141174
 */
public class MealForReservationPrinter implements Visitor<Meal> {

    @Override
    public void visit(Meal visitee) {
        //TODO improve the printer
        System.out.printf(
                //name
                "Name: %-30s\n"
                //dish type
                + "\tDish Type: %-10s\n"
                //nutritional Information
                + "\tNutritional Information: %d Calories %d Salt\n"
                //allergens
                +"\tAllergens: %-30s\n"
                //meal type
                + "\tMeal Type: %-10s"
                //price
                + "\tPrice: %-5f\n"
                //day
                + "\tDate: %-20s\n",
                visitee.dish().name(),
                visitee.dish().dishType().description(),
                visitee.dish().nutricionalInfo().calories(),
                visitee.dish().nutricionalInfo().salt(),
                visitee.dish().allergens(),
                visitee.mealType(),
                visitee.dish().currentPrice().amount(),
                DateTime.format(visitee.day())
        );

    }

}
