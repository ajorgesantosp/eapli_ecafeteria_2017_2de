package eapli.cafeteria.consoleapp.presentation.MealReservation;

import eapli.cafeteria.consoleapp.presentation.meals.MealPrinter;
import eapli.ecafeteria.domain.mealReservation.Reservation;
import eapli.framework.visitor.Visitor;

/**
 *
 * @author Jorge Pereira 1141216 \\ Luís Magalhães 1141174
 */
public class ReservationPrinter implements Visitor<Reservation> {

    @Override
    public void visit(Reservation visitee) {
        System.out.printf("Hash: %d", visitee.hashCode());
        System.out.printf("\nMeal:\n");
        new MealPrinter().visit(visitee.meal());
    }

}
