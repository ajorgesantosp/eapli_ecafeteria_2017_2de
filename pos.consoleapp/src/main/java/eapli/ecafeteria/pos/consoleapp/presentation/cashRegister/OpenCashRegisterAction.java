/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package eapli.ecafeteria.pos.consoleapp.presentation.cashRegister;

import eapli.framework.actions.Action;

/**
 *
 * @author Jorge Pereira 1141216 \ Luís Magalhães 1141174
 */
public class OpenCashRegisterAction implements Action{

    @Override
    public boolean execute() {
        return new OpenCashRegisterUI().doShow();
    }
    
}
