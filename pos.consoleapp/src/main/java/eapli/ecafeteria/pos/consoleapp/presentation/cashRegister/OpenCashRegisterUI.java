/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package eapli.ecafeteria.pos.consoleapp.presentation.cashRegister;

import eapli.cafeteria.consoleapp.presentation.meals.MealTypePrinter;
import eapli.ecafeteria.application.pos.OpenCashRegisterController;
import eapli.ecafeteria.domain.meals.MealType;
import eapli.framework.persistence.DataConcurrencyException;
import eapli.framework.persistence.DataIntegrityViolationException;
import eapli.framework.presentation.console.AbstractUI;
import eapli.framework.presentation.console.SelectWidget;
import eapli.util.DateTime;
import eapli.util.io.Console;
import java.util.Calendar;

/**
 *
 * @author Jorge Pereira 1141216 \ Luís Magalhães 1141174
 */
public class OpenCashRegisterUI extends AbstractUI {

    private final OpenCashRegisterController ctrl = new OpenCashRegisterController();

    @Override
    protected boolean doShow() {
        Calendar date = ctrl.defaultDate();
        String mealTypeString = ctrl.defaultMealType();
        if (!ctrl.startUseCase()) {
            System.out.println("The cash register is already open");
            return false;
        }

        System.out.println("Day: " + DateTime.format(date) + "\nMealType: " + mealTypeString);

        SelectWidget<MealType> lister = new SelectWidget("Meal Type", ctrl.listMealTypes(), new MealTypePrinter());
        lister.show();
        MealType mealType = lister.selectedElement();
        if (mealType == null) {
            //Exit option
            return false;
        }

        final String dateFormat = DateTime.DATE_FORMAT;
        date = Console.readCalendar("Date(" + dateFormat + ")", dateFormat);

        try {
            ctrl.openCashRegister(mealType, date);
            System.out.println("The cash register is open");
            return true;
        } catch (DataConcurrencyException | DataIntegrityViolationException e) {
            //TODO TREAT EACH EXCEPTION
            System.out.println(e.getMessage());
            return false;
        }
    }

    @Override
    public String headline() {
        return "Open Cash Register";
    }

}
