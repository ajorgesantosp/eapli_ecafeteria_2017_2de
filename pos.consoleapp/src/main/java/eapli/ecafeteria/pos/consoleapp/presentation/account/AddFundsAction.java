/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package eapli.ecafeteria.pos.consoleapp.presentation.account;

import eapli.framework.actions.Action;

/**
 *
 * @author André Chaves e Alexandre Gomes
 */
public class AddFundsAction implements Action{

    @Override
    public boolean execute() {
        return new AddFundsUI().show();
    }
    
}
