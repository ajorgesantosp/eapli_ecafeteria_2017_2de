/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package eapli.ecafeteria.pos.consoleapp.presentation.account;

import eapli.ecafeteria.application.account.AddFundsController;
import eapli.framework.persistence.DataConcurrencyException;
import eapli.framework.persistence.DataIntegrityViolationException;
import eapli.framework.presentation.console.AbstractUI;
import eapli.util.io.Console;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 *
 * @author André Chaves e Alexandre Gomes
 */
public class AddFundsUI extends AbstractUI{
    
    private final AddFundsController controller = new AddFundsController();

    @Override
    protected boolean doShow() {
        final String fundsValue = Console.readLine("Amount(€): ");
        final String bankAccount = Console.readLine("MultiBank Account: ");
        
        try {
            return this.controller.addFunds(Double.parseDouble(fundsValue), bankAccount);
        }catch(NullPointerException e){
            System.out.println("Amount of funds invalid!");
            return false;
        } catch (DataIntegrityViolationException ex) {
            System.out.println("Operation failed!");
            return false;
        } catch (IllegalAccessException ex) {
            Logger.getLogger(AddFundsUI.class.getName()).log(Level.SEVERE, null, ex);
            return false;
        } catch (DataConcurrencyException ex) {
            Logger.getLogger(AddFundsUI.class.getName()).log(Level.SEVERE, null, ex);
        }
        return false;
        
    }

    @Override
    public String headline() {
        return "Add Funds";
    }
    
    
    
}
