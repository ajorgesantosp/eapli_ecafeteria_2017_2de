/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package eapli.ecafeteria.pos.consoleapp.presentation.salescharge;

import eapli.ecafeteria.application.pos.AvailableDishesController;
import eapli.ecafeteria.application.pos.CloseShiftController;
import eapli.ecafeteria.application.pos.RegisterMealDeliveryController;
import eapli.framework.application.Controller;
import eapli.framework.presentation.console.AbstractUI;
import eapli.util.io.Console;
import java.util.HashMap;
import java.util.Map;
import java.util.Scanner;

/**
 *
 * @author André Sousa
 */
public class RegisterMealDeliveryUI extends AbstractUI {

    private final RegisterMealDeliveryController theController = new RegisterMealDeliveryController();
    private final AvailableDishesController availableDishesController = new AvailableDishesController();
    private final CloseShiftController CloseController = new CloseShiftController();

    protected Controller controller() {
        return this.theController;
    }

    @Override
    protected boolean doShow() {
        if (!CloseController.getCashState()) {
            System.out.println("Please Open the Cash Register First");
            return false;
        }
        System.out.println("Pratos Disponiveis para hoje:");

        HashMap<String, Integer> dishesMap;
        dishesMap = availableDishesController.availableDishes();
        dishesMap.forEach((key, value) -> {
            System.out.println(key + " : " + value);
        });

        final String qrCode = Console.readLine("QR Code");
        int option;
        Scanner readOption = new Scanner(System.in);
        String qrcodeValidation = this.theController.validateQrCode(qrCode);
        if (qrcodeValidation != "") {
            System.out.println(qrcodeValidation);
            System.out.println("\nQR code is valid! Do you want to confirm the meal was delivered with success?"
                    + "\n1. Confirm\n2. Cancel");

            do {
                option = readOption.nextInt();

                if (option < 1 || option > 2) {
                    System.out.println("Please, select a valid option number!");

                }

            } while (option < 1 || option > 2);

            if (option == 1) {
                System.out.println("Meal was delivered with success" + this.theController.confirmReserveDelivery());
                return true;
            } else {
                System.out.println("The operation was canceled\n");
                return false;
            }

        } else {
            System.out.println("The QR code presented is not valid!\n");
            return false;
        }

    }

    @Override
    public String headline() {
        return "Register Meal Delivery";
    }

}
