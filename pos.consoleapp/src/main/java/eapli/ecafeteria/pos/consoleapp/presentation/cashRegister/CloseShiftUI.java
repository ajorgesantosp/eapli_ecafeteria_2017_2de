/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package eapli.ecafeteria.pos.consoleapp.presentation.cashRegister;

import eapli.ecafeteria.application.pos.CloseShiftController;
import eapli.framework.application.Controller;
import eapli.framework.persistence.DataConcurrencyException;
import eapli.framework.persistence.DataIntegrityViolationException;
import eapli.framework.presentation.console.AbstractUI;

/**
 *
 * @author Miguel
 */
public class CloseShiftUI extends AbstractUI {

    private final CloseShiftController theController = new CloseShiftController();

    protected Controller controller() {
        return this.theController;
    }

    @Override
    protected boolean doShow() {
        if (theController.getCashState()) {

            int nCancelledReservation = theController.getCancelledReservations();
            int nDeliveredReservation = theController.getDeliveredReservations();
           
            // Not delivered meals will be the one that are in the created state
            int nNotDeliveredReservation = theController.getCreatedReservations();
            
            int mealsTotal = nNotDeliveredReservation + nCancelledReservation + nDeliveredReservation;

            try {
                theController.close();
                System.out.println("Meals Summary\n");
                System.out.printf("Meals Total: %d\nDelivered Reservations: %d\nNot Delivered Reservations: %d\nCancelled Reservations: %d\n", mealsTotal, nDeliveredReservation, nNotDeliveredReservation, nCancelledReservation);
               
            } catch (DataConcurrencyException | DataIntegrityViolationException | IllegalStateException | Error ex) {
                System.out.println(ex.getMessage());
            }
            return true;
        }

        System.out.println("Cash Register is already closed");
        return true;
    }

    @Override
    public String headline() {
        return "Close Shift";
    }

}
