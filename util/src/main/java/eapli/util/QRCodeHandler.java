package eapli.util;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.util.Map;
import javax.imageio.ImageIO;

import com.google.zxing.BarcodeFormat;
import com.google.zxing.BinaryBitmap;
import com.google.zxing.MultiFormatReader;
import com.google.zxing.MultiFormatWriter;
import com.google.zxing.NotFoundException;
import com.google.zxing.Result;
import com.google.zxing.WriterException;
import com.google.zxing.client.j2se.BufferedImageLuminanceSource;
import com.google.zxing.client.j2se.MatrixToImageWriter;
import com.google.zxing.common.BitMatrix;
import com.google.zxing.common.HybridBinarizer;
import java.io.FileInputStream;
import java.io.UnsupportedEncodingException;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 * Qrcode handler
 *
 * @author Jorge Pereira 1141216 \\ Luís Magalhães 1141174 \\ Fábio Carneiro
 * 1091328
 */
public class QRCodeHandler {

    private QRCodeHandler() {
        //For utility purpose
    }

    public static final String DEFAULT_CHARSET = "UTF-8";
    public static final Integer DEFAULT_QRCODE_WITH = 200;
    public static final Integer DEFAULT_QRCODE_HEIGHT = 200;

    /**
     * Create a qr code and write into a file
     *
     * @param qrCodeData data to parse
     * @param filePath file to write
     * @param charset The Charset to be used to encode the String
     * @param hintMap Additional parameters to supply to the encoder
     * @param qrCodeheight in pixels
     * @param qrCodewidth in pixels
     * @throws WriterException
     * @throws IOException
     */
    public static void createQRCodeToFile(String qrCodeData, String filePath,
            String charset, Map hintMap, int qrCodeheight, int qrCodewidth) throws WriterException, IOException {
        //Generate the qrcode
        BitMatrix matrix = createQRCodeToMemory(qrCodeData, charset, hintMap, qrCodeheight, qrCodewidth);

        //Write into the file filePath
        MatrixToImageWriter.writeToPath(matrix, filePath.substring(filePath
                .lastIndexOf('.') + 1), new File(filePath).toPath());
    }

    /**
     * Create a qr code and stores it as a BitMatrix on memory
     *
     * @param qrCodeData data to parse
     * @param charset The Charset to be used to encode the String
     * @param hintMap Additional parameters to supply to the encoder
     * @param qrCodeheight in pixels
     * @param qrCodewidth in pixels
     * @return BitMatrix matrix
     * @throws WriterException
     * @throws java.io.UnsupportedEncodingException
     */
    public static BitMatrix createQRCodeToMemory(String qrCodeData,
            String charset, Map hintMap, int qrCodeheight, int qrCodewidth) throws WriterException, UnsupportedEncodingException {

        BitMatrix matrix = new MultiFormatWriter().encode(
                new String(qrCodeData.getBytes(charset), charset),
                BarcodeFormat.QR_CODE, qrCodewidth, qrCodeheight, hintMap);

        return matrix;

    }

    /**
     * Read
     *
     * @param filePath
     * @param charset
     * @param hintMap
     * @return
     * @throws FileNotFoundException
     * @throws IOException
     * @throws NotFoundException
     */
    public static String readQRCodeFromFile(String filePath, String charset, Map hintMap)
            throws FileNotFoundException, IOException, NotFoundException {
        BinaryBitmap binaryBitmap = new BinaryBitmap(new HybridBinarizer(
                new BufferedImageLuminanceSource(
                        ImageIO.read(new FileInputStream(filePath)))));

        //Get the data from the qrcode
        Result qrCodeResult = new MultiFormatReader().decode(
                binaryBitmap, hintMap);
        return qrCodeResult.getText();
    }

    /**
     * Reads QRCode From Bitmatrix
     *
     * @param charset
     * @param hintMap
     * @param matrix
     * @return
     */
    public static String readQRCodeFromBitmatrix(String charset, Map hintMap, BitMatrix matrix) {
        BinaryBitmap binaryBitmap = new BinaryBitmap(new HybridBinarizer(
                new BufferedImageLuminanceSource(MatrixToImageWriter.toBufferedImage(matrix))));
        Result qrCodeResult = null;
        try {
            qrCodeResult = new MultiFormatReader().decode(binaryBitmap, hintMap);
        } catch (NotFoundException ex) {
            Logger.getLogger(QRCodeHandler.class.getName()).log(Level.SEVERE, null, ex);
        }
        return qrCodeResult.getText();
    }

}
