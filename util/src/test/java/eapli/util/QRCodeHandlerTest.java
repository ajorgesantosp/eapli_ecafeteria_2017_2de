/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package eapli.util;

import com.google.zxing.EncodeHintType;
import com.google.zxing.common.BitMatrix;
import com.google.zxing.qrcode.decoder.ErrorCorrectionLevel;
import java.util.EnumMap;
import java.util.Map;
import org.junit.After;
import org.junit.AfterClass;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;
import static org.junit.Assert.*;

/**
 *
 * @author Fabio
 */
public class QRCodeHandlerTest {

    Map<EncodeHintType, ErrorCorrectionLevel> hintMap;

    public QRCodeHandlerTest() {
    }

    @BeforeClass
    public static void setUpClass() {
    }

    @AfterClass
    public static void tearDownClass() {
    }

    @Before
    public void setUp() {
        hintMap = new EnumMap<>(EncodeHintType.class);
        hintMap.put(EncodeHintType.ERROR_CORRECTION, ErrorCorrectionLevel.L);
    }

    @After
    public void tearDown() {
    }

    /**
     * Test of CreateQrCodeToMemory bitmatrix and read it after.
     *
     * @throws java.lang.Exception
     */
    @Test
    public void testCreateQRCodeWriteAndRead() throws Exception {
        System.out.println("createQRCodeToMemory");
        String qrCodeData = "This is a Test Input";
        String charset = "UTF-8"; //"ISO-8859-1
        int qrCodeheight = 200;
        int qrCodewidth = 200;
        String expResult = "This is a Test Input";
        BitMatrix result = QRCodeHandler.createQRCodeToMemory(qrCodeData, charset, hintMap, qrCodeheight, qrCodewidth);
        String resultString = QRCodeHandler.readQRCodeFromBitmatrix(charset, hintMap, result);
        assertEquals(expResult, resultString);
    }

}
